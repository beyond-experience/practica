<?php /*Diccionario */
$diccionario=  array(
	'SPN'=>  array(
'booking'=>array('et1'=>'Traslados',
		'et2'=>'Convenciones',
		'et3'=>'Reserva tu Traslado',
		'et4'=>'Encuentra tu Hotel',
		'et5'=>'Adulto(s)',
		'et6'=>'Niño(s)',
		'et7'=>'Tipo de Viaje',
		'et8'=>'Redondo',
		'et9'=>'Solo salida',
		'et10'=>'Llegada',
		'et11'=>'Salida',
		'et12'=>'Reservar Ahora',
		'et13'=>'¿Que desea hacer?',
		'et14'=>'Seleccione su Tour',
		'et15'=>'Fecha',
		'et16'=>'Baby seat',
		'et17'=>'Reserve su Tour',
		'et18'=>'Seleccione su Tour',
		'et19'=>'Recotizar',
		'et20'=>'Recotizar tu Traslado',),

	'Menu'=>array('et1'=>'Inicio',
		'et2'=>'Traslados',
		'et3'=>'Convenciones',
		'et4'=>'Tours',
		'et5'=>'Acerca de',
		'et6'=>'Cont&aacute;ctenos',
		'et7'=>'Ingles',
		'et8'=>'',
		'et9'=>'',
		'et10'=>'',
		'et11'=>'',
		'et12'=>'',
		'et13'=>'',
		'et14'=>'',
		'et15'=>'',
		'et16'=>'',
		'et17'=>'',
		'et18'=>'',
		'et19'=>'',
		'et20'=>'',),
	'about'=>array('et1'=>'Nuestra Misi&oacute;n y Visi&oacute;n',
		'et2'=>'Cancun Go Travel y Servicios de Transporte<br>
Estamos dedicados a ayudarle a cuidar su tiempo, en traslados del aeropuerto de su hotel en Cancún o la Riviera Maya o incluso en la Península de Yucatán. Compras en Cancún o la Riviera Maya, transferencias exprés, ya sea a tomar un barco para ir a la pesca o a jugar campo de golf! O simplemente para ir a una cena romántica en su lugar favorito.<br>',
		'et3'=>'<b>N</b> uestro principal servicio es el transporte, sin embargo, no estamos limitados a ofrecer este servicio, también contamos con tours y convenciones, nuestra filosofía es el servicio y la calidad.<br>',
		'et4'=>'<b>N</b> uestras unidades están cómodamente amuebladas para su comodidad y seguridad, manejamos precios persona o por vehículo. Siempre optimizamos el tiempo para que pueda hacer lo mejor de sus vacaciones, evitando largas esperas de transporte.',
		'et5'=>'Mejor servio al mejor <b>Precio</b> !!!',),
	'footer'=>array('et1'=>'PAGINAS',
		'et2'=>'Inicio',
		'et3'=>'Acerca de ',
		'et4'=>'Contactanos',
		'et5'=>'Terminos y condiciones',
		'et6'=>'Lista de correos',
		'et7'=>'Inscríbase en nuestra lista de correo para obtener ofertas y actualizaciones',
		'et8'=>'Direcci&oacute;n',
		'et9'=>'Lugares maravillosos',
		'et10'=>'Visitanos - Cancún Quintana Roo, C.P. 77500 <br >AV. Garcia de la Torre Mz.8 SM.2A Lte.47 Depto.  3 ',
		'et11'=>'Correo Electronico..',
		'et12'=>'Correo info@cancungotravel.com',
		'et13'=>'',
		'et14'=>'+52 01 (998) 214 85 25 M&Eacute;XICO',
		'et15'=>'',
		'et16'=>'',
		'et17'=>'',
		'et18'=>'',
		'et19'=>'',
		'et20'=>'',),
	'home'=>array(
		'et1'=>'¿Necesitas <span>inpiraci&oacute;n</span> para tus pr&oacute;ximas vacaciones en Canc&uacute;n?',
		'et2'=>'ESTANCIAS <span>TODO INCLUIDO</span>',
		'et3'=>'ENCUENTRE LOS MEJORES <br>PAQUETES VACACIONALES',
		'et4'=>'Canc&uacute;n Zona hotelera',
		'et5'=>'Precios por noche',
		'et6'=>'Reserva ahora',
		'et7'=>'NUESTRAS <span>BUENAS</span> RAZONES',
		'et8'=>'<span>Tours</span> Premium',
		'et9'=>'Clientes',
		'et10'=>'<span>Soporte</span> Personal',
		'et11'=>'Canc&uacute;n',
		'et12'=>'5ta Avenida',
		'et13'=>'<span>Galer&iacute;a </span> de Tours',
		'et14'=>'Todos',
		'et15'=>'',
		'et16'=>'',
		'et17'=>'',
		'et18'=>'',
		'et19'=>'',
		'et20'=>'',),
	'hotel'=>array('et1'=>'',
		'et2'=>'',
		'et3'=>'',
		'et4'=>'',
		'et5'=>'',
		'et6'=>'',
		'et7'=>'',
		'et8'=>'',
		'et9'=>'',
		'et10'=>'',
		'et11'=>'',
		'et12'=>'',
		'et13'=>'',
		'et14'=>'',
		'et15'=>'',
		'et16'=>'',
		'et17'=>'',
		'et18'=>'',
		'et19'=>'',
		'et20'=>'',),
	'tours'=>array(
		'et1'=>'<b>E</b> n Cancun Go Travel Tours. Sabemos con certeza que usted ha oído hablar de las cosas que puede hacer en este destino caribeño. El clima puede cambiar varias veces al día, pero siempre es increíble, playas de arena blanca, un patrimonio cultural maya muy rico, así como una gran cantidad de excursiones divertidas Cancún y viajes por carretera. Cancún ha florecido como una ciudad moderna y vibrante hoy en día, recibiendo casi 5 millones de visitantes cada año.<br>
Además de beber margaritas en la playa, usted puede llenar sus días con una buena cantidad de diversión como Tours de viajes, desde la natación con tiburones ballena a la pesca en alta mar, puede tener un día de adrenalina en tirolesas por encima de la selva o visita sitios mayas antiguos como Chichén Itzá, una de las nuevas maravillas del mundo y Patrimonio de la Humanidad de la UNESCO.<br>
¿Entonces que te gustaría hacer?<br>
¡Hagánoslo saber!',
		'et2'=>'Detalles del tour',
		'et3'=>'Descripci&oacute;n',
		'et4'=>'Tour',
		'et5'=>'Fecha',
		'et6'=>'Horario',
		'et7'=>'Incluye',
		'et8'=>'¿Necesita nuestra ayuda?',
		'et9'=>'Estar&iacute;amos m&aacute;s que felices de ayudarte. Nuestro asesor de equipo est&aacute; a su servicio 24/7 para ayudarle.',
		'et10'=>'+52 01 (998) 214 85 25 MEXICO',
		'et11'=>'',
		'et12'=>'',
		'et13'=>'',
		'et14'=>'',
		'et15'=>'',
		'et16'=>'',
		'et17'=>'',
		'et18'=>'',
		'et19'=>'',
		'et20'=>'',),
	'transfers'=>array('et1'=>'Traslado',
		'et2'=>'Resultados de su busqueda',
		'et3'=>'Descripci&oacute;n',
		'et4'=>'Caracteristicas',
		'et5'=>'rese&ntilde;as',
		'et6'=>'Hotel Name',
		'et7'=>'Llegada',
		'et8'=>'Salida',
		'et9'=>'MAX Pax',
		'et10'=>'Por/Veh&iacute;culo',
		'et11'=>'Reservar',
		'et12'=>'<b>E</b>l servicio de traslado de Cancun Go Travel  es la mejor manera de comenzar su llegada a Cancún y abrir la puerta al Mundo Maya. Ofrecemos un servicio premium que le asegurará un viaje seguro desde el Aeropuerto Internacional de Cancún hasta su destino. Nuestro objetivo es ofrecer el mejor y más seguro servicio de traslado al aeropuerto de Cancún a todos nuestros clientes a un precio competitivo. Nuestro personal que habla inglés es altamente calificado.<br>
El aeropuerto internacional de Cancún está a sólo 25 minutos de la ciudad de Cancún en coche. Si su viaje es de negocio relacionado o usted está de vacaciones, atreverse a conocer esta hermosa ciudad. Donde se pueden encontrar mercados locales, centros comerciales con las mejores marcas y la vida nocturna sin parar sólo por mencionar algunos. También puede aventurarse a lugares increíbles como Chichén Itzá, Coba, Ek Balam o Tulum Express.<br>
Si lo que quieres es relajarte puedes encontrar increíbles balnearios o simplemente disfrutar de la playa y su increíble agua turquesa azul claro.<br>
El servicio de traslado Cancun Go Travel siempre encontrará una manera de superar las expectativas de nuestros clientes y su bienestar.<br>
Utilice nuestro sistema seguro de reservaciones en línea de Cancun Go Travel Transfer para garantizar un inicio y regreso seguro y asombroso de su viaje.',
		'et13'=>'Personas',
		'et14'=>'Caracteristicas del servicio',
		'et15'=>'El servicio de transporte Cancun Go Travel es profesional y cómodo con conductores calificados. Nuestros conductores le transportarán de manera eficiente, a tiempo a su hotel y de vuelta al aeropuerto.',
		'et16'=>'Staff Bilingue',
		'et17'=>'Transporte con A/A ',
		'et18'=>'Seguros el&eacute;ctricos',
		'et19'=>'Bebidas',
		'et20'=>'Musica',
		'et21'=>'Asiento para Bebe',
		'et22'=>'Seguro de Viaje',
		'et23'=>'SERVICIO',
		'et24'=>'CALIDAD',
		'et25'=>'PRESENTACION',
		'et26'=>'COMODIDAD',
		'et27'=>'Crafter',
		'et28'=>'Hiace',
		'et29'=>'Suburban',
		'et30'=>'Transporter',),
	'payment'=>array('et1'=>'Travel Booking',
		'et2'=>'Detalles de la reservacíon',
		'et3'=>'These are the details that will be used to send you your booking information',
		'et4'=>'Tours',
		'et5'=>'Ingrese su nombre..',
		'et6'=>'Ingrese sus Apellidos.',
		'et7'=>'Ingrese su correo..',
		'et8'=>'Verifique su correo..',
		'et9'=>'Telefono',
		'et10'=>'Datos del Traslado',
		'et11'=>'Hora de llegada',
		'et12'=>'Aerolinea',
		'et13'=>'Vuelo',
		'et14'=>'Hora de salida',
		'et15'=>'By continuing, you agree to the Terms and Conditions.',
		'et16'=>'RESERVAR',
		'et17'=>'Veh&iacute;culo',
		'et18'=>'Traslado',
		'et19'=>'',
		'et20'=>'',),
	'contacto'=>array('et1'=>'Estamos aquí para responder cualquier pregunta que pueda tener sobre nuestro servicio. ',
		'et2'=>' Envíenos un correo y le responderemos tan pronto como nos sea posible. Incluso si hay algo que siempre ha querido experimentar y no puede encontrarlo en Cancun Go Travel, háganoslo saber y nosotros haremos nuestro mejor esfuerzo para encontrarlo para usted.',
		'et3'=>'Si tiene alguna duda sobre el servicio no dude en contactarnos.',
		'et4'=>'Enviar mensaje',
		'et5'=>' <h4>Direcci&oacute;n</h4>
                        <p>Canc&uacute;n Quintana Roo. <br>
                            Mexico. </p>',
		'et6'=>' <h4>Tel&eacute;fono</h4>
                        <p> +52 01 (998) 214 85 25</p>',
		'et7'=>' <h4>Correo electr&oacute;nico</h4>
                        <p>  info@cancungotravel.com<br>
                            www.cancungotravel.com</p>',
		'et8'=>'Nombre',
		'et9'=>'Correo',
		'et10'=>'Telefono',
		'et11'=>'Comentario',
		'et12'=>'',
		'et13'=>'',
		'et14'=>'',
		'et15'=>'',
		'et16'=>'',
		'et17'=>'',
		'et18'=>'',
		'et19'=>'',
		'et20'=>'',),
	'convenciones'=>array('et1'=>'<b>C</b>ancun Go Travel Conventions works closely with planners and suppliers to organize successful networking programs. Unique to the meeting and event industry, these programs are simply a way to engage buyers (planners) and sellers (suppliers) outside of the traditional trade show, hosted buyer programs. They take place in locations where the planner will enjoy the ambiance and setting. The traditional breakfast, lunch, cocktail presentation on behalf of destinations or hotels to get planners to attend are obsolete. Planners these days have a vast variety of ways especially with technology to book business without having to leave their office.',
		'et2'=>'List of Conventions',
		'et3'=>'List <span>of</span> Conventions',
		'et4'=>'In Cancun Go Travel we like to think out of the box, Imagine a Business Card Exchange program sharing RFPs and having fun at a beach destination, major league baseball game and at the same time established face-to-face contact? They become really productive and most importantly time efficiency with real business in front of you.',
		'et5'=>'',
		'et6'=>'',
		'et7'=>'',
		'et8'=>'Nombre',
		'et9'=>'Correo',
		'et10'=>'Telefono',
		'et11'=>'Comentario',
		'et12'=>'',
		'et13'=>'',
		'et14'=>'',
		'et15'=>'',
		'et16'=>'',
		'et17'=>'',
		'et18'=>'',
		'et19'=>'',
		'et20'=>'',),
),'ENG'=>  array(
'booking'=>array('et1'=>'Transfers',
		'et2'=>'Convetions',
		'et3'=>'Book Transfer',
		'et4'=>'Where is your hotel',
		'et5'=>'Adult(s)',
		'et6'=>'Children',
		'et7'=>'Trip Type',
		'et8'=>'Round',
		'et9'=>'One Way',
		'et10'=>'Arrival',
		'et11'=>'Departure',
		'et12'=>'Search Now',
		'et13'=>'What would you like to do?',
		'et14'=>'Select your tour',
		'et15'=>'Date',
		'et16'=>'Baby seat',
		'et17'=>'Book Tour',
		'et18'=>'select your tour',
		'et19'=>'Search Now',
		'et20'=>'Book Transfer',),
'Menu'=>array('et1'=>'Home',
		'et2'=>'Transfers',
		'et3'=>'Conventions',
		'et4'=>'Tours',
		'et5'=>'About Us',
		'et6'=>'Contact Us',
		'et7'=>'Spanish',
		'et8'=>'',
		'et9'=>'',
		'et10'=>'',
		'et11'=>'',
		'et12'=>'',
		'et13'=>'',
		'et14'=>'',
		'et15'=>'',
		'et16'=>'',
		'et17'=>'',
		'et18'=>'',
		'et19'=>'',
		'et20'=>'',),
'about'=>array('et1'=>'Our Mission & Vision',
		'et2'=>'<p style="text-align: justify;">Our <strong>Cancun Go Travel Transportation Services</strong> were carefully created to pamper you and to help you manage your time, so that you can feel safe and relax during your ride from the airport to your hotel in Cancun or the Riviera Maya, or even some place in the Yucatan Peninsula.</p>

<p style="text-align: justify;">Let us help you get to the best places to go shopping in Cancun or the Riviera Maya, we also offer express transfers to either take a boat, go fishing, play a round of golf or just a romantic dinner at your favorite place.</p>',
		'et3'=>'<p style="text-align: justify;"><b>O</b>ur core business is transportation, however we are not limited to only provide this, our philosophy is service and quality, we truly believe you deserve both.</p>',
		'et4'=>'<p style="text-align: justify;"><b>O</b>ur units are equipped to provide you with comfort and safety. We manage a range of prices  depending on your needs, per person or vehicle. We will always try to optimize timings so you can make the best of your vacation.</p>',
		'et5'=>'Best prices, Best service !!!',),
	'home'=>array(
		'et1'=>' Need <span>Inspiration</span> for Your Next Cancun Vacations?',
		'et2'=>'All Inclusive <span>Cancun stays</span>',
		'et3'=>'Find the best Cancun  <br>Vacation Packages',
		'et4'=>'Cancun hotel zone',
		'et5'=>'Price per night',
		'et6'=>'book now',
		'et7'=>'<span>SOME</span> GOOD <span>REASONS WHY</span> CHOOSE US',
		'et8'=>'<span>Premium</span> tours',
		'et9'=>'Customers',
		'et10'=>'<span>Personal</span> Support',
		'et11'=>'Cancun',
		'et12'=>'5th Avenue',
		'et13'=>'<span>Tours </span> Gallery',
		'et14'=>'All',
		'et15'=>'',
		'et16'=>'',
		'et17'=>'',
		'et18'=>'',),
	'footer'=>array('et1'=>'PAGES',
		'et2'=>'Home',
		'et3'=>'About Us',
		'et4'=>'Contact Us',
		'et5'=>'Terms and conditions',
		'et6'=>'OUR MAILING LIST',
		'et7'=>'Sign up for our mailing list for get offers  and upadates',
		'et8'=>'Street address',
		'et9'=>'AMAZING PLACES',
		'et10'=>'Visit Us - Cancun Quintana Roo, C.P. 77500 <br >AV. Garcia de la Torre Mz.8 SM.2A Lte.47 Depto.  3 ',
		'et11'=>'Email Address..',
		'et12'=>'Email info@cancungotravel.com',
		'et13'=>'',
		'et14'=>'+52 01 (998) 214 85 25 MEXICO',
		'et15'=>'',
		'et16'=>'',
		'et17'=>'',
		'et18'=>'',
		'et19'=>'',
		'et20'=>'',),
	'hotel'=>array('et1'=>'',
		'et2'=>'',
		'et3'=>'',
		'et4'=>'',
		'et5'=>'',
		'et6'=>'',
		'et7'=>'',
		'et8'=>'',
		'et9'=>'',
		'et10'=>'',
		'et11'=>'',
		'et12'=>'',
		'et13'=>'',
		'et14'=>'',
		'et15'=>'',
		'et16'=>'',
		'et17'=>'',
		'et18'=>'',
		'et19'=>'',
		'et20'=>'',),
	'tours'=>array('et1'=>'<p style="text-align: justify;">We know for sure you have heard about all the things to do in this Caribbean destination. The view can change several times a day but will always mesmerizing, white sand beaches, a rich Mayan cultural heritage as well as a tremendous amount of fun excursions and road trips. Cancun has flourished into the modern and vibrant city that it is today, receiving nearly 5 million visitors every year.</p>

<p style="text-align: justify;"><b>B</b>esides indulging yourself sipping margaritas at the beach, you can fill your days with a fair amount of fun <strong>Cancun go Travel tours</strong>, from swimming with whale sharks to deep sea fishing, you can have an adrenaline fueled day zip-lining above the jungle or visiting ancient Mayan sites like Chichen Itza, one of the new wonders of the world and an UNESCO World Heritage Site.</p>

<p>So, what would you like to do?<br>
Let us Know, we can make it happen!</p>',
		'et2'=>'Tour Details',
		'et3'=>'Description',
		'et4'=>'Tour Name',
		'et5'=>'Pick Up',
		'et6'=>'Schedule',
		'et7'=>'Includes',
		'et8'=>'Need Our Help?',
		'et9'=>'We would be more than happy to help you. Our team advisor are 24/7 at your service to help you.',
		'et10'=>'+52 01 (998) 214 85 25 MEXICO',
		'et11'=>'',
		'et12'=>'',
		'et13'=>'',
		'et14'=>'',
		'et15'=>'',
		'et16'=>'',
		'et17'=>'',
		'et18'=>'',
		'et19'=>'',
		'et20'=>'',),
	'transfers'=>array('et1'=>'Transfer',
		'et2'=>'Transfer Result',
		'et3'=>'Description',
		'et4'=>'Feature',
		'et5'=>'Reviews',
		'et6'=>'Hotel Name',
		'et7'=>'Picking Up',
		'et8'=>'Dropping Off',
		'et9'=>'Max pax',
		'et10'=>'Per/Vehicle',
		'et11'=>'BOOK',
		'et12'=>'<p style="text-align: left;"><b>C</b>Cancun Go Travel Transfers is the best way to start your arrival to Cancun and open a gate of rich culture and adventure within the Mayan World. We offer a Premium Service that will ensure you a safe ride from the International Cancun Airport to your destination.</p>

<p style="text-align: left;">Our goal, is to offer the best and the safest Cancun airport transfer service to our customers at a very reasonable and competitive price. Our English-speaking staff is highly qualified and ready to assist you on a Professional level.</p>

<p style="text-align: left;">Cancun International airport is only a 25 minutes ride away from Cancun City. Have the audacity to get to know this beautiful city, whether your trip is business-related or you are on vacation. Where you can find non-stop nightlife, local markets, Luxury shopping malls, among many other things. You can also adventure yourself to amazing places like Chichén-Itzá, Cobá, Ek-Balam or Tulum.</p>

<p style="text-align: left;">If you are seeking for adventure or just want to relax, visit a SPA or take a Yoga Class on the beach, just make sure to enjoy the beach and enjoy the view of the amazing clear turquoise blue water.</p>

<p style="text-align: left;">Cancun Go Travel Transfers will always find a way to exceed our customers’ expectations and their well-being.</p>
 
<p style="text-align: left;">Use our secure online Cancun Go Travel Transfers booking system to ensure a safe and amazing start and return of your trip.',
		'et13'=>'Persons',
		'et14'=>'Shuttle Features',
		'et15'=>'The Cancun Go Travel shuttle service is professional and comfortable with qualified drivers. Our drivers will transport you efficiently, On Time to your hotel and back to the airport.',
		'et16'=>'English speaking Staff',
		'et17'=>'A/A Transportation',
		'et18'=>'Power Door Lock',
		'et19'=>'Beverages',
		'et20'=>'Music',
		'et21'=>'Baby seat',
		'et22'=>'Travel insurance',
		'et23'=>'SERVICE',
		'et24'=>'QUALITY',
		'et25'=>'STYLE',
		'et26'=>'COMFORTABLE',
		'et27'=>'Crafter',
		'et28'=>'Hiace',
		'et29'=>'Suburban',
		'et30'=>'Transporter',),
	'payment'=>array('et1'=>'Travel Booking',
		'et2'=>'Reservation details',
		'et3'=>'These are the details that will be used to send you your booking information',
		'et4'=>'Tours',
		'et5'=>'Enter First Name..',
		'et6'=>'Enter Last Name..',
		'et7'=>'Enter Email Address..',
		'et8'=>'Verify Email Address..',
		'et9'=>'Phone Number',
		'et10'=>'Data for transfer',
		'et11'=>'Arrival Time',
		'et12'=>'Airline',
		'et13'=>'Fligth number',
		'et14'=>'Departure Time',
		'et15'=>'By continuing, you agree to the Terms and Conditions.',
		'et16'=>'Book',
		'et17'=>'Vehicle',
		'et18'=>'Transfer',
		'et19'=>'',
		'et20'=>'',),
	'contacto'=>array('et1'=>'<p>We are here to answer any questions you may have about our services. Reach out to us and we´ll respond as soon as we possible.</p>',
		'et2'=>' <p>Even if there is something you have always wanted to experience and can´t find it on  Cancun Go Travel, let us know and we promise we´l do our best to find it for you and send you there.</p>',
		'et3'=>'If you got any questions, please do not hesitate to contact us.',
		'et4'=>'Send message',
		'et5'=>' <h4>Address</h4>
                        <p>Cancun Quintana Roo. <br>
                            Mexico. </p>',
		'et6'=>' <h4>Phone</h4>
                        <p> +52 01 (998) 214 85 25</p>',
		'et7'=>' <h4>Email Address</h4>
                        <p>  info@cancungotravel.com<br>
                            www.cancungotravel.com</p>',
		'et8'=>'Name',
		'et9'=>'Email Address',
		'et10'=>'Phone',
		'et11'=>'Write message',
		'et12'=>'',
		'et13'=>'',
		'et14'=>'',
		'et15'=>'',
		'et16'=>'',
		'et17'=>'',
		'et18'=>'',
		'et19'=>'',
		'et20'=>'',),
	'convenciones'=>array('et1'=>'<p style="text-align: justify;"><strong>Cancun Go Travel Conventions</strong> works closely with planners and suppliers who are dedicated to the meeting and event industry to organize successful networking programs. These programs are simply a way to engage buyers (planners) and sellers (suppliers) outside of the traditional trade show of hosted buyer programs. They take place in locations where the planner will enjoy the ambiance and setting. The traditional breakfast, lunch, cocktail presentation on behalf of destinations or hotels to get planners to attend are obsolete. Planners these days have a vast variety of ways, especially with technology, to book business without having to leave their office.</p>

<p style="text-align: justify;"><b>I</b>n <strong>Cancun Go Travel </strong>  we like to think out of the box, Imagine a Business Card Exchange program sharing RFPs and having fun at a beach destination, major league baseball game and at the same time established face-to-face contact? They become really productive and most importantly time efficiency with real business in front of you.</p>',
		'et2'=>'List of Conventions',
		'et3'=>'List <span>of</span> Conventions',
		'et4'=>'In Cancun Go Travel we like to think out of the box, Imagine a Business Card Exchange program sharing RFPs and having fun at a beach destination, major league baseball game and at the same time established face-to-face contact? They become really productive and most importantly time efficiency with real business in front of you.',
		'et5'=>'',
		'et6'=>'',
		'et7'=>'',
		'et8'=>'Nombre',
		'et9'=>'Correo',
		'et10'=>'Telefono',
		'et11'=>'Comentario',
		'et12'=>'',
		'et13'=>'',
		'et14'=>'',
		'et15'=>'',
		'et16'=>'',
		'et17'=>'',
		'et18'=>'',
		'et19'=>'',
		'et20'=>'',),
));

