<?php
$app->get("/sitios/:idSitio/materiales/activos/", function($idSitio) use($app){
  try{
    $idMaterial=0;
    $connection = getConnection();
    $dbh = $connection->prepare("CALL sp_getMateriales(?,?)");
    $dbh->bindParam(1, $idSitio);
    $dbh->bindParam(2, $idMaterial);
    $dbh->execute();
    $elementos = $dbh->fetchAll();
    $connection = null;
    $respuesta = array();
    foreach ($elementos as $elemento) {
      if($elemento["idEstatus"]==1){
        $respuesta[] = array('idMaterial' => $elemento["idMaterial"]
          ,'idEstatus' => $elemento["idEstatus"]
          ,'material' => htmlentities($elemento["material"])
        );
      }
    }

    $app->response->headers->set("Content-type", "application/json");
    $app->response->status(200);
    $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
  }
  catch(PDOException $e){
    echo "Error: " . $e->getMessage();
  }
});

$app->get("/sitios/:idSitio/materiales/", function($idSitio) use($app){
  try{
    $idMaterial=0;
    $connection = getConnection();
    $dbh = $connection->prepare("CALL sp_getMateriales(?,?)");
    $dbh->bindParam(1, $idSitio);
    $dbh->bindParam(2, $idMaterial);
    $dbh->execute();
    $elementos = $dbh->fetchAll();
    $connection = null;
    $respuesta = array();
    foreach ($elementos as $elemento) {
      $respuesta[] = array('idMaterial' => $elemento["idMaterial"]
        ,'idEstatus' => $elemento["idEstatus"]
        ,'material' => htmlentities($elemento["material"])
      );
    }
    $data= array('data' =>$respuesta  );
    $app->response->headers->set("Content-type", "application/json");
    $app->response->status(200);
    $app->response->body(json_encode($data, JSON_UNESCAPED_UNICODE));
  }
  catch(PDOException $e){
    echo "Error: " . $e->getMessage();
  }
});
$app->get("/sitios/:idSitio/materiales/:idMaterial", function($idSitio,$idMaterial) use($app){
  try{
    $connection = getConnection();
    $dbh = $connection->prepare("CALL sp_getMateriales(?,?)");
    $dbh->bindParam(1, $idSitio);
    $dbh->bindParam(2, $idMaterial);
    $dbh->execute();
    $elemento = $dbh->fetch();
    $connection = null;
    $respuesta = array();
    if(!empty($elemento)) {
        $respuesta =  array('idMaterial' => $elemento["idMaterial"]
          ,'idEstatus' => $elemento["idEstatus"]
          ,'material' => htmlentities($elemento["material"])
        );
    }

    $app->response->headers->set("Content-type", "application/json");
    $app->response->status(200);
    $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
  }
  catch(PDOException $e){
    echo "Error: " . $e->getMessage();
  }
});

$app->post("/sitios/:idSitio/materiales/", function($idSitio) use($app){
    try{
      $connection = getConnection();
      $idEstatus=$app->request->post('idEstatus');
      $material=$app->request->post('material');

      $dbh = $connection->prepare("CALL sp_addMaterial(?,?,?)");
      $dbh->bindParam(1, $idSitio);
      $dbh->bindParam(2, $idEstatus);
      $dbh->bindParam(3, $material);
      $dbh->execute();
      $elemento = $dbh->fetch();
      $connection = null;
      $respuesta = array();
      if(!empty($elemento)) {
        $respuesta = array('respuesta' => $elemento["respuesta"]
          , 'mensaje' => htmlentities(utf8_encode($elemento["mensaje"]))
          );
      }
      $data=array('data'=>$respuesta);
      $app->response->headers->set("Content-type", "application/json");
      $app->response->status(200);
      $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});
$app->options("/sitios/:idSitio/materiales/:idMaterial", function($idSitio,$idMaterial) {
    //Return response headers
});

$app->put("/sitios/:idSitio/materiales/:idMaterial", function($idSitio,$idMaterial) use($app){
  try{
    $connection = getConnection();
    $idEstatus=$app->request->post('idEstatus');
    $material=$app->request->post('material');
 
    $orden=$app->request->post('orden');
    $dbh = $connection->prepare("CALL sp_editMaterial(?,?,?,?)");
    $dbh->bindParam(1, $idSitio);
    $dbh->bindParam(2, $idMaterial);
    $dbh->bindParam(3, $idEstatus);
    $dbh->bindParam(4, $material);
    $dbh->execute();
    $elemento = $dbh->fetch();
    $connection = null;
    $respuesta = array();
    if(!empty($elemento)) {
      $respuesta = array('respuesta' => $elemento["respuesta"]
        , 'mensaje' => htmlentities(utf8_encode($elemento["mensaje"]))
        );
    }
    $app->response->headers->set("Content-type", "application/json");
    $app->response->status(200);
    $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
  }
  catch(PDOException $e){
      echo "Error: " . $e->getMessage();
  }
});

$app->delete("/sitios/:idSitio/materiales/:idMaterial", function($idSitio,$idMaterial) use($app){
    try{
        $connection = getConnection();
        $dbh = $connection->prepare("CALL sp_deleteMaterial(?,?)");
        $dbh->bindParam(1, $idSitio);
        $dbh->bindParam(2, $idMaterial);

        $dbh->execute();
        $elemento = $dbh->fetch();
        $connection = null;
        $respuesta = array();
        if(!empty($elemento)) {
          $respuesta = array('respuesta' => $elemento["respuesta"]
            , 'mensaje' => htmlentities(utf8_encode($elemento["mensaje"]))
            );
        }
        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});