<?php
$app->get("/salones/:idSalon/categorias/:idCategoria/subcategorias/activas/", function($idSalon,$idCategoria) use($app){
    try{
        $idSubcategoria =0;
        $connection = getConnection();
        $dbh = $connection->prepare("CALL sp_getSubcategorias(?,?,?)");
        $dbh->bindParam(1, $idSalon);
        $dbh->bindParam(2, $idCategoria);
        $dbh->bindParam(3, $idSubcategoria);
        $dbh->execute();
        $elementos = $dbh->fetchAll();
        $connection = null;
        $respuesta = array();
        foreach ($elementos as $elemento) {
          if($elemento["idEstatus"]==1)
          $respuesta[]= array('idSubcategoria' => $elemento["idSubcategoria"]
            ,'idCategoria' => $elemento["idCategoria"]
            , 'subCategoria' => htmlentities($elemento["subCategoria"])
            , 'categoria' => htmlentities($elemento["categoria"])
            , 'descripcion' => htmlentities($elemento["descripcion"])
            , 'idEstatus' => $elemento["idEstatus"]
            , 'clave' => htmlentities($elemento["clave"])
            );
        }
        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});
$app->get("/salones/:idSalon/categorias/:idCategoria/subcategorias/", function($idSalon,$idCategoria) use($app){
    try{
        $idSubcategoria =0;
        $connection = getConnection();
        $dbh = $connection->prepare("CALL sp_getSubcategorias(?,?,?)");
        $dbh->bindParam(1, $idSalon);
        $dbh->bindParam(2, $idCategoria);
        $dbh->bindParam(3, $idSubcategoria);
        $dbh->execute();
        $elementos = $dbh->fetchAll();
        $connection = null;
        $respuesta = array();
        foreach ($elementos as $elemento) {
          $respuesta[]= array('idSubcategoria' => $elemento["idSubcategoria"]
            ,'idCategoria' => $elemento["idCategoria"]
            , 'subCategoria' => htmlentities($elemento["subCategoria"])
            , 'categoria' => htmlentities($elemento["categoria"])
            , 'descripcion' => htmlentities($elemento["descripcion"])
            , 'idEstatus' => $elemento["idEstatus"]
            , 'clave' => htmlentities($elemento["clave"])
            );
        }

      $data=array('data'=>$respuesta);
      $app->response->headers->set("Content-type", "application/json");
      $app->response->status(200);
      $app->response->body(json_encode($data, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});

$app->get("/salones/:idSalon/categorias/:idCategoria/subcategorias/:idSubcategoria", function($idSalon,$idCategoria,$idSubcategoria) use($app){
    try{
        $connection = getConnection();
        $dbh = $connection->prepare("CALL sp_getSubcategorias(?,?,?)");
        $dbh->bindParam(1, $idSalon);
        $dbh->bindParam(2, $idCategoria);
        $dbh->bindParam(3, $idSubcategoria);
        $dbh->execute();
        $elemento = $dbh->fetch();
        $connection = null;
        $respuesta = array();
        if(!empty($elemento)) {
          $respuesta= array('idSubcategoria' => $elemento["idSubcategoria"]
            ,'idCategoria' => $elemento["idCategoria"]
            , 'subCategoria' => htmlentities($elemento["subCategoria"])
            , 'categoria' => htmlentities($elemento["categoria"])
            , 'descripcion' => htmlentities($elemento["descripcion"])
            , 'idEstatus' => $elemento["idEstatus"]
            , 'clave' => htmlentities($elemento["clave"])
            );
        }

        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});

$app->post("/salones/:idSalon/categorias/:idCategoria/subcategorias/", function($idSalon,$idCategoria) use($app){
   try{
        $idSubcategoria=0;
        $connection = getConnection();
        $descripcion=$app->request->post('descripcion');
        $subCategoria=$app->request->post('subCategoria');
        $idEstatus=$app->request->post('idEstatus');
        $clave=$app->request->post('clave');
        $dbh = $connection->prepare("CALL sp_addSubcategoria(?,?,?,?,?,?)");
        $dbh->bindParam(1, $idSalon);
        $dbh->bindParam(2, $idCategoria);
        $dbh->bindParam(3, $subCategoria);
        $dbh->bindParam(4, $descripcion);
        $dbh->bindParam(5, $idEstatus);
        $dbh->bindParam(6, $clave);
        $dbh->execute();
        $elemento = $dbh->fetch();
        $connection = null;
        $respuesta = array();
        if(!empty($elemento)) {
          $respuesta = array('respuesta' => $elemento["respuesta"]
            , 'mensaje' => htmlentities($elemento["mensaje"])
            );
        }
        $data=array('data'=>$respuesta);
        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});
$app->options("/salones/:idSalon/categorias/:idCategoria/subcategorias/:idSubcategoria", function($idSalon,$idCategoria,$idSubcategoria) {
    //Return response headers
});

$app->put("/salones/:idSalon/categorias/:idCategoria/subcategorias/:idSubcategoria", function($idSalon,$idCategoria,$idSubcategoria) use($app){
    try{
        $connection = getConnection();
        $descripcion=$app->request->post('descripcion');
        $subCategoria=$app->request->post('subCategoria');
        $idEstatus=$app->request->post('idEstatus');
        $clave=$app->request->post('clave');
        $dbh = $connection->prepare("CALL sp_editSubcategoria(?,?,?,?,?,?,?)");
        $dbh->bindParam(1, $idSalon);
        $dbh->bindParam(2, $idCategoria);
        $dbh->bindParam(3, $idSubcategoria);
        $dbh->bindParam(4, $subCategoria);
        $dbh->bindParam(5, $descripcion);
        $dbh->bindParam(6, $idEstatus);
        $dbh->bindParam(7, $clave);

        $dbh->execute();
        $elemento = $dbh->fetch();
        $connection = null;
        $respuesta = array();
        if(!empty($elemento)) {
          $respuesta = array('respuesta' => $elemento["respuesta"]
            , 'mensaje' => htmlentities($elemento["mensaje"])
            );
        }
        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});
$app->delete("/salones/:idSalon/categorias/:idCategoria/subcategorias/:idSubcategoria", function($idSalon,$idCategoria,$idSubcategoria) use($app){
    try{
        $connection = getConnection();
        $dbh = $connection->prepare("CALL sp_deleteSubcategoria(?,?,?)");
        $dbh->bindParam(1, $idSalon);
        $dbh->bindParam(2, $idCategoria);
        $dbh->bindParam(3, $idSubcategoria);
        $dbh->execute();
        $elemento = $dbh->fetch();
        $connection = null;
        $respuesta = array();
        if(!empty($elemento)) {
          $respuesta = array('respuesta' => $elemento["respuesta"]
            , 'mensaje' => htmlentities($elemento["mensaje"])
            );
        }
        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});