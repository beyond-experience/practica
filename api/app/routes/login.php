<?php
$app->post("/login/", function() use($app)
{
	
	try{
		$connection = getConnection();
		$tk=uniqid();
		$user = $app->request->post("usuario");
	    $pass = $app->request->post("contrasena");
		$dbh = $connection->prepare("CALL sp_getLogin(?,?,?)");
		$dbh->bindParam(1, $user);
		$dbh->bindParam(2, $pass);
		$dbh->bindParam(3, $tk);
		$dbh->execute();
		$elemento = $dbh->fetch();
		$connection = null;
		$respuesta = array();
		$respuesta= array('token' => '','mensaje'=>'Error en credenciales');
    if( !empty($elemento)) {
    	if($elemento["respuesta"]>0){
     	 $respuesta= array('token' => $tk);
    	}
    }
    $app->response->headers->set("Content-type", "application/json");
    $app->response->status(200);
    $app->response->body(json_encode($respuesta));
	}
	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
	}
});
$app->get("/login/", function() use($app)
{
	$respuesta= array('Trabajando' => '.....');
	try{

	 $app->response->headers->set("Content-type", "application/json");
    $app->response->status(200);
    $app->response->body(json_encode($respuesta));
	}
	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
	}	
});
$app->get("/login/:token", function($token) use($app)
{
	try{
		$connection = getConnection();
		$dbh = $connection->prepare("CALL sp_getLoginDatos (?)");
		$dbh->bindParam(1, $token);
		$dbh->execute();
		$elemento = $dbh->fetch();
		$connection = null;
		$respuesta = array();
	    if(!empty($elemento)) {
	      	$respuesta = array('idUsuario' => $elemento["idUsuario"]
		   	, 'nombre' => htmlentities(utf8_encode($elemento["nombre"]))
		   	, 'empresa' => htmlentities(utf8_encode($elemento["empresa"]))
		   	, 'rfc' => htmlentities(utf8_encode($elemento["rfc"]))
		   	, 'telefono' => htmlentities(utf8_encode($elemento["telefono"]))
	        , 'usuario' => htmlentities(utf8_encode($elemento["usuario"]))
	        , 'apellidos' => htmlentities(utf8_encode($elemento["apellidos"]))
	        , 'correo' => htmlentities(utf8_encode($elemento["correo"]))
	        , 'rol' => htmlentities(utf8_encode($elemento["rol"]))
	        , 'token' => htmlentities(utf8_encode($elemento["token"]))
	        , 'idGiro' => $elemento["idGiro"]
	        , 'idEmpresa' => $elemento["idEmpresa"]
	        , 'idEstatus' => $elemento["idEstatus"]);
	    }

	    $app->response->headers->set("Content-type", "application/json");
	    $app->response->status(200);
	    $app->response->body(json_encode($respuesta));
	}
	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
	}
});