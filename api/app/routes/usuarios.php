<?php
$app->get("/users/", function() use($app){
	try{
		$idUsuario=0;
        $idEmpresa=1;
		$connection = getConnection();
		$dbh = $connection->prepare("CALL s_getUsers ()");
		$dbh->execute();
		$elementos = $dbh->fetchAll();
		$connection = null;
		$respuesta = array();
    foreach ($elementos as $elemento) {
      $respuesta[] = array('idUser' => $elemento["idUser"]
        , 'name' => htmlentities($elemento["name"])
        , 'lastName' => htmlentities($elemento["lastName"])
      );
    }
    $app->response->headers->set("Content-type", "application/json");
    $app->response->status(200);
    $app->response->body(json_encode($respuesta));
	}
	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
	}
});
$app->get("/sitios/:idSitio/usuarios/:idUsuario", function($idSitio,$idUsuario) use($app){
	try{
		$connection = getConnection();
		$dbh = $connection->prepare("CALL sp_getUsuarios (?,?)");
		$dbh->bindParam(1, $idSitio);
		$dbh->bindParam(2, $idUsuario);
		$dbh->execute();
		$elemento = $dbh->fetch();
		$connection = null;
		$respuesta = array();
	    if(!empty($elemento)) {
	      $respuesta = array('idUsuario' => $elemento["idUsuario"]
        ,'nombre' => htmlentities($elemento["nombre"])
        ,'usuario' => htmlentities($elemento["usuario"])
        ,'apellidos' => htmlentities($elemento["apellidos"])
        ,'correo' => htmlentities($elemento["correo"])
        ,'empresa' => htmlentities($elemento["empresa"])
        ,'rol' => htmlentities($elemento["rol"])
        ,'telefono' => htmlentities($elemento["telefono"])
	    ,'sucursal' => htmlentities($elemento["sucursal"])
        ,'idEstatus' => $elemento["idEstatus"]
        ,'idSexo' => $elemento["idSexo"]
        , 'comision' => $elemento["comision"]
 				,'idSucursal' => $elemento["idSucursal"]
 				,'idRol' => $elemento["idRol"]
 				,'clase' => htmlentities($elemento["clase"])
				,'idEmpresa' => $elemento["idEmpresa"]
      );
    }
    $app->response->headers->set("Content-type", "application/json");
    $app->response->status(200);
    $app->response->body(json_encode($respuesta));
	}
	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
	}
});
$app->post("/sitios/:idSitio/usuarios/", function($idSitio) use($app){
	try{

        $connection = getConnection();
        $usuario=$app->request->post('usuario');
        $correo=$app->request->post('correo');
        $nombre=$app->request->post('nombre');
        $apellidos=$app->request->post('apellidos');
        $idSexo=$app->request->post('idSexo');
        $idEstatus=1;
        $contrasenia=$app->request->post('contrasenia');
        $idSucursal=$app->request->post('idSucursal');
        $telefono=$app->request->post('telefono');
        $clase=$app->request->post('clase');
        $comision=$app->request->post('comision');
        $fechaNacimiento=$app->request->post('fechaNacimiento');
        $idRol=$app->request->post('idRol');

        $dbh = $connection->prepare("CALL sp_addUsuario(?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
        $dbh->bindParam(1, $idSitio);
        $dbh->bindParam(2, $usuario);
        $dbh->bindParam(3, $correo);
        $dbh->bindParam(4, $nombre);
        $dbh->bindParam(5, $apellidos);
        $dbh->bindParam(6, $idSexo);
        $dbh->bindParam(7, $idEstatus);
        $dbh->bindParam(8, $contrasenia);
        $dbh->bindParam(9, $idSucursal);
        $dbh->bindParam(10, $telefono);
        $dbh->bindParam(11, $clase);
        $dbh->bindParam(12, $comision);
        $dbh->bindParam(13, $fechaNacimiento);
        $dbh->bindParam(14, $idRol);

        $dbh->execute();
        $elemento = $dbh->fetch();
        $connection = null;
        $respuesta = array();
        if(!empty($elemento)) {
          $respuesta = array('respuesta' => $elemento["respuesta"]
            , 'mensaje' => htmlentities($elemento["mensaje"])
            );
        }
        $data=array('data'=>$respuesta);
        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});
$app->options("/sitios/:idSitio/usuarios/:idUsuario", function($idSitio,$idUsuario)use($app) {
    //Return response headers
});

$app->put("/sitios/:idSitio/usuarios/:idUsuario", function($idSitio,$idUsuario) use($app){
   try{

        $connection = getConnection(); 
        $connection = getConnection();
        $usuario=$app->request->post('usuario');
        $correo=$app->request->post('correo');
        $nombre=$app->request->post('nombre');
        $apellidos=$app->request->post('apellidos');
        $idSexo=$app->request->post('idSexo');
        $idEstatus=$app->request->post('idEstatus');
        $contrasenia=$app->request->post('contrasenia');
        $idSucursal=$app->request->post('idSucursal');
        $telefono=$app->request->post('telefono');
        $clase=$app->request->post('clase');
        $comision=$app->request->post('comision');
        $fechaNacimiento=$app->request->post('fechaNacimiento');
        $idRol=$app->request->post('idRol');

        $dbh = $connection->prepare("CALL sp_editUsuario(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
        $dbh->bindParam(1, $idSitio);
        $dbh->bindParam(2, $idUsuario);
        $dbh->bindParam(3, $usuario);
        $dbh->bindParam(4, $correo);
        $dbh->bindParam(5, $nombre);
        $dbh->bindParam(6, $apellidos);
        $dbh->bindParam(7, $idSexo);
        $dbh->bindParam(8, $idEstatus);
        $dbh->bindParam(9, $contrasenia);
        $dbh->bindParam(10, $idSucursal);
        $dbh->bindParam(11, $telefono);
        $dbh->bindParam(12, $clase);
        $dbh->bindParam(13, $comision);
        $dbh->bindParam(14, $fechaNacimiento);
        $dbh->bindParam(15, $idRol);


        $dbh->execute();
        $elemento = $dbh->fetch();
        $connection = null;
        $respuesta = array();
        if(!empty($elemento)) {
          $respuesta = array('respuesta' => $elemento["respuesta"]
            , 'mensaje' => htmlentities($elemento["mensaje"])
            );
        }
        $data=array('data'=>$respuesta);
        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});

$app->get("/usuarios/", function() use($app)
{
	try{
		$connection = getConnection();
		$id=0;
		$dbh = $connection->prepare("CALL sp_getUsuarios (?)");
		$dbh->bindParam(1, $id);
		$dbh->execute();
		$elementos = $dbh->fetchAll();
		$connection = null;
		$respuesta = array();
    foreach ($elementos as $elemento) {
      $respuesta[] = array('idUsuario' => $elemento["idUsuario"]
        , 'nombre' => htmlentities(utf8_encode($elemento["nombre"]))
        , 'usuario' => htmlentities(utf8_encode($elemento["usuario"]))
        , 'apellidos' => htmlentities(utf8_encode($elemento["apellidos"]))
        , 'correo' => htmlentities(utf8_encode($elemento["correo"]))
        , 'idEstatus' => $elemento["idEstatus"]);
    }
    $data=array('data'=>$respuesta);
    $app->response->headers->set("Content-type", "application/json");
    $app->response->status(200);
    $app->response->body(json_encode($data));
	}
	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
	}
});

$app->get("/usuarios/:id", function($id) use($app)
{
	try{
		$connection = getConnection();
		$dbh = $connection->prepare("CALL sp_Usuarios (?)");
		$dbh->bindParam(1, $id);
		$dbh->execute();
		$elemento = $dbh->fetch();
		$connection = null;
		$respuesta = array();
	    if(!empty($elemento)) {
	      $respuesta = array('idUsuario' => $elemento["idUsuario"]
		   , 'nombre' => htmlentities(utf8_encode($elemento["nombre"]))
	        , 'usuario' => htmlentities(utf8_encode($elemento["usuario"]))
	        , 'apellidos' => htmlentities(utf8_encode($elemento["apellidos"]))
	        , 'correo' => htmlentities(utf8_encode($elemento["correo"]))
	        , 'idEstatus' => $elemento["idEstatus"]);
	    }

	    $app->response->headers->set("Content-type", "application/json");
	    $app->response->status(200);
	    $app->response->body(json_encode($respuesta));
	}
	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
	}
});

$app->post("/usuarios/", function() use($app)
{
	$title = $app->request->post("title");
	$isbn = $app->request->post("isbn");
	$author = $app->request->post("author");

	try{
		$connection = getConnection();
		$dbh = $connection->prepare("INSERT INTO books VALUES(null, ?, ?, ?, NOW())");
		$dbh->bindParam(1, $title);
		$dbh->bindParam(2, $isbn);
		$dbh->bindParam(3, $author);
		$dbh->execute();
		$bookId = $connection->lastInsertId();
		$connection = null;
		$app->response->headers->set("Content-type", "application/json");
		$app->response->status(200);
		$app->response->body(json_encode($bookId));
	}
	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
	}
});

$app->put("/usuarios/", function() use($app)
{
	$title = $app->request->put("title");
	$isbn = $app->request->put("isbn");
	$author = $app->request->put("author");
	$id = $app->request->put("id");

	try{
		$connection = getConnection();
		$dbh = $connection->prepare("UPDATE books SET title = ?, isbn = ?, author = ?, created_at = NOW() WHERE id = ?");
		$dbh->bindParam(1, $title);
		$dbh->bindParam(2, $isbn);
		$dbh->bindParam(3, $author);
		$dbh->bindParam(4, $id);
		$dbh->execute();
		$connection = null;
		$app->response->headers->set("Content-type", "application/json");
		$app->response->status(200);
		$app->response->body(json_encode(array("res" => 1)));
	}
	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
	}
});

$app->delete("/usuarios/:id", function($id) use($app)
{
	try{
		$connection = getConnection();
		$dbh = $connection->prepare("DELETE FROM books WHERE id = ?");
		$dbh->bindParam(1, $id);
		$dbh->execute();
		$connection = null;
		$app->response->headers->set("Content-type", "application/json");
		$app->response->status(200);
		$app->response->body(json_encode(array("res" => 1)));
	}
	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
	}
});