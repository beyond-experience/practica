<?php
$app->get("/sitios/:idSitio/movimientos/activos/", function($idSitio) use($app){
  try{
    $idMovimiento=0;
    $connection = getConnection();
    $dbh = $connection->prepare("CALL sp_getMovimientos(?,?)");
    $dbh->bindParam(1, $idSitio);
    $dbh->bindParam(2, $idMovimiento);
    $dbh->execute();
    $elementos = $dbh->fetchAll();
    $connection = null;
    $respuesta = array();
    foreach ($elementos as $elemento) {
      if($elemento["idEstatus"]==1){
        $respuesta[] =  array('idMovimiento' => $elemento["idMovimiento"]
          ,'idEstatus' => $elemento["idEstatus"]
          ,'movimiento' => htmlentities($elemento["movimiento"])
        );
      }
    }

    $app->response->headers->set("Content-type", "application/json");
    $app->response->status(200);
    $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
  }
  catch(PDOException $e){
    echo "Error: " . $e->getMessage();
  }
});

$app->get("/sitios/:idSitio/movimientos/", function($idSitio) use($app){
  try{
    $idMovimiento=0;
    $connection = getConnection();
    $dbh = $connection->prepare("CALL sp_getMovimientos(?,?)");
    $dbh->bindParam(1, $idSitio);
    $dbh->bindParam(2, $idMovimiento);
    $dbh->execute();
    $elementos = $dbh->fetchAll();
    $connection = null;
    $respuesta = array();
    foreach ($elementos as $elemento) {
      $respuesta[] =  array('idMovimiento' => $elemento["idMovimiento"]
          ,'idEstatus' => $elemento["idEstatus"]
          ,'movimiento' => htmlentities($elemento["movimiento"])
        );
    }
    $data= array('data' =>$respuesta  );
    $app->response->headers->set("Content-type", "application/json");
    $app->response->status(200);
    $app->response->body(json_encode($data, JSON_UNESCAPED_UNICODE));
  }
  catch(PDOException $e){
    echo "Error: " . $e->getMessage();
  }
});
$app->get("/sitios/:idSitio/movimientos/:idMovimiento", function($idSitio,$idMovimiento) use($app){
  try{
    $connection = getConnection();
    $dbh = $connection->prepare("CALL sp_getMovimientos(?,?)");
    $dbh->bindParam(1, $idSitio);
    $dbh->bindParam(2, $idMovimiento);
     $dbh->execute();
    $elemento = $dbh->fetch();
    $connection = null;
    $respuesta = array();
    if(!empty($elemento)) {
        $respuesta =  array('idMovimiento' => $elemento["idMovimiento"]
          ,'idEstatus' => $elemento["idEstatus"]
          ,'movimiento' => htmlentities($elemento["movimiento"])
        );
    }

    $app->response->headers->set("Content-type", "application/json");
    $app->response->status(200);
    $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
  }
  catch(PDOException $e){
    echo "Error: " . $e->getMessage();
  }
});