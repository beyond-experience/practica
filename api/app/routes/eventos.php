<?php
$app->get("/bares/:idBar/eventos/web/", function($idBar) use($app){
    try{
        $idEvento=0;
        $connection = getConnection();
        $dbh = $connection->prepare("CALL sp_getEventos(?,?)");
        $dbh->bindParam(1, $idBar);
        $dbh->bindParam(2, $idEvento);
        $dbh->execute();
        $elementos = $dbh->fetchAll();
        $connection = null;
        $respuesta = array();
        foreach ($elementos as $elemento) {
            if ($elemento["principal"]==1)
          $respuesta[] = array('idEvento' => $elemento["idEvento"]
            , 'imagen' => htmlentities(utf8_encode($elemento["imagen"]))
            , 'titulo' => htmlentities(utf8_encode($elemento["titulo"]))
            , 'descripcion' => htmlentities(utf8_encode($elemento["descripcion"]))
            ,'orden' => $elemento["orden"]
            ,'idEstatus' => $elemento["idEstatus"]
            ,'idBar' => $elemento["idBar"]
            ,'principal' => $elemento["principal"]
            );
        }
        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});

$app->get("/bares/:idBar/eventos/", function($idBar) use($app){
    try{
        $idEvento=0;
        $connection = getConnection();
        $dbh = $connection->prepare("CALL sp_getEventos(?,?)");
        $dbh->bindParam(1, $idBar);
        $dbh->bindParam(2, $idEvento);
        $dbh->execute();
        $elementos = $dbh->fetchAll();
        $connection = null;
        $respuesta = array();
        foreach ($elementos as $elemento) {
          $respuesta[] = array('idEvento' => $elemento["idEvento"]
            ,'imagen' => htmlentities(utf8_encode($elemento["imagen"]))
            ,'titulo' => htmlentities(utf8_encode($elemento["titulo"]))
            ,'descripcion' => htmlentities(utf8_encode($elemento["descripcion"]))
            ,'orden' => $elemento["orden"]
            ,'idEstatus' => $elemento["idEstatus"]
            ,'idBar' => $elemento["idBar"]
            ,'principal' => $elemento["principal"]
            );
        }
        $data=array('data'=>$respuesta);
        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($data, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});

$app->get("/bares/:idBar/eventos/:idEvento", function($idBar,$idEvento) use($app){
    try{
        $connection = getConnection();
        $dbh = $connection->prepare("CALL sp_getEventos(?,?)");
        $dbh->bindParam(1, $idBar);
        $dbh->bindParam(2, $idEvento);
        $dbh->execute();
        $elemento = $dbh->fetch();
        $connection = null;
        $respuesta = array();
        if(!empty($elemento)) {
          $respuesta = array('idEvento' => $elemento["idEvento"]
            ,'imagen' => htmlentities(utf8_encode($elemento["imagen"]))
            ,'titulo' => htmlentities(utf8_encode($elemento["titulo"]))
            ,'descripcion' => htmlentities(utf8_encode($elemento["descripcion"]))
            ,'orden' => $elemento["orden"]
            ,'idEstatus' => $elemento["idEstatus"]
            ,'idBar' => $elemento["idBar"]
            ,'principal' => $elemento["principal"]
            );
        }
        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});
$app->post("/bares/:idBar/eventos/", function($idBar) use($app){
    try{
        $connection = getConnection();
        $imagen=utf8_decode($app->request->post('imagen'));
        $titulo=utf8_decode($app->request->post('titulo'));
        $descripcion=utf8_decode($app->request->post('descripcion'));
        $idEstatus=utf8_decode($app->request->post('idEstatus'));
        $principal=utf8_decode($app->request->post('principal'));
        $orden=$app->request->post('orden');
        $dbh = $connection->prepare("CALL sp_addEvento(?,?,?,?,?,?,?)");
        $dbh->bindParam(1, $idBar);
        $dbh->bindParam(2, $imagen);
        $dbh->bindParam(3, $titulo);
        $dbh->bindParam(4, $descripcion);
        $dbh->bindParam(5, $idEstatus);
        $dbh->bindParam(6, $principal);
        $dbh->bindParam(7, $orden);
        $dbh->execute();
        $elemento = $dbh->fetch();
        $connection = null;
        $respuesta = array();
        if(!empty($elemento)) {
          $respuesta = array('respuesta' => $elemento["respuesta"]
            , 'mensaje' => htmlentities(utf8_encode($elemento["mensaje"]))
            );
        }
        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});
$app->options("/bares/:idBar/eventos/:idEvento", function($idBar,$idEvento) {
    //Return response headers
});
$app->put("/bares/:idBar/eventos/:idEvento", function($idBar,$idEvento) use($app){
    try{
        $connection = getConnection();
        $imagen=utf8_decode($app->request->post('imagen'));
        $titulo=utf8_decode($app->request->post('titulo'));
        $descripcion=utf8_decode($app->request->post('descripcion'));
        $idEstatus=utf8_decode($app->request->post('idEstatus'));
        $principal=utf8_decode($app->request->post('principal'));
        $orden=$app->request->post('orden');
        $dbh = $connection->prepare("CALL sp_editEvento(?,?,?,?,?,?,?,?)");
        $dbh->bindParam(1, $idBar);
        $dbh->bindParam(2, $idEvento);
        $dbh->bindParam(3, $imagen);
        $dbh->bindParam(4, $titulo);
        $dbh->bindParam(5, $descripcion);
        $dbh->bindParam(6, $idEstatus);
        $dbh->bindParam(7, $principal);
        $dbh->bindParam(8, $orden);
        $dbh->execute();
        $elemento = $dbh->fetch();
        $connection = null;
        $respuesta = array();
        if(!empty($elemento)) {
          $respuesta = array('respuesta' => $elemento["respuesta"]
            , 'mensaje' => htmlentities(utf8_encode($elemento["mensaje"]))
            );
        }
        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});
