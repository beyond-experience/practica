<?php
$app->get("/sitios/", function() use($app){
  try{
    $idSitio=0;
    $connection = getConnection();
    $dbh = $connection->prepare("CALL sp_getSitios(?)");
    $dbh->bindParam(1, $idSitio);
    $dbh->execute();
    $elementos = $dbh->fetchAll();
    $connection = null;
    $respuesta = array();
    foreach ($elementos as $elemento) {
      if($elemento["idEstatus"]==1){
        $respuesta[] = array('idSitio' => $elemento["idSitio"]
          ,'sitio' => htmlentities($elemento["sitio"])
          ,'url' => htmlentities($elemento["url"])
          ,'carpetaImagenes' => htmlentities($elemento["carpetaImagenes"])
          ,'titulo' => htmlentities($elemento["titulo"])
          ,'tituloSeo' => htmlentities($elemento["tituloSeo"])
          ,'descripcion' => htmlentities($elemento["descripcion"])
          ,'descripcionSeo' => htmlentities($elemento["descripcionSeo"])
          ,'urlFacebook' => htmlentities($elemento["urlFacebook"])
          ,'urlGoogle' => htmlentities($elemento["urlGoogle"])
          ,'telefono' => htmlentities($elemento["telefono"])
          ,'correo' => htmlentities($elemento["correo"])
          ,'idEstado' => htmlentities($elemento["idEstado"])
          ,'idPais' => $elemento["idPais"]
          ,'urlTwitter' => htmlentities($elemento["urlTwitter"])
          ,'direccion' => htmlentities($elemento["direccion"])
          ,'estado' => htmlentities($elemento["estado"])
          ,'pais' => htmlentities($elemento["pais"])
          ,'cp' => htmlentities($elemento["cp"])
          ,'urlInstagram' => htmlentities($elemento["urlInstagram"])
          ,'urlPinterest' => htmlentities($elemento["urlPinterest"]) 
          ,'idEstatus' => $elemento["idEstatus"]
          ,'appIdFacebook' => htmlentities($elemento["appIdFacebook"])
          ,'adminsFacebook' => htmlentities($elemento["adminsFacebook"]) 
          ,'keywords' => htmlentities($elemento["keywords"]) 
          ,'copy' => htmlentities($elemento["copy"]) 
          ,'autor' => htmlentities($elemento["autor"]) 
        );
      }
    }
    $app->response->headers->set("Content-type", "application/json");
    $app->response->status(200);
    $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
  }
  catch(PDOException $e){
    echo "Error: " . $e->getMessage();
  }
});

$app->get("/sitios/:idSitio/", function($idSitio) use($app){
  try{
    $connection = getConnection();
    $dbh = $connection->prepare("CALL sp_getSitios(?)");
    $dbh->bindParam(1, $idSitio);
    $dbh->execute();
    $elemento = $dbh->fetch();
    $connection = null;
    $respuesta = array();
    if(!empty($elemento)) {
      $respuesta= array('idSitio' => $elemento["idSitio"]
        ,'sitio' => htmlentities($elemento["sitio"])
        ,'url' => htmlentities($elemento["url"])
        ,'carpetaImagenes' => htmlentities($elemento["carpetaImagenes"])
        ,'titulo' => htmlentities($elemento["titulo"])
        ,'tituloSeo' => htmlentities($elemento["tituloSeo"])
        ,'descripcion' => htmlentities($elemento["descripcion"])
        ,'descripcionSeo' => htmlentities($elemento["descripcionSeo"])
        ,'urlFacebook' => htmlentities($elemento["urlFacebook"])
        ,'urlGoogle' => htmlentities($elemento["urlGoogle"])
        ,'telefono' => htmlentities($elemento["telefono"])
        ,'correo' => htmlentities($elemento["correo"])
        ,'idEstado' => htmlentities($elemento["idEstado"])
        ,'idPais' => $elemento["idPais"]
        ,'urlTwitter' => htmlentities($elemento["urlTwitter"])
        ,'direccion' => htmlentities($elemento["direccion"])
        ,'estado' => htmlentities($elemento["estado"])
        ,'pais' => htmlentities($elemento["pais"])
        ,'cp' => htmlentities($elemento["cp"])
        ,'urlInstagram' => htmlentities($elemento["urlInstagram"])
        ,'urlPinterest' => htmlentities($elemento["urlPinterest"]) 
        ,'idEstatus' => $elemento["idEstatus"]
        ,'appIdFacebook' => htmlentities($elemento["appIdFacebook"])
        ,'adminsFacebook' => htmlentities($elemento["adminsFacebook"]) 
        ,'copy' => htmlentities($elemento["copy"]) 
        ,'keywords' => htmlentities($elemento["keywords"])
        ,'autor' => htmlentities($elemento["autor"]) 
      );
    }

    $app->response->headers->set("Content-type", "application/json");
    $app->response->status(200);
    $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
  }
  catch(PDOException $e){
    echo "Error: " . $e->getMessage();
  }
});

$app->get("/sitios/:idSitio/idiomas/", function($idSitio) use($app){
 try{
    $idSlider=0;
    $connection = getConnection();
    $dbh = $connection->prepare("CALL sp_getIdiomas()");

    $dbh->execute();
    $elementos = $dbh->fetchAll();
    $connection = null;
    $respuesta = array();
    foreach ($elementos as $elemento) {
      if($elemento["idEstatus"]==1){
        $respuesta[] = array('idIdioma' => $elemento["idIdioma"]
           ,'idioma' => htmlentities($elemento["nombre"])
           ,'idEstatus' => $elemento["idEstatus"]
            ,'urlImagen' => htmlentities($elemento["urlImagen"])
        );
      }

    }
    $app->response->headers->set("Content-type", "application/json");
    $app->response->status(200);
    $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
  }
  catch(PDOException $e){
    echo "Error: " . $e->getMessage();
  }
});

$app->get("/sitios/:idSitio/paises/", function($idSitio) use($app){
  try{
    $connection = getConnection();
    $dbh = $connection->prepare("CALL sp_getPaises()");
    $dbh->execute();
    $elementos = $dbh->fetchAll();
    $connection = null;
    $respuesta = array();
    foreach ($elementos as $elemento) {
      if($elemento["idEstatus"]==1){
        $respuesta[]= array('idPais' => $elemento["idPais"]
          ,'idEstatus' => $elemento["idEstatus"]
          ,'pais' => htmlentities($elemento["pais"])
          ,'codigo' => htmlentities($elemento["codigo"])
          ,'claveTelefono' => htmlentities($elemento["claveTelefono"])
        );
      }
    }
    $app->response->headers->set("Content-type", "application/json");
    $app->response->status(200);
    $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
  }
  catch(PDOException $e){
    echo "Error: " . $e->getMessage();
  }
});
$app->get("/sitios/:idSitio/paises/:idPais/estados/", function($idSitio,$idPais) use($app){
  try{
    $connection = getConnection();
    $dbh = $connection->prepare("CALL sp_getEstados(?)");
    $dbh->bindParam(1, $idPais);
    $dbh->execute();
    $elementos = $dbh->fetchAll();
    $connection = null;
    $respuesta = array();
    foreach ($elementos as $elemento) {
        $respuesta[]= array('idEstado' => $elemento["idEstado"]
          ,'estado' => htmlentities($elemento["estado"])
          ,'iso' => htmlentities($elemento["iso"])
        );
      }

    $app->response->headers->set("Content-type", "application/json");
    $app->response->status(200);
    $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
  }
  catch(PDOException $e){
    echo "Error: " . $e->getMessage();
  }
});


$app->options("/sitios/:idSitio/", function($idSitio) {
    //Return response headers
});

$app->put("/sitios/:idSitio/", function($idSitio) use($app){
  try{
    $connection = getConnection();
    $sitio=$app->request->post('sitio');
    $url=$app->request->post('url');
    $titulo=$app->request->post('titulo');
    $tituloSeo=$app->request->post('tituloSeo');
    $descripcion=$app->request->post('descripcion');
    $descripcionSeo=$app->request->post('descripcionSeo');
    $urlFacebook =$app->request->post('urlFacebook');
    $urlGoogle =$app->request->post('urlGoogle');
    $idEstatus =$app->request->post('idEstatus');
    $telefono =$app->request->post('telefono');
    $correo =$app->request->post('correo');
    $direccion =$app->request->post('direccion');
    $idEstado =$app->request->post('idEstado');
    $idPais =$app->request->post('idPais');
    $urlTwitter =$app->request->post('urlTwitter');
    $cp =$app->request->post('cp');
    $urlInstagram =$app->request->post('urlInstagram');
    $urlPinterest =$app->request->post('urlPinterest');
    $carpetaImagenes=$app->request->post('carpetaImagenes'); 
    $appIdFacebook =$app->request->post('appIdFacebook');
    $adminsFacebook =$app->request->post('adminsFacebook');
    $copy =$app->request->post('copy');
    $autor =$app->request->post('autor');
    $keywords =$app->request->post('keywords');

      $dbh = $connection->prepare("CALL sp_editSitio(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
      $dbh->bindParam(1, $idSitio);
      $dbh->bindParam(2, $sitio);
      $dbh->bindParam(3, $url);
      $dbh->bindParam(4, $titulo);
      $dbh->bindParam(5, $tituloSeo);
      $dbh->bindParam(6, $descripcion);
      $dbh->bindParam(7, $descripcionSeo);
      $dbh->bindParam(8, $urlFacebook);
      $dbh->bindParam(9, $urlGoogle);
      $dbh->bindParam(10, $idEstatus);

      $dbh->bindParam(11, $telefono);
      $dbh->bindParam(12, $correo);
      $dbh->bindParam(13, $direccion);
      $dbh->bindParam(14, $idEstado);
      $dbh->bindParam(15, $idPais);
      $dbh->bindParam(16, $urlTwitter);
      $dbh->bindParam(17, $cp);
      $dbh->bindParam(18, $urlInstagram);
      $dbh->bindParam(19, $urlPinterest);
      $dbh->bindParam(20, $carpetaImagenes);

      $dbh->bindParam(21, $appIdFacebook);
      $dbh->bindParam(22, $adminsFacebook);
      $dbh->bindParam(23, $copy);
      $dbh->bindParam(24, $autor);
      $dbh->bindParam(25, $keywords);

      $dbh->execute();
      
      $elemento = $dbh->fetch();
      $connection = null;
      $respuesta = array();
     // if(!empty($elemento)) {
        $respuesta = array('respuesta' => $elemento["respuesta"]
          , 'mensaje' => htmlentities($elemento["mensaje"])
          );
     /* }else{
         $respuesta = array('respuesta' => -99
          , 'mensaje' => htmlentities('error de actualizacion')
          );
      }*/
      $app->response->headers->set("Content-type", "application/json");
      $app->response->status(200);
      $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});

