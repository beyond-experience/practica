<?php 
$app->get("/sitios/:idSitio/marcas/activas/", function($idSitio) use($app){
  try{
    $idMarca=0;
    $connection = getConnection();
    $dbh = $connection->prepare("CALL sp_getMarcas(?,?)");
    $dbh->bindParam(1, $idSitio);
    $dbh->bindParam(2, $idMarca);
    $dbh->execute();
    $elementos = $dbh->fetchAll();
    $connection = null;
    $respuesta = array();
    foreach ($elementos as $elemento) {
      if($elemento["idEstatus"]==1){
        $respuesta[] = array('idMarca' => $elemento["idMarca"]
          ,'idEstatus' => $elemento["idEstatus"]
          ,'urlImagen' => htmlentities($elemento["urlImagen"])
          ,'urlSeo' => htmlentities($elemento["urlSeo"])
          ,'marca' => htmlentities($elemento["marca"])
        );
      }
    }

    $app->response->headers->set("Content-type", "application/json");
    $app->response->status(200);
    $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
  }
  catch(PDOException $e){
    echo "Error: " . $e->getMessage();
  }
});

$app->get("/sitios/:idSitio/marcas/", function($idSitio) use($app){
  try{
    $idMarca=0;
    $connection = getConnection();
    $dbh = $connection->prepare("CALL sp_getMarcas(?,?)");
    $dbh->bindParam(1, $idSitio);
    $dbh->bindParam(2, $idMarca);
    $dbh->execute();
    $elementos = $dbh->fetchAll();
    $connection = null;
    $respuesta = array();
    foreach ($elementos as $elemento) {
      $respuesta[] = array('idMarca' => $elemento["idMarca"]
        ,'idEstatus' => $elemento["idEstatus"]
        ,'urlImagen' => htmlentities($elemento["urlImagen"])
        ,'urlSeo' => htmlentities($elemento["urlSeo"])
        ,'marca' => htmlentities($elemento["marca"])
        ,'titulo' => htmlentities($elemento["titulo"])
        ,'descripcion' => htmlentities($elemento["descripcion"])
      );
    }
    $data= array('data' =>$respuesta  );
    $app->response->headers->set("Content-type", "application/json");
    $app->response->status(200);
    $app->response->body(json_encode($data, JSON_UNESCAPED_UNICODE));
  }
  catch(PDOException $e){
    echo "Error: " . $e->getMessage();
  }
});
$app->get("/sitios/:idSitio/marcas/:idMarca", function($idSitio,$idMarca) use($app){
  try{
    $connection = getConnection();
    $dbh = $connection->prepare("CALL sp_getMarcas(?,?)");
    $dbh->bindParam(1, $idSitio);
    $dbh->bindParam(2, $idMarca);
     $dbh->execute();
    $elemento = $dbh->fetch();
    $connection = null;
    $respuesta = array();
    if(!empty($elemento)) {
        $respuesta = array('idMarca' => $elemento["idMarca"]
          ,'idEstatus' => $elemento["idEstatus"]
          ,'urlImagen' => htmlentities($elemento["urlImagen"])
          ,'urlSeo' => htmlentities($elemento["urlSeo"])
          ,'marca' => htmlentities($elemento["marca"])
          ,'titulo' => htmlentities($elemento["titulo"])
          ,'descripcion' => htmlentities($elemento["descripcion"])
        );
    }

    $app->response->headers->set("Content-type", "application/json");
    $app->response->status(200);
    $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
  }
  catch(PDOException $e){
    echo "Error: " . $e->getMessage();
  }
});

$app->post("/sitios/:idSitio/marcas/", function($idSitio) use($app){
 try{

    $connection = getConnection(); 
    $idEstatus=$app->request->post('idEstatus');
    $marca=$app->request->post('marca');
    $urlImagen=$app->request->post('urlImagen');
		$urlSeo=$app->request->post('urlSeo');
		$titulo=$app->request->post('titulo');
		$descripcion=$app->request->post('descripcion');
   
    $dbh = $connection->prepare("CALL sp_addMarca(?,?,?,?,?,?,?)");
    $dbh->bindParam(1, $idSitio);
    $dbh->bindParam(2, $idEstatus);
    $dbh->bindParam(3, $marca);
    $dbh->bindParam(4, $urlImagen);
    $dbh->bindParam(5, $urlSeo);
    $dbh->bindParam(6, $titulo);
    $dbh->bindParam(7, $descripcion);
    $dbh->execute();
    $elemento = $dbh->fetch();
    $connection = null;
    $respuesta = array();
    if(!empty($elemento)) {
      $respuesta = array('respuesta' => $elemento["respuesta"]
        , 'mensaje' => htmlentities($elemento["mensaje"])
        );
    }
    $data=array('data'=>$respuesta);
    $app->response->headers->set("Content-type", "application/json");
    $app->response->status(200);
    $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
  }
  catch(PDOException $e){
      echo "Error: " . $e->getMessage();
  }
});

$app->options("/sitios/:idSitio/marcas/:idMarcas", function($idSitio,$idMarcas)use($app) {
    //Return response headers
});

$app->put("/sitios/:idSitio/marcas/:idMarcas", function($idSitio,$idMarcas) use($app){
 	try{

		$connection = getConnection(); 
		$idEstatus=$app->request->post('idEstatus');
    $marca=$app->request->post('marca');
    $urlImagen=$app->request->post('urlImagen');
		$urlSeo=$app->request->post('urlSeo');
		$titulo=$app->request->post('titulo');
		$descripcion=$app->request->post('descripcion');
		$dbh = $connection->prepare("CALL sp_editMarca(?,?,?,?,?,?,?,?)");
		$dbh->bindParam(1, $idSitio);
		$dbh->bindParam(2, $idMarcas);
    $dbh->bindParam(3, $idEstatus);
    $dbh->bindParam(4, $marca);
    $dbh->bindParam(5, $urlImagen);
    $dbh->bindParam(6, $urlSeo);
    $dbh->bindParam(7, $titulo);
    $dbh->bindParam(8, $descripcion);
        
    $dbh->execute();
    $elemento = $dbh->fetch();
    $connection = null;
    $respuesta = array();
    if(!empty($elemento)) {
      $respuesta = array('respuesta' => $elemento["respuesta"]
        , 'mensaje' => htmlentities($elemento["mensaje"])
        );
    }
    $data=array('data'=>$respuesta);
    $app->response->headers->set("Content-type", "application/json");
    $app->response->status(200);
    $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
  }
  catch(PDOException $e){
      echo "Error: " . $e->getMessage();
  }
});

$app->delete("/sitios/:idSitio/marcas/:idMarcas", function($idSitio,$idMarcas) use($app){
    try{
        $connection = getConnection();
        $dbh = $connection->prepare("CALL sp_deleteMarca(?,?)");
        $dbh->bindParam(1, $idSalon);
        $dbh->bindParam(2, $idMarcas);
        $dbh->execute();
        $elemento = $dbh->fetch();
        $connection = null;
        $respuesta = array();
        if(!empty($elemento)) {
          $respuesta = array('respuesta' => $elemento["respuesta"]
            , 'mensaje' => htmlentities($elemento["mensaje"])
            );
        }
        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});

