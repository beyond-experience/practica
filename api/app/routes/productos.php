<?php
$app->get("/sitios/:idSitio/productos/home/", function($idSitio) use($app){
    try{
        $idProducto=0;
        $connection = getConnection();
        $dbh = $connection->prepare("CALL sp_getProductos(?,?)");
        $dbh->bindParam(1, $idSitio);
        $dbh->bindParam(2, $idProducto);
        $dbh->execute();
        $elementos = $dbh->fetchAll();
        $connection = null;
        $respuesta = array();
        foreach ($elementos as $elemento) {
          if($elemento["principal"]==1 && $elemento["idEstatus"]==1)
          $respuesta[]= array('idProducto' => $elemento["idProducto"]
            ,'idCategoria' => $elemento["idCategoria"]
            , 'idEstatus' => $elemento["idEstatus"]
            ,'idModelo' => $elemento["idModelo"]
            ,'principal' => $elemento["principal"]
            ,'idMovimiento' => $elemento["idMovimiento"]
            ,'idMarca' => $elemento["idMarca"]
            , 'idCristal' => $elemento["idCristal"]
            , 'idMaterial' => $elemento["idMaterial"]
             ,'piezas' => $elemento["piezas"]
            , 'producto' => htmlentities($elemento["producto"])
            , 'fechaHora' => htmlentities($elemento["fechaHora"])
            , 'diametro' => htmlentities($elemento["diametro"])
            , 'descripcion' => htmlentities($elemento["descripcion"])
            , 'urlImagen' => htmlentities($elemento["urlImagen"])
            , 'urlSeo' => htmlentities($elemento["urlSeo"])
            , 'modelo' => htmlentities($elemento["modelo"])
            , 'marca' => htmlentities($elemento["marca"])
            , 'precioPublico' => $elemento["precioPublico"]
            , 'precioNeto' => $elemento["precioNeto"]
             , 'precioOferta' => $elemento["precioOferta"]
            , 'clave' => htmlentities($elemento["clave"])
            , 'movimiento' => $elemento["movimiento"]
            , 'categoria' => htmlentities($elemento["categoria"])
            , 'bisel' => htmlentities($elemento["bisel"])
            , 'accesorios' => htmlentities($elemento["accesorios"])
            , 'cristal' => htmlentities($elemento["cristal"])
            , 'condicion' => htmlentities($elemento["condicion"])
            , 'brazalete' => htmlentities($elemento["brazalete"])
            , 'categoriaUrl' => htmlentities($elemento["categoriaUrl"])
            );
        }
        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});
$app->get("/sitios/:idSitio/productos/activos", function($idSitio) use($app){
    try{
        $idProducto=0;
        $connection = getConnection();
        $dbh = $connection->prepare("CALL sp_getProductos(?,?)");
        $dbh->bindParam(1, $idSitio);
        $dbh->bindParam(2, $idProducto);
        $dbh->execute();
        $elementos = $dbh->fetchAll();
        $connection = null;
        $respuesta = array();
        foreach ($elementos as $elemento) {
          if($elemento["idEstatus"]==1)
          $respuesta[]= array('idProducto' => $elemento["idProducto"]
            ,'idCategoria' => $elemento["idCategoria"]
            , 'idEstatus' => $elemento["idEstatus"]
            ,'idModelo' => $elemento["idModelo"]
            ,'principal' => $elemento["principal"]
            ,'idMovimiento' => $elemento["idMovimiento"]
            ,'idMarca' => $elemento["idMarca"]
            , 'idCristal' => $elemento["idCristal"]
            , 'idMaterial' => $elemento["idMaterial"]
             ,'piezas' => $elemento["piezas"]
            , 'producto' => htmlentities($elemento["producto"])
            , 'fechaHora' => htmlentities($elemento["fechaHora"])
            , 'diametro' => htmlentities($elemento["diametro"])
            , 'descripcion' => htmlentities($elemento["descripcion"])
            , 'urlImagen' => htmlentities($elemento["urlImagen"])
            , 'urlSeo' => htmlentities($elemento["urlSeo"])
            , 'modelo' => htmlentities($elemento["modelo"])
            , 'marca' => htmlentities($elemento["marca"])
            , 'precioPublico' => $elemento["precioPublico"]
            , 'precioNeto' => $elemento["precioNeto"]
            , 'precioOferta' => $elemento["precioOferta"]
            , 'clave' => htmlentities($elemento["clave"])
            , 'movimiento' => $elemento["movimiento"]
             , 'bisel' => htmlentities($elemento["bisel"])
            , 'accesorios' => htmlentities($elemento["accesorios"])
            , 'cristal' => htmlentities($elemento["cristal"])
            , 'condicion' => htmlentities($elemento["condicion"])
            , 'brazalete' => htmlentities($elemento["brazalete"])
            , 'categoria' => htmlentities($elemento["categoria"])
            , 'categoriaUrl' => htmlentities($elemento["categoriaUrl"])
            );
        }
        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});

$app->get("/sitios/:idSitio/productos/", function($idSitio) use($app){
    try{
        $idProducto=0;
        $connection = getConnection();
        $dbh = $connection->prepare("CALL sp_getProductos(?,?)");
        $dbh->bindParam(1, $idSitio);
        $dbh->bindParam(2, $idProducto);
        $dbh->execute();
        $elementos = $dbh->fetchAll();
        $connection = null;
        $respuesta = array();
        foreach ($elementos as $elemento) {
          $respuesta[]= array('idProducto' => $elemento["idProducto"]
            ,'idCategoria' => $elemento["idCategoria"]
            , 'idEstatus' => $elemento["idEstatus"]
            ,'idModelo' => $elemento["idModelo"]
            ,'principal' => $elemento["principal"]
            ,'idMovimiento' => $elemento["idMovimiento"]
            ,'idMarca' => $elemento["idMarca"]
            , 'idCristal' => $elemento["idCristal"]
            , 'idMaterial' => $elemento["idMaterial"]
             ,'piezas' => $elemento["piezas"]
            , 'producto' => htmlentities($elemento["producto"])
            , 'fechaHora' => htmlentities($elemento["fechaHora"])
            , 'diametro' => htmlentities($elemento["diametro"])
            , 'descripcion' => htmlentities($elemento["descripcion"])
            , 'urlImagen' => htmlentities($elemento["urlImagen"])
            , 'urlSeo' => htmlentities($elemento["urlSeo"])
            , 'modelo' => htmlentities($elemento["modelo"])
            , 'marca' => htmlentities($elemento["marca"])
            , 'precioPublico' => $elemento["precioPublico"]
            , 'precioNeto' => $elemento["precioNeto"]
            , 'precioOferta' => $elemento["precioOferta"]
            , 'clave' => htmlentities($elemento["clave"])
            , 'movimiento' => $elemento["movimiento"]
            , 'bisel' => htmlentities($elemento["bisel"])
            , 'accesorios' => htmlentities($elemento["accesorios"])
            , 'cristal' => htmlentities($elemento["cristal"])
            , 'condicion' => htmlentities($elemento["condicion"])
            , 'brazalete' => htmlentities($elemento["brazalete"])
            , 'categoria' => htmlentities($elemento["categoria"])
            , 'categoriaUrl' => htmlentities($elemento["categoriaUrl"])
            );
        }
        $data=array("data"=>$respuesta);
        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($data, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});

$app->get("/sitios/:idSitio/productos/categoria/:idCategoria/activos/", function($idSitio,$idCategoria) use($app){
    try{
        $connection = getConnection();
        $dbh = $connection->prepare("CALL sp_getProductosCategoria(?,?)");
        $dbh->bindParam(1, $idSitio);
        $dbh->bindParam(2, $idCategoria);
        $dbh->execute();
        $elementos = $dbh->fetchAll();
        $connection = null;
        $respuesta = array();
        foreach ($elementos as $elemento) {
          if($elemento["idEstatus"]==1)
          $respuesta[]= array('idProducto' => $elemento["idProducto"]
            ,'idCategoria' => $elemento["idCategoria"]
            , 'idEstatus' => $elemento["idEstatus"]
            ,'idModelo' => $elemento["idModelo"]
            ,'principal' => $elemento["principal"]
            ,'idMovimiento' => $elemento["idMovimiento"]
            ,'idMarca' => $elemento["idMarca"]
            , 'idCristal' => $elemento["idCristal"]
            , 'idMaterial' => $elemento["idMaterial"]
             ,'piezas' => $elemento["piezas"]
            , 'producto' => htmlentities($elemento["producto"])
            , 'fechaHora' => htmlentities($elemento["fechaHora"])
            , 'diametro' => htmlentities($elemento["diametro"])
            , 'descripcion' => htmlentities($elemento["descripcion"])
            , 'urlImagen' => htmlentities($elemento["urlImagen"])
            , 'urlSeo' => htmlentities($elemento["urlSeo"])
            , 'modelo' => htmlentities($elemento["modelo"])
            , 'marca' => htmlentities($elemento["marca"])
            , 'precioPublico' => $elemento["precioPublico"]
            , 'precioNeto' => $elemento["precioNeto"]
            , 'precioOferta' => $elemento["precioOferta"]
            , 'clave' => htmlentities($elemento["clave"])
            , 'movimiento' => $elemento["movimiento"]
            , 'bisel' => htmlentities($elemento["bisel"])
            , 'accesorios' => htmlentities($elemento["accesorios"])
            , 'cristal' => htmlentities($elemento["cristal"])
            , 'condicion' => htmlentities($elemento["condicion"])
            , 'brazalete' => htmlentities($elemento["brazalete"])
            , 'categoria' => htmlentities($elemento["categoria"])
            , 'categoriaUrl' => htmlentities($elemento["categoriaUrl"])
            );
        }
        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});
$app->get("/sitios/:idSitio/productos/categoria/:idCategoria/tienda/", function($idSitio,$idCategoria) use($app){
    try{
        $connection = getConnection();
        $dbh = $connection->prepare("CALL sp_getProductosCategoria(?,?)");
        $dbh->bindParam(1, $idSitio);
        $dbh->bindParam(2, $idCategoria);
        $dbh->execute();
        $elementos = $dbh->fetchAll();
        $connection = null;
        $respuesta = array("productos"=>array(),"filtros"=>array());
        $filtros=array("marcas"=>array(),"cristales"=>array(),"movimientos"=>array(),"modelos"=>array(),"materiales"=>array());
        foreach ($elementos as $elemento) {
          if($elemento["idEstatus"]==1){
          $respuesta["productos"][]= array('idProducto' => $elemento["idProducto"]
            ,'idCategoria' => $elemento["idCategoria"]
            ,'idEstatus' => $elemento["idEstatus"]
            ,'idModelo' => $elemento["idModelo"]
            ,'principal' => $elemento["principal"]
            ,'idMovimiento' => $elemento["idMovimiento"]
            ,'idMarca' => $elemento["idMarca"]
            ,'idCristal' => $elemento["idCristal"]
            ,'idMaterial' => $elemento["idMaterial"]
            ,'piezas' => $elemento["piezas"]
            ,'producto' => htmlentities($elemento["producto"])
            ,'fechaHora' => htmlentities($elemento["fechaHora"])
            ,'diametro' => htmlentities($elemento["diametro"])
            ,'descripcion' => htmlentities($elemento["descripcion"])
            ,'urlImagen' => htmlentities($elemento["urlImagen"])
            ,'urlSeo' => htmlentities($elemento["urlSeo"])
            ,'modelo' => htmlentities($elemento["modelo"])
            ,'marca' => htmlentities($elemento["marca"])
            ,'precioPublico' => $elemento["precioPublico"]
            ,'precioNeto' => $elemento["precioNeto"]
            ,'precioOferta' => $elemento["precioOferta"]
            ,'clave' => htmlentities($elemento["clave"])
            ,'material' => htmlentities($elemento["material"])
            ,'movimiento' => $elemento["movimiento"]
            ,'bisel' => htmlentities($elemento["bisel"])
            ,'accesorios' => htmlentities($elemento["accesorios"])
            ,'cristal' => htmlentities($elemento["cristal"])
            ,'condicion' => htmlentities($elemento["condicion"])
            ,'brazalete' => htmlentities($elemento["brazalete"])
            ,'categoria' => htmlentities($elemento["categoria"])
            ,'categoriaUrl' => htmlentities($elemento["categoriaUrl"])
            );
            $marca=array("idMarca"=>$elemento["idMarca"],"marca"=>$elemento["marca"],"contador"=>0);
            if(array_search($elemento["idMarca"] ,array_column($filtros["marcas"],"idMarca"))===false){
                $filtros["marcas"][]=$marca;
            }
            $cristal=array("idCristal"=>$elemento["idCristal"],"cristal"=>$elemento["cristal"],"contador"=>0);
            if(array_search($elemento["idCristal"] ,array_column($filtros["cristales"],"idCristal"))===false){
                $filtros["cristales"][]=$cristal;
            }
            $movimiento=array("idMovimiento"=>$elemento["idMovimiento"],"movimiento"=>$elemento["movimiento"]);
            if(array_search($elemento["idMovimiento"] ,array_column($filtros["movimientos"],"idMovimiento"))===false){
                $filtros["movimientos"][]=$movimiento;
            }
             $material=array("idMaterial"=>$elemento["idMaterial"],"material"=>$elemento["material"],"contador"=>0);
            if(array_search($elemento["idMaterial"] ,array_column($filtros["materiales"],"idMaterial"))===false){
                $filtros["materiales"][]=$material;
            }
            $modelo=array("idModelo"=>$elemento["idModelo"],"modelo"=>$elemento["modelo"],"contador"=>0);
            if(array_search($elemento["idModelo"] ,array_column($filtros["modelos"],"idModelo"))===false){
                $filtros["modelos"][]=$modelo;
            }
         }
        }
            $respuesta["filtros"]=$filtros;
 

        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});
$app->get("/sitios/:idSitio/productos/categoria/:idCategoria", function($idSitio,$idCategoria) use($app){
    try{
        $connection = getConnection();
        $dbh = $connection->prepare("CALL sp_getProductosCategoria(?,?)");
        $dbh->bindParam(1, $idSitio);
        $dbh->bindParam(2, $idCategoria);
        $dbh->execute();
        $elementos = $dbh->fetchAll();
        $connection = null;
        $respuesta = array();
        foreach ($elementos as $elemento) {
          $respuesta[]= array('idProducto' => $elemento["idProducto"]
            ,'idCategoria' => $elemento["idCategoria"]
            , 'idEstatus' => $elemento["idEstatus"]
            ,'idModelo' => $elemento["idModelo"]
            ,'principal' => $elemento["principal"]
            ,'idMovimiento' => $elemento["idMovimiento"]
            ,'idMarca' => $elemento["idMarca"]
            , 'idCristal' => $elemento["idCristal"]
            , 'idMaterial' => $elemento["idMaterial"]
             ,'piezas' => $elemento["piezas"]
            , 'producto' => htmlentities($elemento["producto"])
            , 'fechaHora' => htmlentities($elemento["fechaHora"])
            , 'diametro' => htmlentities($elemento["diametro"])
            , 'descripcion' => htmlentities($elemento["descripcion"])
            , 'urlImagen' => htmlentities($elemento["urlImagen"])
            , 'urlSeo' => htmlentities($elemento["urlSeo"])
            , 'modelo' => htmlentities($elemento["modelo"])
            , 'marca' => htmlentities($elemento["marca"])
            , 'precioPublico' => $elemento["precioPublico"]
            , 'precioNeto' => $elemento["precioNeto"]
            , 'precioOferta' => $elemento["precioOferta"]
            , 'clave' => htmlentities($elemento["clave"])
            , 'movimiento' => $elemento["movimiento"]
            , 'bisel' => htmlentities($elemento["bisel"])
            , 'accesorios' => htmlentities($elemento["accesorios"])
            , 'cristal' => htmlentities($elemento["cristal"])
            , 'condicion' => htmlentities($elemento["condicion"])
            , 'brazalete' => htmlentities($elemento["brazalete"])
            , 'categoria' => htmlentities($elemento["categoria"])
            , 'categoriaUrl' => htmlentities($elemento["categoriaUrl"])
            );
        }
        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});
$app->get("/sitios/:idSitio/productos/:idProducto/galeria/", function($idSitio,$idProducto) use($app){
    try{
        $idGaleria=0;
        $connection = getConnection();
        $dbh = $connection->prepare("CALL sp_getGaleriaProducto(?,?,?)");
        $dbh->bindParam(1, $idSitio);
        $dbh->bindParam(2, $idProducto);
        $dbh->bindParam(3, $idGaleria);
        $dbh->execute();
        $elementos = $dbh->fetchAll();
        $connection = null;
        $respuesta = array();
        foreach ($elementos as $elemento) {
          $respuesta[]= array('idGaleria' => $elemento["idGaleria"]
            , 'urlImagen' => htmlentities($elemento["urlImagen"])
            , 'titulo' => htmlentities($elemento["titulo"])
            , 'descripcionSeo' => htmlentities($elemento["descripcionSeo"])
            , 'idProducto'=>$elemento["idProducto"]
            , 'tituloSeo' => htmlentities($elemento["tituloSeo"])
            );
        }
        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});

$app->get("/sitios/:idSitio/productos/:idProducto/galeria/:idGaleria", function($idSitio,$idProducto,$idGaleria) use($app){
    try{
        $connection = getConnection();
        $dbh = $connection->prepare("CALL sp_getGaleriaProducto(?,?,?)");
        $dbh->bindParam(1, $idSitio);
        $dbh->bindParam(2, $idProducto);
        $dbh->execute();
        $elementos = $dbh->fetchAll();
        $connection = null;
        $respuesta = array();
        foreach ($elementos as $elemento) {
          $respuesta[]= array('idGaleria' => $elemento["idGaleria"]
            , 'urlImagen' => htmlentities($elemento["urlImagen"])
            , 'titulo' => htmlentities($elemento["titulo"])
            , 'descripcionSeo' => htmlentities($elemento["descripcionSeo"])
            , 'tituloSeo' => htmlentities($elemento["tituloSeo"])
            );
        }
        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});
$app->post("/sitios/:idSitio/productos/:idProducto/galeria/", function($idSitio,$idProducto) use($app){
 try{

        $connection = getConnection();
        $urlImagen=$app->request->post('urlImagen');
        $idEstatus=$app->request->post('idEstatus');
        $titulo=$app->request->post('titulo');
        $descripcionSeo=$app->request->post('descripcionSeo');
        $tituloSeo=$app->request->post('tituloSeo');
        if (empty($urlImagen))
            $urlImagen='expertos-en-relojes-producto-default.jpg';
        if (empty($idModelo))
            $idModelo=0;
 
        $dbh = $connection->prepare("CALL sp_addProductoGaleria(?,?,?,?,?,?,?)");
        $dbh->bindParam(1, $idSitio);
        $dbh->bindParam(2, $idProducto);
        $dbh->bindParam(3, $urlImagen);
        $dbh->bindParam(4, $idEstatus);
        $dbh->bindParam(5, $titulo);
        $dbh->bindParam(6, $descripcionSeo);
        $dbh->bindParam(7, $titulo);
        $dbh->execute();
        $elemento = $dbh->fetch();
        $connection = null;
        $respuesta = array();
        if(!empty($elemento)) {


          $respuesta = array('respuesta' => $elemento["respuesta"]
            , 'mensaje' => htmlentities($elemento["mensaje"])
            );
        }
        $data=array('data'=>$respuesta);
        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});
$app->options("/sitios/:idSitio/productos/:idProducto/galeria/:idGaleria", function($idSitio,$idProducto,$idGaleria) use($app){
});

$app->put("/sitios/:idSitio/productos/:idProducto/galeria/:idGaleria", function($idSitio,$idProducto,$idGaleria) use($app){
    try{
        $connection = getConnection(); 
        $urlImagen=$app->request->post('urlImagen');
        $idEstatus= 1;//$app->request->post('idEstatus');
        $titulo=$app->request->post('titulo');
        $descripcionSeo=$app->request->post('descripcionSeo');
        $tituloSeo=$app->request->post('tituloSeo');
        $dbh = $connection->prepare("CALL sp_editProductoGaleria(?,?,?,?,?,?,?,?)");
        $dbh->bindParam(1, $idSitio);
        $dbh->bindParam(2, $idProducto);
        $dbh->bindParam(3, $idGaleria);
        $dbh->bindParam(4, $urlImagen);
        $dbh->bindParam(5, $idEstatus);
        $dbh->bindParam(6, $titulo);
        $dbh->bindParam(7, $descripcionSeo);
        $dbh->bindParam(8, $titulo);

        $dbh->execute();
        $elemento = $dbh->fetch();
        $connection = null;
        $respuesta = array();
        if(!empty($elemento)) {
          $respuesta = array('respuesta' => $elemento["respuesta"]
            , 'mensaje' => htmlentities($elemento["mensaje"])
            );
        }
        $data=array('data'=>$respuesta);
        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});
$app->delete("/sitios/:idSitio/productos/:idProducto/galeria/:idGaleria", function($idSitio,$idProducto,$idGaleria) use($app){
     try{
        $connection = getConnection();
        $dbh = $connection->prepare("CALL sp_deleteProductoGaleria(?,?,?)");
        $dbh->bindParam(1, $idSalon);
        $dbh->bindParam(2, $idProducto);
        $dbh->bindParam(3, $idGaleria);
        $dbh->execute();
        $elemento = $dbh->fetch();
        $connection = null;
        $respuesta = array();
        if(!empty($elemento)) {
          $respuesta = array('respuesta' => $elemento["respuesta"]
            , 'mensaje' => htmlentities($elemento["mensaje"])
            );
        }
        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});


$app->get("/sitios/:idSitio/productos/seo/:url", function($idSitio,$url) use($app){
    try{
        $connection = getConnection();
        $dbh = $connection->prepare("CALL sp_getProductoSeo(?,?)");
        $dbh->bindParam(1, $idSitio);
        $dbh->bindParam(2, $url);
        $dbh->execute();
        $elemento = $dbh->fetch();
        $respuesta = array();
        $idProducto=0;
        if(!empty($elemento)) {
            $idProducto=$elemento["idProducto"];
          $respuesta= array('idProducto' => $elemento["idProducto"]
            ,'idCategoria' => $elemento["idCategoria"]
            , 'idEstatus' => $elemento["idEstatus"]
            ,'idModelo' => $elemento["idModelo"]
            ,'principal' => $elemento["principal"]
            ,'idMovimiento' => $elemento["idMovimiento"]
            ,'idMarca' => $elemento["idMarca"]
            , 'idCristal' => $elemento["idCristal"]
            , 'idMaterial' => $elemento["idMaterial"]
             ,'piezas' => $elemento["piezas"]
            , 'producto' => htmlentities($elemento["producto"])
            , 'fechaHora' => htmlentities($elemento["fechaHora"])
            , 'diametro' => htmlentities($elemento["diametro"])
            , 'descripcion' => htmlentities($elemento["descripcion"])
            , 'urlImagen' => htmlentities($elemento["urlImagen"])
            , 'urlSeo' => htmlentities($elemento["urlSeo"])
            , 'modelo' => htmlentities($elemento["modelo"])
            , 'marca' => htmlentities($elemento["marca"])
            , 'precioPublico' => $elemento["precioPublico"]
            , 'precioNeto' => $elemento["precioNeto"]
             , 'precioOferta' => $elemento["precioOferta"]
            , 'clave' => htmlentities($elemento["clave"])
            , 'movimiento' => $elemento["movimiento"]
            , 'bisel' => htmlentities($elemento["bisel"])
            , 'accesorios' => htmlentities($elemento["accesorios"])
            , 'cristal' => htmlentities($elemento["cristal"])
            , 'condicion' => htmlentities($elemento["condicion"])
            , 'brazalete' => htmlentities($elemento["brazalete"])
            , 'categoria' => htmlentities($elemento["categoria"])
            ,"galeria"=>array()
            , 'categoriaUrl' => htmlentities($elemento["categoriaUrl"]));
        }

        $galeria = array();
        if($idProducto>0){
             $idGaleria=0;
            $dbh = $connection->prepare("CALL sp_getGaleriaProducto(?,?,?)");
        $dbh->bindParam(1, $idSitio);
        $dbh->bindParam(2, $idProducto);
        $dbh->bindParam(3, $idGaleria);
        $dbh->execute();
        $elementos = $dbh->fetchAll();
      
       
        foreach ($elementos as $elemento) {
        
          $galeria[]= array('idGaleria' => $elemento["idGaleria"]
            , 'urlImagen' => htmlentities($elemento["urlImagen"])
            , 'titulo' => htmlentities($elemento["titulo"])
            , 'descripcionSeo' => htmlentities($elemento["descripcionSeo"])
            , 'tituloSeo' => htmlentities($elemento["tituloSeo"])
          
            );
        }
        }
        $respuesta["galeria"]=$galeria;
          $connection = null; 
      $app->response->headers->set("Content-type", "application/json");
      $app->response->status(200);
      $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});
$app->get("/sitios/:idSitio/productos/:idProducto", function($idSitio,$idProducto) use($app){
    try{
        $connection = getConnection();
        $dbh = $connection->prepare("CALL sp_getProductos(?,?)");
        $dbh->bindParam(1, $idSitio);
        $dbh->bindParam(2, $idProducto);
        $dbh->execute();
        $elemento = $dbh->fetch();
        $respuesta = array();
        if(!empty($elemento)) {
          $respuesta= array('idProducto' => $elemento["idProducto"]
            ,'idCategoria' => $elemento["idCategoria"]
            ,'idEstatus' => $elemento["idEstatus"]
            ,'idModelo' => $elemento["idModelo"]
            ,'principal' => $elemento["principal"]
            ,'idMovimiento' => $elemento["idMovimiento"]
            ,'idMarca' => $elemento["idMarca"]
            ,'idCristal' => $elemento["idCristal"]
            ,'idMaterial' => $elemento["idMaterial"]
            ,'piezas' => $elemento["piezas"]
            ,'producto' => htmlentities($elemento["producto"])
            ,'fechaHora' => htmlentities($elemento["fechaHora"])
            ,'diametro' => htmlentities($elemento["diametro"])
            ,'descripcion' => htmlentities($elemento["descripcion"])
            ,'urlImagen' => htmlentities($elemento["urlImagen"])
            ,'urlSeo' => htmlentities($elemento["urlSeo"])
            ,'modelo' => htmlentities($elemento["modelo"])
            ,'marca' => htmlentities($elemento["marca"])
            ,'precioPublico' => $elemento["precioPublico"]
            ,'precioNeto' => $elemento["precioNeto"]
            ,'precioOferta' => $elemento["precioOferta"]
            ,'clave' => htmlentities($elemento["clave"])
            ,'movimiento' => $elemento["movimiento"]
            , 'bisel' => htmlentities($elemento["bisel"])
            , 'accesorios' => htmlentities($elemento["accesorios"])
            , 'cristal' => htmlentities($elemento["cristal"])
            , 'condicion' => htmlentities($elemento["condicion"])
            , 'brazalete' => htmlentities($elemento["brazalete"])
            ,'categoria' => htmlentities($elemento["categoria"])
            ,'categoriaUrl' => htmlentities($elemento["categoriaUrl"])
            );
        }
        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});


$app->post("/sitios/:idSitio/productos/", function($idSitio) use($app){
   try{

        $connection = getConnection(); 
        $producto=$app->request->post('producto');
        $urlSeo=$app->request->post('urlSeo');
        $diametro=$app->request->post('diametro');
        $idCategoria=$app->request->post('idCategoria');
        $idMarca=$app->request->post('idMarca');
        $idEstatus=$app->request->post('idEstatus');
        $idMaterial=$app->request->post('idMaterial');
        $idModelo=$app->request->post('idModelo');
        $idCristal=$app->request->post('idCristal');
        $idMovimiento=$app->request->post('idMovimiento');
        $principal=$app->request->post('principal');
        $piezas=$app->request->post('piezas');
        $precioNeto=$app->request->post('precioNeto');
        $precioOferta=$app->request->post('precioOferta');
        $precioPublico=$app->request->post('precioPublico');
        $descripcion=$app->request->post('descripcion');
        $urlImagen=$app->request->post('urlImagen');

        $bisel=$app->request->post('bisel');
        $accesorios=$app->request->post('accesorios');
        $condicion=$app->request->post('condicion');
        $brazalete=$app->request->post('brazalete');


        if (empty($urlImagen))
            $urlImagen='expertos-en-relojes-producto-default.jpg';
        if (empty($idModelo))
            $idModelo=0;
 
        $dbh = $connection->prepare("CALL sp_addProducto(?,?,?,?,?,?,?,?,?,? ,?,?,?,? ,?,?,?,?)");
        $dbh->bindParam(1, $idSitio);
        $dbh->bindParam(2, $idEstatus);
        $dbh->bindParam(3, $producto);
        $dbh->bindParam(4, $idMaterial);
        $dbh->bindParam(5, $idMarca);
        $dbh->bindParam(6, $idCategoria);
        $dbh->bindParam(7, $idModelo);
        $dbh->bindParam(8, $idCristal);
        $dbh->bindParam(9, $diametro);
        $dbh->bindParam(10, $descripcion);
        $dbh->bindParam(11, $idMovimiento);
        $dbh->bindParam(12, $principal);
        $dbh->bindParam(13, $urlImagen);
        $dbh->bindParam(14, $urlSeo); 
        $dbh->bindParam(15, $precioPublico);
        $dbh->bindParam(16, $precioNeto);
        $dbh->bindParam(17, $precioOferta);
        $dbh->bindParam(18, $piezas);
        $dbh->bindParam(19, $bisel);
        $dbh->bindParam(20, $accesorios);
        $dbh->bindParam(21, $condicion);
        $dbh->bindParam(22, $brazalete);




        $dbh->execute();
        $elemento = $dbh->fetch();
        $connection = null;
        $respuesta = array();
        if(!empty($elemento)) {


          $respuesta = array('respuesta' => $elemento["respuesta"]
            , 'mensaje' => htmlentities($elemento["mensaje"])
            );
        }
        $data=array('data'=>$respuesta);
        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});

$app->options("/sitios/:idSitio/productos/:idProducto", function($idSitio,$idProducto)use($app) {
    //Return response headers
});

$app->put("/sitios/:idSitio/productos/:idProducto", function($idSitio,$idProducto) use($app){
 try{

        $connection = getConnection(); 
        $producto=$app->request->post('producto');
        $urlSeo=$app->request->post('urlSeo');
        $diametro=$app->request->post('diametro');
        $idMarca=$app->request->post('idMarca');
        $idCategoria=$app->request->post('idCategoria');
        $idEstatus=$app->request->post('idEstatus');
        $idMaterial=$app->request->post('idMaterial');
        $idModelo=$app->request->post('idModelo');
        $idCristal=$app->request->post('idCristal');
        $idMovimiento=$app->request->post('idMovimiento');
        $principal=$app->request->post('principal');
        $piezas=$app->request->post('piezas');
        $precioNeto=$app->request->post('precioNeto');
        $precioOferta=$app->request->post('precioOferta');
        $precioPublico=$app->request->post('precioPublico');
        $descripcion=$app->request->post('descripcion');
        $urlImagen=$app->request->post('urlImagen');
        $bisel=$app->request->post('bisel');
        $accesorios=$app->request->post('accesorios');
        $condicion=$app->request->post('condicion');
        $brazalete=$app->request->post('brazalete');

       $dbh = $connection->prepare("CALL sp_editProducto(?,?,?,?,?, ?,?,?,?,?,? ,?,?,?,?,?,?,?,?)");
        $dbh->bindParam(1, $idSitio);
        $dbh->bindParam(2, $idProducto);
        $dbh->bindParam(3, $idEstatus);
        $dbh->bindParam(4, $producto);
        $dbh->bindParam(5, $idMaterial);
        $dbh->bindParam(6, $idMarca);
        $dbh->bindParam(7, $idCategoria);
        $dbh->bindParam(8, $idModelo);
        $dbh->bindParam(9, $idCristal);
        $dbh->bindParam(10, $diametro);
        $dbh->bindParam(11, $descripcion);
        $dbh->bindParam(12, $idMovimiento);
        $dbh->bindParam(13, $principal);
        $dbh->bindParam(14, $urlImagen);
        $dbh->bindParam(15, $urlSeo);
        $dbh->bindParam(16, $precioPublico);
        $dbh->bindParam(17, $precioNeto);
        $dbh->bindParam(18, $precioOferta);
        $dbh->bindParam(19, $piezas);
        $dbh->bindParam(20, $bisel);
        $dbh->bindParam(21, $accesorios);
        $dbh->bindParam(22, $condicion);
        $dbh->bindParam(23, $brazalete);

        $dbh->execute();
        $elemento = $dbh->fetch();
        $connection = null;
        $respuesta = array();
        if(!empty($elemento)) {
          $respuesta = array('respuesta' => $elemento["respuesta"]
            , 'mensaje' => htmlentities($elemento["mensaje"])
            );
        }
        $data=array('data'=>$respuesta);
        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});

$app->delete("/sitios/:idSitio/productos/:idProducto", function($idSitio,$idProducto) use($app){
    try{
        $connection = getConnection();
        $dbh = $connection->prepare("CALL sp_deleteProducto(?,?)");
        $dbh->bindParam(1, $idSalon);
        $dbh->bindParam(2, $idProducto);
        $dbh->execute();
        $elemento = $dbh->fetch();
        $connection = null;
        $respuesta = array();
        if(!empty($elemento)) {
          $respuesta = array('respuesta' => $elemento["respuesta"]
            , 'mensaje' => htmlentities($elemento["mensaje"])
            );
        }
        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});

