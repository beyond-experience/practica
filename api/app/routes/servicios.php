<?php
$app->get("/salones/:idSalon/servicios/categorias/:idCategoria/subcategorias/:idSubcategoria/activos/", function($idSalon,$idCategoria,$idSubcategoria) use($app){
    try{
        $idProducto =0;
        $connection = getConnection();
        $dbh = $connection->prepare("CALL sp_getServicios(?,?,?,?)");
        $dbh->bindParam(1, $idSalon);
        $dbh->bindParam(2, $idProducto);
        $dbh->bindParam(3, $idCategoria);
        $dbh->bindParam(4, $idSubcategoria);
        $dbh->execute();
        $elementos = $dbh->fetchAll();
        $connection = null;
        $respuesta = array();
        foreach ($elementos as $elemento) {
          if($elemento["idEstatus"]==1)
          $respuesta[]= array('idProducto' => $elemento["idProducto"]
            ,'idCategoria' => $elemento["idCategoria"]
            ,'idSubcategoria' => $elemento["idSubcategoria"]
            , 'producto' => htmlentities($elemento["producto"])
            , 'categoria' => htmlentities($elemento["categoria"])
            , 'subCategoria' => htmlentities($elemento["subCategoria"])
            , 'descripcion' => htmlentities($elemento["descripcion"])
            , 'tiempo' => $elemento["tiempo"]
            , 'idEstatus' => $elemento["idEstatus"]
            , 'precioPublico' => $elemento["precioPublico"]
            , 'precioNeto' => $elemento["precioNeto"]
            , 'precioOferta' => $elemento["precioOferta"]
            , 'clave' => htmlentities($elemento["clave"])
            );
        }
        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});
$app->get("/salones/:idSalon/servicios/categorias/:idCategoria/subcategorias/:idSubcategoria", function($idSalon,$idCategoria,$idSubcategoria) use($app){
    try{
       $idProducto =0;
        $connection = getConnection();
        $dbh = $connection->prepare("CALL sp_getServicios(?,?,?,?)");
        $dbh->bindParam(1, $idSalon);
        $dbh->bindParam(2, $idProducto);
        $dbh->bindParam(3, $idCategoria);
        $dbh->bindParam(4, $idSubcategoria);
        $dbh->execute();
        $elementos = $dbh->fetchAll();
        $connection = null;
        $respuesta = array();
        foreach ($elementos as $elemento) {
          $respuesta[]= array('idProducto' => $elemento["idProducto"]
            ,'idCategoria' => $elemento["idCategoria"]
            ,'idSubcategoria' => $elemento["idSubcategoria"]
            , 'producto' => htmlentities($elemento["producto"])
            , 'categoria' => htmlentities($elemento["categoria"])
            , 'subCategoria' => htmlentities($elemento["subCategoria"])
            , 'descripcion' => htmlentities($elemento["descripcion"])
            , 'tiempo' => $elemento["tiempo"]
            , 'idEstatus' => $elemento["idEstatus"]
            , 'precioPublico' => $elemento["precioPublico"]
            , 'precioNeto' => $elemento["precioNeto"]
            , 'precioOferta' => $elemento["precioOferta"]
            , 'clave' => htmlentities($elemento["clave"])
            );
        }

      $data=array('data'=>$respuesta);
      $app->response->headers->set("Content-type", "application/json");
      $app->response->status(200);
      $app->response->body(json_encode($data, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});

$app->get("/salones/:idSalon/servicios/:idProducto", function($idSalon,$idProducto) use($app){
    try{
        $idCategoria=0;
        $idSubcategoria=0;
        $connection = getConnection();
        $dbh = $connection->prepare("CALL sp_getServicios(?,?,?,?)");
        $dbh->bindParam(1, $idSalon);
        $dbh->bindParam(2, $idProducto);
        $dbh->bindParam(3, $idCategoria);
        $dbh->bindParam(4, $idSubcategoria);
        $dbh->execute();
        $elemento = $dbh->fetch();
        $connection = null;
        $respuesta = array();
        if(!empty($elemento)) {
          $respuesta= array('idProducto' => $elemento["idProducto"]
            ,'idCategoria' => $elemento["idCategoria"]
            ,'idSubcategoria' => $elemento["idSubcategoria"]
            , 'producto' => htmlentities($elemento["producto"])
            , 'categoria' => htmlentities($elemento["categoria"])
            , 'subCategoria' => htmlentities($elemento["subCategoria"])
            , 'descripcion' => htmlentities($elemento["descripcion"])
            , 'tiempo' => $elemento["tiempo"]
            , 'idEstatus' => $elemento["idEstatus"]
            , 'precioPublico' => $elemento["precioPublico"]
            , 'precioNeto' => $elemento["precioNeto"]
            , 'precioOferta' => $elemento["precioOferta"]
            , 'clave' => htmlentities($elemento["clave"])
            );
        }

        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});

$app->post("/salones/:idSalon/servicios/", function($idSalon) use($app){
   try{

        $connection = getConnection(); 
        $idCategoria=$app->request->post('idCategoria');
        $idSubcategoria=$app->request->post('idSubcategoria');
        $descripcion=$app->request->post('descripcion');
        $idEstatus=$app->request->post('idEstatus');
        $producto=$app->request->post('producto');
        $precioNeto=$app->request->post('precioNeto');
        $precioOferta=$app->request->post('precioOferta');
        $precioPublico=$app->request->post('precioPublico');
        $clave=$app->request->post('clave');
        $tiempo=$app->request->post('tiempo');
        $idMoneda=1;
        $idTipo=1;
      
        $dbh = $connection->prepare("CALL sp_addProducto(?,?,?,?,?,?,?,?,?,?,?,?,?)");
        $dbh->bindParam(1, $idSalon);
        $dbh->bindParam(2, $idCategoria);
        $dbh->bindParam(3, $idSubcategoria);
        $dbh->bindParam(4, $producto);
        $dbh->bindParam(5, $tiempo);
        $dbh->bindParam(6, $idTipo);
        $dbh->bindParam(7, $idEstatus);
        $dbh->bindParam(8, $clave);
        $dbh->bindParam(9, $descripcion);
        $dbh->bindParam(10, $precioNeto);
        $dbh->bindParam(11, $precioPublico);
        $dbh->bindParam(12, $precioOferta);
        $dbh->bindParam(13, $idMoneda);


        $dbh->execute();
        $elemento = $dbh->fetch();
        $connection = null;
        $respuesta = array();
        if(!empty($elemento)) {
          $respuesta = array('respuesta' => $elemento["respuesta"]
            , 'mensaje' => htmlentities($elemento["mensaje"])
            );
        }
        $data=array('data'=>$respuesta);
        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});
$app->options("/salones/:idSalon/servicios/:idProducto", function($idSalon,$idProducto)use($app) {
    //Return response headers
});

$app->put("/salones/:idSalon/servicios/:idProducto", function($idSalon,$idProducto) use($app){
    try{
        $connection = getConnection(); 
        $idCategoria=$app->request->post('idCategoria');
        $idSubcategoria=$app->request->post('idSubcategoria');
        $descripcion=$app->request->post('descripcion');
        $idEstatus=$app->request->post('idEstatus');
        $producto=$app->request->post('producto');
        $precioNeto=$app->request->post('precioNeto');
        $precioOferta=$app->request->post('precioOferta');
        $precioPublico=$app->request->post('precioPublico');
        $clave=$app->request->post('clave');
        $tiempo=$app->request->post('tiempo');
        $idMoneda=1;
        
        $idTipo=1;
   
        $dbh = $connection->prepare("CALL sp_editProducto(?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
        $dbh->bindParam(1, $idSalon);
        $dbh->bindParam(2, $idProducto);
        $dbh->bindParam(3, $idCategoria);
        $dbh->bindParam(4, $idSubcategoria);
        $dbh->bindParam(5, $producto);
        $dbh->bindParam(6, $tiempo);
        $dbh->bindParam(7, $idTipo);
        $dbh->bindParam(8, $idEstatus);
        $dbh->bindParam(9, $clave);
        $dbh->bindParam(10, $descripcion);
        $dbh->bindParam(11, $precioNeto);
        $dbh->bindParam(12, $precioPublico);
        $dbh->bindParam(13, $precioOferta);
        $dbh->bindParam(14, $idMoneda);
        $dbh->execute();
        $elemento = $dbh->fetch();
        $connection = null;
        $respuesta = array('respuesta' => '----');
        if(!empty($elemento)) {
          $respuesta = array('respuesta' => $elemento["respuesta"]
            , 'mensaje' => htmlentities($elemento["mensaje"])
            );
        }
        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});
$app->delete("/salones/:idSalon/servicios/:idProducto", function($idSalon,$idProducto) use($app){
    try{
        $connection = getConnection();
        $dbh = $connection->prepare("CALL sp_deleteProducto(?,?)");
        $dbh->bindParam(1, $idSalon);
        $dbh->bindParam(2, $idProducto);
        $dbh->execute();
        $elemento = $dbh->fetch();
        $connection = null;
        $respuesta = array();
        if(!empty($elemento)) {
          $respuesta = array('respuesta' => $elemento["respuesta"]
            , 'mensaje' => htmlentities($elemento["mensaje"])
            );
        }
        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});