<?php 
$app->get("/salones/:idSalon/estilistas/:idSucursal/", function($idSalon,$idSucursal) use($app)
{
	try{
		$connection = getConnection();
		$dbh = $connection->prepare("CALL sp_getEstilistas(?,?)");
		$dbh->bindParam(1, $idSalon);
		$dbh->bindParam(2, $idSucursal);
		$dbh->execute();
		$elementos = $dbh->fetchAll();
        $connection = null;
        $respuesta = array();
        foreach ($elementos as $elemento) {
          $respuesta[] = array('idUsuario' => $elemento["idUsuario"]
		   , 'nombre' => htmlentities(utf8_encode($elemento["nombre"]))
	        , 'usuario' => htmlentities(utf8_encode($elemento["usuario"]))
	        , 'apellidos' => htmlentities(utf8_encode($elemento["apellidos"]))
	        , 'correo' => htmlentities(utf8_encode($elemento["correo"]))
	        , 'clase' => htmlentities(utf8_encode($elemento["clase"]))
	        , 'idEstatus' => $elemento["idEstatus"]);
	    }

	    $app->response->headers->set("Content-type", "application/json");
	    $app->response->status(200);
	    $app->response->body(json_encode($respuesta));
	}
	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
	}
});