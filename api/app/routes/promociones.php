<?php
$app->get("/bares/:idBar/promociones/", function($idBar) use($app){
    try{
        $idPromocion=0;
        $connection = getConnection();
        $dbh = $connection->prepare("CALL sp_getPromociones(?,?)");
        $dbh->bindParam(1, $idBar);
        $dbh->bindParam(2, $idPromocion);
        $dbh->execute();
        $elementos = $dbh->fetchAll();
        $connection = null;
        $respuesta = array();
        foreach ($elementos as $elemento) {
          $respuesta[] = array('idPromocion' => $elemento["idPromocion"]
            , 'imagen' => htmlentities(utf8_encode($elemento["imagen"]))
            , 'baner' => htmlentities(utf8_encode($elemento["baner"]))
            , 'alt' => htmlentities(utf8_encode($elemento["alt"]))
            , 'titulo' => htmlentities($elemento["titulo"])
            , 'descripcion' => htmlentities($elemento["descripcion"])
            ,'orden' => $elemento["orden"]
            ,'idEstatus' => $elemento["idEstatus"]
            ,'idBar' => $elemento["idBar"]
            ,'urlSeo' => $elemento["urlSeo"]
            );
        }
        $data=array('data'=>$respuesta);
        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($data, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});
$app->get("/bares/:idBar/promociones/web/", function($idBar) use($app){
    try{
        $idPromocion=0;
        $connection = getConnection();
        $dbh = $connection->prepare("CALL sp_getPromociones(?,?)");
        $dbh->bindParam(1, $idBar);
        $dbh->bindParam(2, $idPromocion);
        $dbh->execute();
        $elementos = $dbh->fetchAll();
        $connection = null;
        $respuesta = array();
        foreach ($elementos as $elemento) {
             if ($elemento["principal"]==1)
          $respuesta[] = array('idPromocion' => $elemento["idPromocion"]
             , 'imagen' => htmlentities(utf8_encode($elemento["imagen"]))
            , 'baner' => htmlentities(utf8_encode($elemento["baner"]))
            , 'alt' => htmlentities(utf8_encode($elemento["alt"]))
            ,'orden' => $elemento["orden"]
            ,'idEstatus' => $elemento["idEstatus"]
            ,'idBar' => $elemento["idBar"]
            );
        }
        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});

$app->get("/bares/:idBar/promociones/:idPromocion", function($idBar,$idPromocion) use($app){
    try{
        $connection = getConnection();
        $dbh = $connection->prepare("CALL sp_getPromociones(?,?)");
        $dbh->bindParam(1, $idBar);
        $dbh->bindParam(2, $idPromocion);
        $dbh->execute();
        $elemento = $dbh->fetch();
        $connection = null;
        $respuesta = array();
        if(!empty($elemento)) {
          $respuesta = array('idPromocion' => $elemento["idPromocion"]
             , 'imagen' => htmlentities(utf8_encode($elemento["imagen"]))
            , 'baner' => htmlentities(utf8_encode($elemento["baner"]))
            ,'alt' => htmlentities(utf8_encode($elemento["alt"]))
            ,'titulo' => htmlentities($elemento["titulo"])
            ,'descripcion' => htmlentities($elemento["descripcion"])
            ,'orden' => $elemento["orden"]
            ,'idEstatus' => $elemento["idEstatus"]
            ,'idBar' => $elemento["idBar"]
            ,'urlSeo' => $elemento["urlSeo"]
            ,'principal' => $elemento["principal"]

            );
        }
        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});

$app->post("/bares/:idBar/promociones/", function($idBar) use($app){
    try{
        $connection = getConnection();
        $imagen=utf8_decode($app->request->post('imagen'));
        $alt=utf8_decode($app->request->post('alt'));
        $idEstatus=$app->request->post('idEstatus');
        $orden=utf8_decode($app->request->post('orden'));
        $principal=utf8_decode($app->request->post('principal'));
        $baner=utf8_decode($app->request->post('baner'));
        $urlSeo=utf8_decode($app->request->post('urlSeo'));
        $titulo=utf8_decode($app->request->post('titulo'));
        $descripcion=utf8_decode($app->request->post('descripcion'));

        $dbh = $connection->prepare("CALL sp_addPromocion(?,?,?,?,?,?,?,?,?,?,?)");
        $dbh->bindParam(1, $idBar);
        $dbh->bindParam(2, $imagen);
        $dbh->bindParam(3, $alt);
        $dbh->bindParam(4, $idEstatus);
        $dbh->bindParam(5, $orden);
        $dbh->bindParam(6, $principal);
        $dbh->bindParam(7, $baner);
        $dbh->bindParam(8, $urlSeo);
        $dbh->bindParam(9, $titulo);
        $dbh->bindParam(10, $descripcion);
        $dbh->execute();
        $elemento = $dbh->fetch();
        $connection = null;
        $respuesta = array();
        if(!empty($elemento)) {
          $respuesta = array('respuesta' => $elemento["respuesta"]
            , 'mensaje' => htmlentities(utf8_encode($elemento["mensaje"]))
            );
        }
        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});
$app->options("/bares/:idBar/promociones/:idPromocion", function($idBar,$idPromocion) {
    //Return response headers
});
$app->put("/bares/:idBar/promociones/:idPromocion", function($idBar,$idPromocion) use($app){
    try{
        $connection = getConnection();
        $imagen=utf8_decode($app->request->post('imagen'));
        $alt=utf8_decode($app->request->post('alt'));
        $idEstatus=$app->request->post('idEstatus');
        $orden=utf8_decode($app->request->post('orden'));
        $principal=utf8_decode($app->request->post('principal'));
        $baner=utf8_decode($app->request->post('baner'));
        $urlSeo=utf8_decode($app->request->post('urlSeo'));
        $titulo=utf8_decode($app->request->post('titulo'));
        $descripcion=utf8_decode($app->request->post('descripcion'));

        $dbh = $connection->prepare("CALL sp_editPromocion(?,?,?,?,?,?,?,?,?,?,?,?)");
        $dbh->bindParam(1, $idBar);
        $dbh->bindParam(2, $idPromocion);
        $dbh->bindParam(3, $imagen);
        $dbh->bindParam(4, $alt);
        $dbh->bindParam(5, $idEstatus);
        $dbh->bindParam(6, $orden);
        $dbh->bindParam(7, $principal);
        $dbh->bindParam(8, $baner);
        $dbh->bindParam(9, $urlSeo);
        $dbh->bindParam(10, $titulo);
        $dbh->bindParam(11, $descripcion);
        $dbh->execute();
        $elemento = $dbh->fetch();
        $connection = null;
        $respuesta = array();
        if(!empty($elemento)) {
          $respuesta = array('respuesta' => $elemento["respuesta"]
            , 'mensaje' => htmlentities(utf8_encode($elemento["mensaje"]))
            );
        }
        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});