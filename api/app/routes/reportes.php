<?php 
$app->get("/salones/:idSalon/reportes/comisiones/:inicio/:fin/:idEstilista/", function($idSalon,$inicio,$fin,$idEstilista) use($app){
 try{
    $connection = getConnection(); 
    $dbh = $connection->prepare("CALL sp_getReporteComisiones(?,?,?)");
  //  $dbh->bindParam(1, $idSalon);
    $dbh->bindParam(1, $inicio);
    $dbh->bindParam(2, $fin);
    $dbh->bindParam(3, $idEstilista);
	    $dbh->execute();
    $elementos = $dbh->fetchAll();
        $connection = null;
        $respuesta = array();
        foreach ($elementos as $elemento) {
          $respuesta[]= array('idEstilista' => $elemento["idEstilista"]
      ,'estilista' => $elemento["estilista"]
      ,'comision' => $elemento["comision"]
      ,'total' => $elemento["total"]
      );
      }
      $data=array('data'=>$respuesta);
        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($data, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }

});