<?php
$app->get("/sitios/:idSitio/modelos/activos/", function($idSitio) use($app){
  try{
    $idMarca=0;
    $idModelo=0;
    $connection = getConnection();
    $dbh = $connection->prepare("CALL sp_getModelos(?,?,?)");
    $dbh->bindParam(1, $idSitio);
    $dbh->bindParam(2, $idModelo);
    $dbh->bindParam(3, $idMarca);
    $dbh->execute();
    $elementos = $dbh->fetchAll();
    $connection = null;
    $respuesta = array();
    foreach ($elementos as $elemento) {
      if($elemento["idEstatus"]==1){
        $respuesta[] =  array('idModelo' => $elemento["idModelo"]
          ,'idEstatus' => $elemento["idEstatus"]
          ,'idMarca' => $elemento["idMarca"]
          ,'modelo' => htmlentities($elemento["modelo"])
          ,'clave' => htmlentities($elemento["clave"])
        );
      }
    }
    $app->response->headers->set("Content-type", "application/json");
    $app->response->status(200);
    $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
  }
  catch(PDOException $e){
    echo "Error: " . $e->getMessage();
  }
});
$app->get("/sitios/:idSitio/modelos/", function($idSitio) use($app){
  try{
    $idMarca=0;
    $idModelo=0;
    $connection = getConnection();
    $dbh = $connection->prepare("CALL sp_getModelos(?,?,?)");
    $dbh->bindParam(1, $idSitio);
    $dbh->bindParam(2, $idModelo);
    $dbh->bindParam(3, $idMarca);
    $dbh->execute();
    $elementos = $dbh->fetchAll();
    $connection = null;
    $respuesta = array();
    foreach ($elementos as $elemento) {
        $respuesta[] =  array('idModelo' => $elemento["idModelo"]
          ,'idEstatus' => $elemento["idEstatus"]
          ,'idMarca' => $elemento["idMarca"]
          ,'marca' => $elemento["marca"]
          ,'modelo' => htmlentities($elemento["modelo"])
          ,'clave' => htmlentities($elemento["clave"])
        );
    }
    $data=array("data"=>$respuesta);
    $app->response->headers->set("Content-type", "application/json");
    $app->response->status(200);
    $app->response->body(json_encode($data, JSON_UNESCAPED_UNICODE));
  }
  catch(PDOException $e){
    echo "Error: " . $e->getMessage();
  }
});
$app->get("/sitios/:idSitio/modelos/:idModelo", function($idSitio,$idModelo) use($app){
  try{
    $idMarca=0;
    $connection = getConnection();
    $dbh = $connection->prepare("CALL sp_getModelos(?,?,?)");
    $dbh->bindParam(1, $idSitio);
    $dbh->bindParam(2, $idModelo);
    $dbh->bindParam(3, $idMarca);
    $dbh->execute();
    $elemento = $dbh->fetch();
    $connection = null;
    $respuesta = array();
    if(!empty($elemento)) {
        $respuesta =  array('idModelo' => $elemento["idModelo"]
          ,'idEstatus' => $elemento["idEstatus"]
          ,'idMarca' => $elemento["idMarca"]
          ,'marca' => $elemento["marca"]
          ,'modelo' => htmlentities($elemento["modelo"])
          ,'clave' => htmlentities($elemento["clave"])
        );
    }

    $app->response->headers->set("Content-type", "application/json");
    $app->response->status(200);
    $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
  }
  catch(PDOException $e){
    echo "Error: " . $e->getMessage();
  }
});
$app->get("/sitios/:idSitio/marcas/:idMarca/modelos/activos/", function($idSitio,$idMarca) use($app){
  try{
    $idModelo=0;
    $connection = getConnection();
    $dbh = $connection->prepare("CALL sp_getModelos(?,?,?)");
    $dbh->bindParam(1, $idSitio);
    $dbh->bindParam(2, $idModelo);
    $dbh->bindParam(3, $idMarca);
    $dbh->execute();
    $elementos = $dbh->fetchAll();
    $connection = null;
    $respuesta = array();
    foreach ($elementos as $elemento) {
      if($elemento["idEstatus"]==1)
        $respuesta[] =  array('idModelo' => $elemento["idModelo"]
          ,'idEstatus' => $elemento["idEstatus"]
          ,'idMarca' => $elemento["idMarca"]
          ,'marca' => $elemento["marca"]
          ,'modelo' => htmlentities($elemento["modelo"])
          ,'clave' => htmlentities($elemento["clave"])
        );
    }
    $app->response->headers->set("Content-type", "application/json");
    $app->response->status(200);
    $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
  }
  catch(PDOException $e){
    echo "Error: " . $e->getMessage();
  }
});
$app->get("/sitios/:idSitio/marcas/:idMarca/modelos/", function($idSitio,$idMarca) use($app){
  try{
    $idModelo=0;
    $connection = getConnection();
    $dbh = $connection->prepare("CALL sp_getModelos(?,?,?)");
    $dbh->bindParam(1, $idSitio);
    $dbh->bindParam(2, $idModelo);
    $dbh->bindParam(3, $idMarca);
    $dbh->execute();
    $elementos = $dbh->fetchAll();
    $connection = null;
    $respuesta = array();
    foreach ($elementos as $elemento) {
        $respuesta[] =  array('idModelo' => $elemento["idModelo"]
          ,'idEstatus' => $elemento["idEstatus"]
          ,'idMarca' => $elemento["idMarca"]
          ,'marca' => $elemento["marca"]
          ,'modelo' => htmlentities($elemento["modelo"])
          ,'clave' => htmlentities($elemento["clave"])
        );
    }
    $app->response->headers->set("Content-type", "application/json");
    $app->response->status(200);
    $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
  }
  catch(PDOException $e){
    echo "Error: " . $e->getMessage();
  }
});
$app->post("/sitios/:idSitio/modelos/", function($idSitio) use($app){
try{

        $connection = getConnection(); 
        $modelo=$app->request->post('modelo');
        $clave=$app->request->post('clave');
        $idMarca=$app->request->post('idMarca');
        $idEstatus=$app->request->post('idEstatus');
        $dbh = $connection->prepare("CALL sp_addModelo(?,?,?,?,?)");
        $dbh->bindParam(1, $idSitio);
        $dbh->bindParam(2, $idMarca);
        $dbh->bindParam(3, $idEstatus);
        $dbh->bindParam(4, $modelo);
         $dbh->bindParam(5, $clave);
        $dbh->execute();
        $elemento = $dbh->fetch();
        $connection = null;
        $respuesta = array();
        if(!empty($elemento)) {
          $respuesta = array('respuesta' => $elemento["respuesta"]
            , 'mensaje' => htmlentities($elemento["mensaje"])
            );
        }
        $data=array('data'=>$respuesta);
        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});
$app->options("/sitios/:idSitio/modelos/:idModelo", function($idSitio,$idModelo) use($app){
  });
$app->put("/sitios/:idSitio/modelos/:idModelo", function($idSitio,$idModelo) use($app){
  try{

        $connection = getConnection(); 
        $modelo=$app->request->post('modelo');
        $clave=$app->request->post('clave');
        $idMarca=$app->request->post('idMarca');
        $idEstatus=$app->request->post('idEstatus');
        $dbh = $connection->prepare("CALL sp_EditModelo(?,?,?,?,?,?)");
        $dbh->bindParam(1, $idSitio);
        $dbh->bindParam(2, $idModelo);
        $dbh->bindParam(3, $idMarca);
        $dbh->bindParam(4, $idEstatus);
        $dbh->bindParam(5, $modelo);
         $dbh->bindParam(6, $clave);
        $dbh->execute();
        $elemento = $dbh->fetch();
        $connection = null;
        $respuesta = array();
        if(!empty($elemento)) {
          $respuesta = array('respuesta' => $elemento["respuesta"]
            , 'mensaje' => htmlentities($elemento["mensaje"])
            );
        }
        $data=array('data'=>$respuesta);
        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
  });
