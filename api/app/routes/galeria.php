<?php
$app->get("/sitios/:idSitio/sliders/activos/", function($idSitio) use($app){
  try{
    $connection = getConnection();
    $dbh = $connection->prepare("CALL sp_getSliders(?,?)");
    $dbh->bindParam(1, $idSitio);
    $dbh->bindParam(2, $idSlider);
    $dbh->execute();
    $elementos = $dbh->fetchAll();
    $connection = null;
    $respuesta = array();
    foreach ($elementos as $elemento) {
      if($elemento["idEstatus"]==1){
        $respuesta[] = array('idSitio' => $elemento["idSitio"]
          ,'idSlider' => $elemento["idSlider"]
          ,'urlImagen' => htmlentities($elemento["urlImagen"])
          ,'texto1' => htmlentities($elemento["texto1"])
          ,'texto2' => htmlentities($elemento["texto2"])
          ,'texto3' => htmlentities($elemento["texto3"])
          ,'urlDestino' => htmlentities($elemento["urlDestino"])
          ,'tituloSeo' => htmlentities($elemento["tituloSeo"])
          ,'orden' => $elemento["orden"]
          ,'idEstatus' => $elemento["idEstatus"]
        );
      }
    }
    $app->response->headers->set("Content-type", "application/json");
    $app->response->status(200);
    $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
  }
  catch(PDOException $e){
    echo "Error: " . $e->getMessage();
  }
});

$app->get("/sitios/:idSitio/sliders/", function($idSitio) use($app){
 try{
    $idSlider=0;
    $connection = getConnection();
    $dbh = $connection->prepare("CALL sp_getSliders(?,?)");
    $dbh->bindParam(1, $idSitio);
    $dbh->bindParam(2, $idSlider);
    $dbh->execute();
    $elementos = $dbh->fetchAll();
    $connection = null;
    $respuesta = array();
    foreach ($elementos as $elemento) {
      $respuesta[] = array('idSitio' => $elemento["idSitio"]
        ,'idSlider' => $elemento["idSlider"]
        ,'urlImagen' => htmlentities($elemento["urlImagen"])
        ,'texto1' => htmlentities($elemento["texto1"])
        ,'texto2' => htmlentities($elemento["texto2"])
        ,'texto3' => htmlentities($elemento["texto3"])
        ,'urlDestino' => htmlentities($elemento["urlDestino"])
        ,'tituloSeo' => htmlentities($elemento["tituloSeo"])
        ,'orden' => $elemento["orden"]
        ,'idEstatus' => $elemento["idEstatus"]
      );
    }
    $data= array('data' =>$respuesta  );
    $app->response->headers->set("Content-type", "application/json");
    $app->response->status(200);
    $app->response->body(json_encode($data, JSON_UNESCAPED_UNICODE));
  }
  catch(PDOException $e){
    echo "Error: " . $e->getMessage();
  }
});
$app->get("/sitios/:idSitio/sliders/:idSlider", function($idSitio,$idSlider) use($app){
 try{
    $connection = getConnection();
    $dbh = $connection->prepare("CALL sp_getSliders(?,?)");
    $dbh->bindParam(1, $idSitio);
    $dbh->bindParam(2, $idSlider);
    $dbh->execute();
    $elemento = $dbh->fetch();
    $connection = null;
    $respuesta = array();
    if(!empty($elemento)) {
        $respuesta  = array('idSitio' => $elemento["idSitio"]
        ,'idSlider' => $elemento["idSlider"]
        ,'urlImagen' => htmlentities($elemento["urlImagen"])
        ,'texto1' => htmlentities($elemento["texto1"])
        ,'texto2' => htmlentities($elemento["texto2"])
        ,'texto3' => htmlentities($elemento["texto3"])
        ,'urlDestino' => htmlentities($elemento["urlDestino"])
        ,'tituloSeo' => htmlentities($elemento["tituloSeo"])
        ,'orden' => $elemento["orden"]
        ,'idEstatus' => $elemento["idEstatus"]
      );
    }

    $app->response->headers->set("Content-type", "application/json");
    $app->response->status(200);
    $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
  }
  catch(PDOException $e){
    echo "Error: " . $e->getMessage();
  }
});

$app->post("/sitios/:idSitio/sliders/", function($idSitio) use($app){
    try{
      $connection = getConnection();
      $urlImagen=$app->request->post('urlImagen');
      $texto1=$app->request->post('texto1');
      $texto2=$app->request->post('texto2');
      $texto3=$app->request->post('texto3');
      $urlDestino=$app->request->post('urlDestino');
      $tituloSeo=$app->request->post('tituloSeo');
      $idEstatus=$app->request->post('idEstatus');
      $orden=$app->request->post('orden');

      $dbh = $connection->prepare("CALL sp_addSlider(?,?,?,?,?,?,?,?,?)");
      $dbh->bindParam(1, $idSitio);
      $dbh->bindParam(2, $urlImagen);
      $dbh->bindParam(3, $texto1);
      $dbh->bindParam(4, $texto2);
      $dbh->bindParam(5, $texto3);
      $dbh->bindParam(6, $urlDestino);
      $dbh->bindParam(7, $idEstatus);
      $dbh->bindParam(8, $orden);
      $dbh->bindParam(9,$tituloSeo);
      $dbh->execute();
      $elemento = $dbh->fetch();
      $connection = null;
      $respuesta = array();
      if(!empty($elemento)) {
        $respuesta = array('respuesta' => $elemento["respuesta"]
          , 'mensaje' => htmlentities(utf8_encode($elemento["mensaje"]))
          );
      }
      $data=array('data'=>$respuesta);
      $app->response->headers->set("Content-type", "application/json");
      $app->response->status(200);
      $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});
$app->options("/sitios/:idSitio/sliders/:idSlider", function($idSitio,$idSlider) {
    //Return response headers
});

$app->put("/sitios/:idSitio/sliders/:idSlider", function($idSitio,$idSlider) use($app){
  try{
    $connection = getConnection();
    $urlImagen=$app->request->post('urlImagen');
    $texto1=$app->request->post('texto1');
    $texto2=$app->request->post('texto2');
    $texto3=$app->request->post('texto3');
    $urlDestino=$app->request->post('urlDestino');
    $tituloSeo=$app->request->post('tituloSeo');
    $idEstatus=$app->request->post('idEstatus');
    $orden=$app->request->post('orden');
    $dbh = $connection->prepare("CALL sp_editSlider(?,?,?,?,?,?,?,?,?,?)");
    $dbh->bindParam(1, $idSitio);
    $dbh->bindParam(2, $idSlider);
    $dbh->bindParam(3, $urlImagen);
    $dbh->bindParam(4, $texto1);
    $dbh->bindParam(5, $texto2);
    $dbh->bindParam(6, $texto3);
    $dbh->bindParam(7, $urlDestino);
    $dbh->bindParam(8, $idEstatus);
    $dbh->bindParam(9, $orden);
    $dbh->bindParam(10,$tituloSeo);
    $dbh->execute();
    $elemento = $dbh->fetch();
    $connection = null;
    $respuesta = array();
    if(!empty($elemento)) {
      $respuesta = array('respuesta' => $elemento["respuesta"]
        , 'mensaje' => htmlentities(utf8_encode($elemento["mensaje"]))
        );
    }
    $app->response->headers->set("Content-type", "application/json");
    $app->response->status(200);
    $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
  }
  catch(PDOException $e){
      echo "Error: " . $e->getMessage();
  }
});

$app->delete("/sitios/:idSitio/sliders/:idSlider", function($idSitio,$idSlider) use($app){
    try{
        $connection = getConnection();
        $dbh = $connection->prepare("CALL sp_deleteSlider(?,?)");
        $dbh->bindParam(1, $idSitio);
        $dbh->bindParam(2, $idSlider);

        $dbh->execute();
        $elemento = $dbh->fetch();
        $connection = null;
        $respuesta = array();
        if(!empty($elemento)) {
          $respuesta = array('respuesta' => $elemento["respuesta"]
            , 'mensaje' => htmlentities(utf8_encode($elemento["mensaje"]))
            );
        }
        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});