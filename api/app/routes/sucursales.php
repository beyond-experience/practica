<?php
$app->get("/salones/:idSalon/sucursales/activas/", function($idSalon) use($app){
    try{
        $idSucursal=0;
        $connection = getConnection();
        $dbh = $connection->prepare("CALL sp_getSucursales(?,?)");
        $dbh->bindParam(1, $idSalon);
        $dbh->bindParam(2, $idSucursal);
        $dbh->execute();
        $elementos = $dbh->fetchAll();
        $connection = null;
        $respuesta = array();
        foreach ($elementos as $elemento) {
            if($elemento["idEstatus"]==1)
          $respuesta[] = array('idSucursal' => $elemento["idSucursal"]
            ,'sucursal' => htmlentities($elemento["sucursal"])
            ,'direccion' => htmlentities($elemento["direccion"])
            ,'telefono' => htmlentities($elemento["telefono"])
            ,'correo' => htmlentities($elemento["correo"])
            ,'cp' => htmlentities($elemento["cp"])
            ,'idEstado' => $elemento["idEstado"]
            ,'idPais' => $elemento["idPais"]
            ,'idMunicipio' => $elemento["idMunicipio"]
            ,'principal' => $elemento["principal"]
            ,'idEstatus' => $elemento["idEstatus"]
            );
        }
    $data=array('data'=>$respuesta);
      $app->response->headers->set("Content-type", "application/json");
      $app->response->status(200);
      $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});
$app->get("/salones/:idSalon/sucursales/", function($idSalon) use($app){
	try{
        $idSucursal=0;
		$connection = getConnection();
		$dbh = $connection->prepare("CALL sp_getSucursales(?,?)");
		$dbh->bindParam(1, $idSalon);
        $dbh->bindParam(2, $idSucursal);
		$dbh->execute();
		$elementos = $dbh->fetchAll();
		$connection = null;
		$respuesta = array();
        foreach ($elementos as $elemento) {
          $respuesta[] = array('idSucursal' => $elemento["idSucursal"]
            ,'sucursal' => htmlentities($elemento["sucursal"])
            ,'direccion' => htmlentities($elemento["direccion"])
            ,'telefono' => htmlentities($elemento["telefono"])
            ,'correo' => htmlentities($elemento["correo"])
            ,'cp' => htmlentities($elemento["cp"])
            ,'idEstado' => $elemento["idEstado"]
            ,'idPais' => $elemento["idPais"]
            ,'idMunicipio' => $elemento["idMunicipio"]
            ,'principal' => $elemento["principal"]
            ,'idEstatus' => $elemento["idEstatus"]
            );
        }
    $data=array('data'=>$respuesta);
      $app->response->headers->set("Content-type", "application/json");
      $app->response->status(200);
      $app->response->body(json_encode($data, JSON_UNESCAPED_UNICODE));
	}
	catch(PDOException $e){
		echo "Error: " . $e->getMessage();
	}
});

$app->get("/salones/:idSalon/sucursales/:idSucursal", function($idSalon,$idSucursal) use($app){
    try{
        $connection = getConnection();
        $dbh = $connection->prepare("CALL sp_getSucursales(?,?)");
        $dbh->bindParam(1, $idSalon);
        $dbh->bindParam(2, $idSucursal);
        $dbh->execute();
        $elemento = $dbh->fetch();
        $connection = null;
        $respuesta = array();
        if(!empty($elemento)) {
          $respuesta = array('idSucursal' => $elemento["idSucursal"]
            ,'sucursal' => htmlentities($elemento["sucursal"])
            ,'direccion' => htmlentities($elemento["direccion"])
            ,'telefono' => htmlentities($elemento["telefono"])
            ,'correo' => htmlentities($elemento["correo"])
            ,'cp' => htmlentities($elemento["cp"])
            ,'idEstado' => $elemento["idEstado"]
            ,'idPais' => $elemento["idPais"]
            ,'idMunicipio' => $elemento["idMunicipio"]
            ,'principal' => $elemento["principal"]
            ,'idEstatus' => $elemento["idEstatus"]
            );
        }

        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});
?>