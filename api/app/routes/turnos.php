<?php 
$app->get("/salones/:idSalon/turnos/:idUsuario/:idSucursal/", function($idSalon,$idUsuario,$idSucursal) use($app){
 try{
    $idTurno=0;
    $connection = getConnection(); 
    $dbh = $connection->prepare("CALL sp_getTurnos(?,?,?,?)");
    $dbh->bindParam(1, $idSalon);
    $dbh->bindParam(2, $idSucursal);
    $dbh->bindParam(3, $idUsuario);
    $dbh->bindParam(4, $idTurno);
	    $dbh->execute();
    $elemento = $dbh->fetch();
    $connection = null;
    $respuesta = array();
    if(!empty($elemento)) {
      $respuesta= array('idTurno' => $elemento["idTurno"]
      ,'idSalon' => $elemento["idSalon"]
      ,'idSucursal' => $elemento["idSucursal"]
      ,'idUsuario' => $elemento["idUsuario"]
      ,'fondoInicial' => $elemento["fondoInicial"]
      ,'ventas' => $elemento["ventas"]
      ,'cancelaciones' => $elemento["cancelaciones"]
      ,'fecha' => $elemento["fecha"]
      ,'horaInicio' => $elemento["horaInicio"]
      ,'horaFin' => $elemento["horaFin"]
      ,'maximoRetiros' => $elemento["maximoRetiros"]
      ,'ventasTarjetas' => $elemento["ventasTarjetas"]
      ,'retirosEfectivo' => $elemento["retirosEfectivo"]
      ,'ventasEfectivo' => $elemento["ventasEfectivo"]
      ,'retirosTarjetas' => $elemento["retirosTarjetas"]
      ,'ventasRegalo' => $elemento["ventasRegalo"]
      ,'retirosRegalo' => $elemento["retirosRegalo"]
      );
      }
        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }

});

$app->get("/salones/:idSalon/turnos/:idUsuario/:idSucursal/vencidos/", function($idSalon,$idUsuario,$idSucursal) use($app){
 try{
      $idTurno=0;
        $connection = getConnection(); 
        $dbh = $connection->prepare("CALL sp_getTurnosVencidos(?,?,?,?)");
        $dbh->bindParam(1, $idSalon);
        $dbh->bindParam(2, $idSucursal);
        $dbh->bindParam(3, $idUsuario);
        $dbh->bindParam(4, $idTurno);
        $dbh->execute();
       $elementos = $dbh->fetchAll();
        $connection = null;
        $respuesta = array();
        foreach ($elementos as $elemento) {
          $respuesta[]= array('idTurno' => $elemento["idTurno"]
            ,'idSalon' => $elemento["idSalon"]
            ,'idSucursal' => $elemento["idSucursal"]
            ,'idUsuario' => $elemento["idUsuario"]
            ,'fondoInicial' => $elemento["fondoInicial"]
            ,'ventas' => $elemento["ventas"]
            ,'cancelaciones' => $elemento["cancelaciones"]
            ,'fecha' => $elemento["fecha"]
            ,'horaInicio' => $elemento["horaInicio"]
            ,'horaFin' => $elemento["horaFin"]
            ,'maximoRetiros' => $elemento["maximoRetiros"]
            ,'ventasTarjetas' => $elemento["ventasTarjetas"]
            ,'retirosEfectivo' => $elemento["retirosEfectivo"]
            ,'ventasEfectivo' => $elemento["ventasEfectivo"]
            ,'retirosTarjetas' => $elemento["retirosTarjetas"]
            ,'ventasRegalo' => $elemento["ventasRegalo"]
            ,'retirosRegalo' => $elemento["retirosRegalo"]
            );
        }
        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }

});


$app->post("/salones/:idSalon/turnos/", function($idSalon) use($app){
   try{
        $connection = getConnection(); 
        $idUsuario=$app->request->post('idUsuario');
        $idSucursal=$app->request->post('idSucursal');
        $fondo=$app->request->post('fondo');
        $retiros=$app->request->post('retiros');

        $dbh = $connection->prepare("CALL sp_addTurno(?,?,?,?,?)");
        $dbh->bindParam(1, $idSalon);
        $dbh->bindParam(2, $idSucursal);
        $dbh->bindParam(3, $idUsuario);
        $dbh->bindParam(4, $fondo);
        $dbh->bindParam(5, $retiros);
   	    $dbh->execute();
        $elemento = $dbh->fetch();
        $connection = null;
        $respuesta = array();
        if(!empty($elemento)) {
          $respuesta = array('respuesta' => $elemento["respuesta"]
            , 'mensaje' => htmlentities($elemento["mensaje"])
            );
        }
        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
}

    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});

$app->options("/salones/:idSalon/turnos/:idUsuario/:idSucursal/:idTurno/", function($idSalon,$idUsuario,$idSucursal,$idTurno) use($app){
	});
$app->put("/salones/:idSalon/turnos/:idUsuario/:idSucursal/:idTurno/", function($idSalon,$idUsuario,$idSucursal,$idTurno) use($app){
try{
        $connection = getConnection(); 
        $dbh = $connection->prepare("CALL sp_cerrarTurno(?,?,?,?)");
        $dbh->bindParam(1, $idSalon);
        $dbh->bindParam(2, $idSucursal);
        $dbh->bindParam(3, $idUsuario);
        $dbh->bindParam(4, $idTurno);
   	    $dbh->execute();
        $elemento = $dbh->fetch();
        $connection = null;
        $respuesta = array();
        if(!empty($elemento)) {
          $respuesta = array('respuesta' => $elemento["respuesta"]
            , 'mensaje' => htmlentities($elemento["mensaje"])
            );
        }
        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
}

    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});
