<?php
$app->get("/salones/:idSalon/tickets/activos/", function($idSalon) use($app){
    try{
        $idTicket =0;
        $connection = getConnection();
        $dbh = $connection->prepare("CALL sp_getTickets(?,?)");
        $dbh->bindParam(1, $idSalon);
        $dbh->bindParam(2, $idTicket);
        $dbh->execute();
        $elementos = $dbh->fetchAll();
        $connection = null;
        $respuesta = array();
        foreach ($elementos as $elemento) {
          if($elemento["idEstatus"]==1)
            $respuesta[]= array('idTicket' => $elemento["idTicket"]
            ,'idSucursal' => $elemento["idSucursal"]
            ,'idSalon' => $elemento["idSalon"]
            ,'idUsuario' => $elemento["idUsuario"]
            ,'idEstilista' => $elemento["idEstilista"]
            ,'idMoneda' => $elemento["idMoneda"]
            ,'fecha' => $elemento["fecha"]
            ,'horaInicio' => $elemento["horaInicio"]
            ,'horaFin' => $elemento["horaFin"]
            ,'total' => $elemento["total"]
            ,'productos' => $elemento["productos"]
            ,'servicios' => $elemento["servicios"]
            , 'idEstatus' => $elemento["idEstatus"]
            , 'observaciones' => htmlentities($elemento["observaciones"])
            , 'usuario' => htmlentities($elemento["usuario"])
            , 'idCliente' => $elemento["idCliente"]
            , 'cliente' => htmlentities($elemento["cliente"])
            , 'estilista' => htmlentities($elemento["estilista"])
            );
        }
       $data=array('data'=>$respuesta);
      $app->response->headers->set("Content-type", "application/json");
      $app->response->status(200);
      $app->response->body(json_encode($data, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});
$app->get("/salones/:idSalon/tickets/:idTicket/", function($idSalon,$idTicket) use($app){
    try{
        $connection = getConnection();
        $dbh = $connection->prepare("CALL sp_getTickets(?,?)");
        $dbh->bindParam(1, $idSalon);
        $dbh->bindParam(2, $idTicket);
         $dbh->execute();
        $elemento = $dbh->fetch();
       // $connection = null;
        $respuesta = array();
        if(!empty($elemento)) {
          $respuesta= array('idTicket' => $elemento["idTicket"]
          ,'idSucursal' => $elemento["idSucursal"]
          ,'idSalon' => $elemento["idSalon"]
          ,'idUsuario' => $elemento["idUsuario"]
          ,'idEstilista' => $elemento["idEstilista"]
          ,'idMoneda' => $elemento["idMoneda"]
          ,'fecha' => $elemento["fecha"]
          ,'horaInicio' => $elemento["horaInicio"]
          ,'horaFin' => $elemento["horaFin"]
          ,'total' => $elemento["total"]
          ,'productos' => $elemento["productos"]
          ,'servicios' => $elemento["servicios"]
          , 'idEstatus' => $elemento["idEstatus"]
          , 'observaciones' => htmlentities($elemento["observaciones"])
          , 'usuario' => htmlentities($elemento["usuario"])
          , 'idCliente' => $elemento["idCliente"]
          , 'cliente' => htmlentities($elemento["cliente"])
          , 'estilista' => htmlentities($elemento["estilista"])
          , 'detalleProductos' =>''
          );
        }
        $idDetalleTicket=0;
        $dbh = $connection->prepare("CALL sp_getTicketDetalle(?,?,?)");
        $dbh->bindParam(1, $idSalon);
        $dbh->bindParam(2, $idTicket);//idDetalleTicket
        $dbh->bindParam(3, $idDetalleTicket);//idDetalleTicket
        $dbh->execute();
        $elementos = $dbh->fetchAll();
        $connection = null;
        $productos = array();
        foreach ($elementos as $elemento) {
            $productos[]= array('idTicket' => $elemento["idTicket"]
            ,'idDetalleTicket' => $elemento["idDetalleTicket"]
            ,'idSalon' => $elemento["idSalon"]
            ,'idProducto' => $elemento["idProducto"]
            ,'total' => $elemento["total"]
            ,'cantidad' => $elemento["cantidad"]
            ,'idEstatus' => $elemento["idEstatus"]
            ,'comision' => $elemento["comision"]
            , 'nombreProducto' => htmlentities($elemento["nombreProducto"])
            , 'nombreServicio' => htmlentities($elemento["nombreServicio"])
            ,'idUsuario' => $elemento["idUsuario"]
            ,'descuento' => $elemento["descuento"]
        );
        };
        $respuesta["detalleProductos"]=$productos;
        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});

$app->post("/salones/:idSalon/tickets/:idSucursal/:idUsuario/:idCliente/:idEstilista/", function($idSalon,$idSucursal,$idUsuario,$idCliente,$idEstilista) use($app){
  try{
    $idMoneda=1;
    $connection = getConnection();
    $dbh = $connection->prepare("CALL sp_addTicket(?,?,?,?,?,?)");
    $dbh->bindParam(1, $idSalon);
    $dbh->bindParam(2, $idSucursal);
    $dbh->bindParam(3, $idUsuario);
    $dbh->bindParam(4, $idCliente);
    $dbh->bindParam(5, $idEstilista);
    $dbh->bindParam(6, $idMoneda);
    $dbh->execute();
    $elemento = $dbh->fetch();
    $connection = null;
    $respuesta = array();
    if(!empty($elemento)) {
    $respuesta = array('respuesta' => $elemento["respuesta"]
    , 'mensaje' => htmlentities($elemento["mensaje"])
    );
    }
    $data=array('data'=>$respuesta);
    $app->response->headers->set("Content-type", "application/json");
    $app->response->status(200);
    $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
  }
  catch(PDOException $e){
      echo "Error: " . $e->getMessage();
  }

});
$app->options("/salones/:idSalon/tickets/:idTicket/", function($idSalon,$idTicket)use($app) {
    //Return response headers
});

$app->put("/salones/:idSalon/tickets/:idTicket/", function($idSalon,$idTicket) use($app){
    try{
        $connection = getConnection(); 
        $idCategoria=$app->request->post('idCategoria');
       
   
        $dbh = $connection->prepare("CALL sp_editTicket(?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
        $dbh->bindParam(1, $idSalon);
      
        $dbh->execute();
        $elemento = $dbh->fetch();
        $connection = null;
        $respuesta = array('respuesta' => '----');
        if(!empty($elemento)) {
          $respuesta = array('respuesta' => $elemento["respuesta"]
            , 'mensaje' => htmlentities($elemento["mensaje"])
            );
        }
        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});
$app->delete("/salones/:idSalon/tickets/:idTicket/", function($idSalon,$idTicket) use($app){
    try{
        $connection = getConnection();
        $dbh = $connection->prepare("CALL sp_deleteTicket(?,?)");
        $dbh->bindParam(1, $idSalon);
        $dbh->bindParam(2, $idTicket);
        $dbh->execute();
        $elemento = $dbh->fetch();
        $connection = null;
        $respuesta = array();
        if(!empty($elemento)) {
          $respuesta = array('respuesta' => $elemento["respuesta"]
            , 'mensaje' => htmlentities($elemento["mensaje"])
            );
        }
        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});

$app->post("/salones/:idSalon/tickets/:idTicket/productos/", function($idSalon,$idTicket) use($app){
  try{
    $idProducto = $app->request->post('idProducto');
    $idServicio = $app->request->post('idServicio');
    $comision = $app->request->post('comision');
    $cantidad = $app->request->post('cantidad');
    $total = $app->request->post('total');
    $descuento = $app->request->post('descuento');
    $idUsuario = $app->request->post('idUsuario');
    $idProducto =empty($idProducto)?0:$idProducto;
    $idServicio=empty( $idServicio)?0:$idServicio;
    $connection = getConnection();

    $dbh = $connection->prepare("CALL sp_addTicketProducto(?,?,?,?,?,?,?,?,?)");
    $dbh->bindParam(1, $idSalon);
    $dbh->bindParam(2, $idTicket);
    $dbh->bindParam(3, $idProducto);
    $dbh->bindParam(4, $idServicio);
    $dbh->bindParam(5, $total);
    $dbh->bindParam(6, $cantidad);
    $dbh->bindParam(7, $comision);
    $dbh->bindParam(8, $idUsuario);
    $dbh->bindParam(9, $descuento);

    $dbh->execute();
    $elemento = $dbh->fetch();
    $connection = null;
    $respuesta = array();
    if(!empty($elemento)) {
    $respuesta = array('respuesta' => $elemento["respuesta"]
    , 'mensaje' => htmlentities($elemento["mensaje"])
    );
    }
    $data=array('data'=>$respuesta);
    $app->response->headers->set("Content-type", "application/json");
    $app->response->status(200);
    $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
  }
  catch(PDOException $e){
      echo "Error: " . $e->getMessage();
  }

});

$app->post("/salones/:idSalon/tickets/:idTicket/cerrar/", function($idSalon,$idTicket) use($app){
  try{
    $propina = 0;//$app->request->post('propina');
    $idFormaPago = $app->request->post('idFormaPago');
    $idFormaPago2 = $app->request->post('idFormaPago2');
    $total = $app->request->post('total');
    $efectivo = $app->request->post('efectivo');
    $tarjeta = $app->request->post('tarjeta');
    $regalo = $app->request->post('regalo');
    $connection = getConnection();

    $dbh = $connection->prepare("CALL sp_cerrarTicket(?,?,?,?,?,?,?,?)");
    $dbh->bindParam(1, $idSalon);
    $dbh->bindParam(2, $idTicket);
    $dbh->bindParam(3, $total);
    $dbh->bindParam(4, $efectivo);
    $dbh->bindParam(5, $tarjeta);
    $dbh->bindParam(6, $regalo);
    $dbh->bindParam(7, $historial); 
    $dbh->execute();
    $elemento = $dbh->fetch();
    $connection = null;
    $respuesta = array();
    if(!empty($elemento)) {
    $respuesta = array('respuesta' => $elemento["respuesta"]
    , 'mensaje' => htmlentities($elemento["mensaje"])
    );
    }
    $data=array('data'=>$respuesta);
    $app->response->headers->set("Content-type", "application/json");
    $app->response->status(200);
    $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
  }
  catch(PDOException $e){
      echo "Error: " . $e->getMessage();
  }

});
$app->options("/salones/:idSalon/tickets/:idTicket/detalle/:idDetalle", function($idSalon,$idTicket,$idDetalle)use($app) {
    //Return response headers
});

$app->delete("/salones/:idSalon/tickets/:idTicket/detalle/:idDetalle", function($idSalon,$idTicket,$idDetalle)use($app) {
     try{
        $connection = getConnection();
        $dbh = $connection->prepare("CALL sp_deleteTicketDetalle(?,?,?)");
        $dbh->bindParam(1, $idSalon);
        $dbh->bindParam(2, $idTicket);
        $dbh->bindParam(3, $idDetalle);
        $dbh->execute();
        $elemento = $dbh->fetch();
        $connection = null;
        $respuesta = array();
        if(!empty($elemento)) {
          $respuesta = array('respuesta' => $elemento["respuesta"]
            , 'mensaje' => htmlentities($elemento["mensaje"])
            );
        }
        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});