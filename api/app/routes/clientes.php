<?php

$app->get("/salones/:idSalon/clientes/", function($idSalon) use($app){
	try{
		$idCliente=0;
		$connection = getConnection();
		$dbh = $connection->prepare("CALL sp_getClientes(?,?)");
		$dbh->bindParam(1, $idSalon);
		$dbh->bindParam(2, $idCliente);
		$dbh->execute();
		$elementos = $dbh->fetchAll();
		$connection = null;
		$respuesta = array();
      foreach ($elementos as $elemento) {
        $respuesta[] = array('idCliente' => $elemento["idCliente"]
          , 'nombre' => htmlentities($elemento["nombre"])
          , 'apellidos' => htmlentities($elemento["apellidos"])
          , 'correo' => htmlentities($elemento["correo"])
          , 'telefono' => htmlentities($elemento["telefono"])
          ,'idSexo' => $elemento["idSexo"]
          ,'compras' => $elemento["compras"]
          ,'idSalon' => $elemento["idSalon"]
          ,'dia' => $elemento["dia"]
          ,'mes' => $elemento["mes"]
          ,'historial' => $elemento["historial"]
          ,'fechaRegistro' => $elemento["fechaRegistro"]
          ,'idEstatus' => $elemento["idEstatus"]
          );
      }
      $data=array('data'=>$respuesta);
      $app->response->headers->set("Content-type", "application/json");
      $app->response->status(200);
      $app->response->body(json_encode($data, JSON_UNESCAPED_UNICODE));
	}
	catch(PDOException $e){
		echo "Error: " . $e->getMessage();
	}
});
$app->get("/salones/:idSalon/clientes/web", function($idSalon) use($app){
	try{
		$idCliente=0;
		$connection = getConnection();
		$dbh = $connection->prepare("CALL sp_getClientes(?,?)");
		$dbh->bindParam(1, $idSalon);
		$dbh->bindParam(2, $idCliente);
		$dbh->execute();
		$elementos = $dbh->fetchAll();
		$connection = null;
		$respuesta = array();
      foreach ($elementos as $elemento) {
        $respuesta[] = array('idCliente' => $elemento["idCliente"]
          , 'nombre' => htmlentities($elemento["nombre"])
          , 'apellidos' => htmlentities($elemento["apellidos"])
          , 'correo' => htmlentities($elemento["correo"])
          , 'telefono' => htmlentities($elemento["telefono"])
          ,'idSexo' => $elemento["idSexo"]
          ,'compras' => $elemento["compras"]
          ,'idSalon' => $elemento["idSalon"]
          ,'dia' => $elemento["dia"]
          ,'mes' => $elemento["mes"]
          ,'historial' => $elemento["historial"]
          ,'fechaRegistro' => $elemento["fechaRegistro"]
          ,'idEstatus' => $elemento["idEstatus"]
          );
      }
      $app->response->headers->set("Content-type", "application/json");
      $app->response->status(200);
      $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
	}
	catch(PDOException $e){
		echo "Error: " . $e->getMessage();
	}
});
$app->get("/salones/:idSalon/clientes/:idCliente/", function($idSalon,$idCliente) use($app){
	try{
		$connection = getConnection();
		$dbh = $connection->prepare("CALL sp_getClientes(?,?)");
		$dbh->bindParam(1, $idSalon);
		$dbh->bindParam(2, $idCliente);
		$dbh->execute();
		$elemento = $dbh->fetch();
    $connection = null;
    $respuesta = array();
    if(!empty($elemento)) {
        $respuesta = array('idCliente' => $elemento["idCliente"]
          , 'nombre' => htmlentities($elemento["nombre"])
          , 'apellidos' => htmlentities($elemento["apellidos"])
          , 'correo' => htmlentities($elemento["correo"])
          , 'telefono' => htmlentities($elemento["telefono"])
          ,'idSexo' => $elemento["idSexo"]
          ,'compras' => $elemento["compras"]
          ,'idSalon' => $elemento["idSalon"]
          ,'dia' => $elemento["dia"]
          ,'mes' => $elemento["mes"]
          ,'historial' => $elemento["historial"]
          ,'fechaRegistro' => $elemento["fechaRegistro"]
          ,'idEstatus' => $elemento["idEstatus"]
          );
      }
      $app->response->headers->set("Content-type", "application/json");
      $app->response->status(200);
      $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
	}
	catch(PDOException $e){
		echo "Error: " . $e->getMessage();
	}
});
$app->get("/salones/:idSalon/clientes/autocomplete/:cliente", function($idSalon,$cliente) use($app){
  try{
    $connection = getConnection();
    $dbh = $connection->prepare("CALL sp_getClientesAutomplete(?,?)");
    $dbh->bindParam(1, $idSalon);
    $dbh->bindParam(2, $cliente);
    $dbh->execute();
    $elementos = $dbh->fetchAll();
    $connection = null;
    $respuesta = array();
      foreach ($elementos as $elemento) {
        $respuesta[] =array('id' => $elemento["idCliente"]
        , 'value' =>  htmlentities($elemento["nombre"]).' '.htmlentities($elemento["apellidos"])
        , 'label' =>  htmlentities($elemento["nombre"]).' '.htmlentities($elemento["apellidos"])
          );
      }
      $app->response->headers->set("Content-type", "application/json");
      $app->response->status(200);
      $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
  }
  catch(PDOException $e){
    echo "Error: " . $e->getMessage();
  }
});

$app->post("/salones/:idSalon/clientes/", function($idSalon) use($app){
  try{
    $connection = getConnection(); 
    $nombre=$app->request->post('nombre');
    $apellidos=$app->request->post('apellidos');
    $correo=$app->request->post('correo');
    $telefono=$app->request->post('telefono');
    $idSexo=$app->request->post('idSexo');
    $dia=$app->request->post('dia');
    $mes=$app->request->post('mes');
    $historial=$app->request->post('historial');
    $dbh = $connection->prepare("CALL sp_addCliente(?,?,?,?,?,?,?,?,?)");
    $dbh->bindParam(1, $idSalon);
    $dbh->bindParam(2, $nombre);
    $dbh->bindParam(3, $apellidos);
    $dbh->bindParam(4, $correo);
    $dbh->bindParam(5, $telefono);
    $dbh->bindParam(6, $idSexo);
    $dbh->bindParam(7, $mes);
    $dbh->bindParam(8, $dia);
    $dbh->bindParam(9, $historial);
    $dbh->execute();
    $elemento = $dbh->fetch();
    $connection = null;
    $respuesta = array();
    if(!empty($elemento)) {
      $respuesta = array('respuesta' => $elemento["respuesta"]
        , 'mensaje' => htmlentities($elemento["mensaje"])
        );
    }
    $app->response->headers->set("Content-type", "application/json");
    $app->response->status(200);
    $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
  }catch(PDOException $e){
    echo "Error: " . $e->getMessage();
  }

});
$app->options("/salones/:idSalon/clientes/:idCliente/", function($idSalon,$idCliente) use($app){


});
$app->put("/salones/:idSalon/clientes/:idCliente/", function($idSalon,$idCliente) use($app){
try{
    $connection = getConnection(); 
    $nombre=$app->request->post('nombre');
    $apellidos=$app->request->post('apellidos');
    $correo=$app->request->post('correo');
    $telefono=$app->request->post('telefono');
    $idSexo=$app->request->post('idSexo');
    $dia=$app->request->post('dia');
    $mes=$app->request->post('mes');
//1,1,'Raquel','Rojas','raquel@rojas.com','1234568',2,'1990-12-01'
    $dbh = $connection->prepare("CALL sp_editCliente(?,?,?,?,?,?,?,?,?)");
    $dbh->bindParam(1, $idSalon);
    $dbh->bindParam(2, $idCliente);
    $dbh->bindParam(3, $nombre);
    $dbh->bindParam(4, $apellidos);
    $dbh->bindParam(5, $correo);
    $dbh->bindParam(6, $telefono);
    $dbh->bindParam(7, $idSexo);
    $dbh->bindParam(8, $mes);
    $dbh->bindParam(9, $dia);
    $dbh->execute();
    $elemento = $dbh->fetch();
    $connection = null;
    $respuesta = array();
    if(!empty($elemento)) {
      $respuesta = array('respuesta' => $elemento["respuesta"]
        , 'mensaje' => htmlentities($elemento["mensaje"])
        );
    }
    $app->response->headers->set("Content-type", "application/json");
    $app->response->status(200);
    $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
  }catch(PDOException $e){
    echo "Error: " . $e->getMessage();
  }

});

$app->get("/salones/:idSalon/citas/:fecha/", function($idSalon,$fecha) use($app){
try{
    $idCita=0;
    $connection = getConnection();
    $dbh = $connection->prepare("CALL sp_getCitas(?,?,?)");
    $dbh->bindParam(1, $idSalon);
    $dbh->bindParam(2, $idCita);
    $dbh->bindParam(3, $fecha);
    $dbh->execute();
    $elementos = $dbh->fetchAll();
    $connection = null;
    $respuesta = array();
      foreach ($elementos as $elemento) {
        $respuesta[] = array('idCita' => $elemento["idCita"]
          ,'idCliente' => $elemento["idCliente"]
          ,'idSucursal' => $elemento["idSucursal"]
          ,'idEstilista' => $elemento["idEstilista"]
          ,'idEstatus' => $elemento["idEstatus"]
          ,'comentarios' => htmlentities($elemento["comentarios"])
          ,'estilista' => htmlentities($elemento["estilista"])
          ,'cliente' => htmlentities($elemento["cliente"])
          ,'titulo' => htmlentities($elemento["titulo"])
          ,'fecha' => $elemento["fecha"]
          ,'horaInicio' => $elemento["horaInicio"]
          ,'horaFin' => $elemento["horaFin"]
          ,'duracion' => $elemento["duracion"]
          ,'idSalon' => $elemento["idSalon"]
          ,'categoria' => htmlentities($elemento["categoria"])
          ,'producto' => htmlentities($elemento["producto"])
          ,'idServicio' => $elemento["idServicio"]
          ,'idCategoria' => $elemento["idCategoria"]
          ,'comision' => $elemento["comision"]
          );
      }
      
      $data=array('data'=>$respuesta);
      $app->response->headers->set("Content-type", "application/json");
      $app->response->status(200);
      $app->response->body(json_encode($data, JSON_UNESCAPED_UNICODE));
  }
  catch(PDOException $e){
    echo "Error: " . $e->getMessage();
  }

});

$app->get("/salones/:idSalon/cita/:idCita/", function($idSalon,$idCita) use($app){
try{
  $fecha='2010-10-01';
    $connection = getConnection();
    $dbh = $connection->prepare("CALL sp_getCitas(?,?,?)");
    $dbh->bindParam(1, $idSalon);
    $dbh->bindParam(2, $idCita);
    $dbh->bindParam(3, $fecha);
     $dbh->execute();
    $elemento = $dbh->fetch();
    $connection = null;
    $respuesta = array();
    if(!empty($elemento)) {
      $respuesta = array('idCita' => $elemento["idCita"]
          ,'idCliente' => $elemento["idCliente"]
          ,'idSucursal' => $elemento["idSucursal"]
          ,'idEstilista' => $elemento["idEstilista"]
          ,'idEstatus' => $elemento["idEstatus"]
          ,'comentarios' => htmlentities($elemento["comentarios"])
          ,'estilista' => htmlentities($elemento["estilista"])
          ,'cliente' => htmlentities($elemento["cliente"])
          ,'titulo' => htmlentities($elemento["titulo"])
          ,'fecha' => $elemento["fecha"]
          ,'horaInicio' => $elemento["horaInicio"]
          ,'horaFin' => $elemento["horaFin"]
          ,'duracion' => $elemento["duracion"]
          ,'idSalon' => $elemento["idSalon"]
          ,'categoria' => htmlentities($elemento["categoria"])
          ,'producto' => htmlentities($elemento["producto"])
          ,'idServicio' => $elemento["idServicio"]
          ,'idCategoria' => $elemento["idCategoria"]
          ,'comision' => $elemento["comision"]
          );
      }
        $data=array('data'=>$respuesta);
      $app->response->headers->set("Content-type", "application/json");
      $app->response->status(200);
      $app->response->body(json_encode($data, JSON_UNESCAPED_UNICODE));
  }
  catch(PDOException $e){
    echo "Error: " . $e->getMessage();
  }

});
$app->get("/salones/:idSalon/citaCalendar/:fecha/", function($idSalon,$fecha) use($app){
try{
    $idCita=0;
    $connection = getConnection();
    $dbh = $connection->prepare("CALL sp_getCitas(?,?,?)");
    $dbh->bindParam(1, $idSalon);
    $dbh->bindParam(2, $idCita);
    $dbh->bindParam(3, $fecha);
    $dbh->execute();
    $elementos = $dbh->fetchAll();
    $connection = null;
    $respuesta = array();
      foreach ($elementos as $elemento) {
        $respuesta[] = array('idCita' => $elemento["idCita"]
          ,'idCliente' => $elemento["idCliente"]
          ,'idSucursal' => $elemento["idSucursal"]
          ,'idEstilista' => $elemento["idEstilista"]
          ,'idEstatus' => $elemento["idEstatus"]
          ,'comentarios' => htmlentities($elemento["comentarios"])
          ,'estilista' => htmlentities($elemento["estilista"])
          ,'cliente' => htmlentities($elemento["cliente"])
          ,'title' => htmlentities($elemento["titulo"]).': '.htmlentities($elemento["cliente"])
          ,'start' => $elemento["fecha"].'T'.$elemento["horaInicio"]
          ,'end' => $elemento["fecha"].'T'.$elemento["horaFin"]
          ,'horaInicio' => $elemento["horaInicio"]
          ,'horaFin' => $elemento["horaFin"]
          ,'duracion' => $elemento["duracion"]
          ,'idSalon' => $elemento["idSalon"]
          ,'categoria' => htmlentities($elemento["categoria"])
          ,'producto' => htmlentities($elemento["producto"])
          ,'idServicio' => $elemento["idServicio"]
          ,'idCategoria' => $elemento["idCategoria"]
          ,'comision' => $elemento["comision"]
          );
      }
      

      $app->response->headers->set("Content-type", "application/json");
      $app->response->status(200);
      $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
  }
  catch(PDOException $e){
    echo "Error: " . $e->getMessage();
  }

});

$app->post("/salones/:idSalon/citas/", function($idSalon) use($app){
  try{
    $connection = getConnection(); 
    $idCliente=$app->request->post('idCliente');
    $idSucursal=$app->request->post('idSucursal');
    $idEstilista=$app->request->post('idEstilista');
    $fecha=$app->request->post('fecha');
    $horaInicio=$app->request->post('horaInicio');
    $comentarios=$app->request->post('comentarios');
    $duracion=$app->request->post('duracion');
    $titulo=$app->request->post('titulo');
    $horaFin=$app->request->post('horaFin');
    $idCategoria=$app->request->post('idCategoria');
    $idServicio=$app->request->post('idServicio');
    $comision=$app->request->post('comision');
    $dbh = $connection->prepare("CALL sp_addCita(?,?,?,?,?,?,?,?,?,?,?,?,?)");
    $dbh->bindParam(1, $idSalon);
    $dbh->bindParam(2, $idCliente);
    $dbh->bindParam(3, $idSucursal);
    $dbh->bindParam(4, $idEstilista);
    $dbh->bindParam(5, $fecha);
    $dbh->bindParam(6, $horaInicio);
    $dbh->bindParam(7, $comentarios);
    $dbh->bindParam(8, $duracion);
    $dbh->bindParam(9, $titulo);
    $dbh->bindParam(10, $horaFin);
    $dbh->bindParam(11, $idCategoria);
    $dbh->bindParam(12, $idServicio);
    $dbh->bindParam(13, $comision);
    $dbh->execute();

    $elemento = $dbh->fetch();
    $connection = null;
    $respuesta = array();
    if(!empty($elemento)) {
      $respuesta = array('respuesta' => $elemento["respuesta"]
        , 'mensaje' => htmlentities($elemento["mensaje"])
        );
    }
    $app->response->headers->set("Content-type", "application/json");
    $app->response->status(200);
    $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
  }catch(PDOException $e){
    echo "Error: " . $e->getMessage();
  }

});
$app->options("/salones/:idSalon/citas/:idCita", function($idSalon,$idCita) use($app){


});
$app->delete("/salones/:idSalon/citas/:idCita", function($idSalon,$idCita) use($app){
    try{
        $connection = getConnection();
        $dbh = $connection->prepare("CALL sp_deleteCita(?,?)");
        $dbh->bindParam(1, $idSalon);
        $dbh->bindParam(2, $idCita);
        $dbh->execute();
        $elemento = $dbh->fetch();
        $connection = null;
        $respuesta = array();
        if(!empty($elemento)) {
          $respuesta = array('respuesta' => $elemento["respuesta"]
            , 'mensaje' => htmlentities($elemento["mensaje"])
            );
        }
        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});

$app->put("/salones/:idSalon/citas/:idCita", function($idSalon,$idCita) use($app){
  try{
    $connection = getConnection(); 
    $idCliente=0;
    $idSucursal=$app->request->post('idSucursal');
    $idEstilista=$app->request->post('idEstilista');
    $fecha=$app->request->post('fecha');
    $horaInicio=$app->request->post('horaInicio');
    $comentarios=$app->request->post('comentarios');
    $duracion=$app->request->post('duracion');
    $titulo=$app->request->post('titulo');
    $horaFin=$app->request->post('horaFin');
    $comision=$app->request->post('comision');
    $idCategoria=$app->request->post('idCategoria');
    $idServicio=$app->request->post('idServicio');

    $dbh = $connection->prepare("CALL sp_editCita(?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
    $dbh->bindParam(1, $idSalon);
    $dbh->bindParam(2, $idCita);
    $dbh->bindParam(3, $idCliente);
    $dbh->bindParam(4, $idSucursal);
    $dbh->bindParam(5, $idEstilista);
    $dbh->bindParam(6, $fecha);
    $dbh->bindParam(7, $horaInicio);
    $dbh->bindParam(8, $comentarios);
    $dbh->bindParam(9, $duracion);
    $dbh->bindParam(10, $titulo);
    $dbh->bindParam(11, $horaFin);
    $dbh->bindParam(12, $idCategoria);
    $dbh->bindParam(13, $idServicio);
    $dbh->bindParam(14, $comision);
//1,10,0,1,6,'2018-08-17','9:00','nueva cita1',0,'Cambio de cita','10:00',1,2,0.15
    $dbh->execute();
    $elemento = $dbh->fetch();
    $connection = null;
    $respuesta = array();
    if(!empty($elemento)) {
      $respuesta = array('respuesta' => $elemento["respuesta"]
        , 'mensaje' => htmlentities($elemento["mensaje"])
        );
    }
    $app->response->headers->set("Content-type", "application/json");
    $app->response->status(200);
    $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
  }catch(PDOException $e){
    echo "Error: " . $e->getMessage();
  }

});

$app->get("/salones/:idSalon/citas/tickets/:hora/:minutos", function($idSalon,$hora,$minutos) use($app){
     try{
          $horaInicio='09:00:00';
          $horaFin='10:00:00';

      if($minutos>=50){
        $minutos=($minutos+10)-60;
        $hora=$hora+1;
      }else{
        $minutos=$minutos+10;
      }
      $horaFin=($hora<10?'0'.$hora:$hora).':'.($minutos<10?'0'.$minutos:$minutos).':00';
    $connection = getConnection();
    $dbh = $connection->prepare("CALL sp_getCitasTickets(?,?,?)");
    $dbh->bindParam(1, $idSalon);
    $dbh->bindParam(2, $horaInicio);
    $dbh->bindParam(3, $horaFin);
    $dbh->execute();
    $elementos = $dbh->fetchAll();
    $respuesta = array();
    $respuestas = array();
      foreach ($elementos as $elemento) {
        $respuesta[] = array('idCita' => $elemento["idCita"]
          ,'idCliente' => $elemento["idCliente"]
          ,'idSalon' => $elemento["idSalon"]
          ,'idSucursal' => $elemento["idSucursal"]
          ,'idEstilista' => $elemento["idEstilista"]
          ,'comentarios' => htmlentities($elemento["comentarios"])
          ,'fecha' => $elemento["fecha"]
          ,'horaInicio' => $elemento["horaInicio"]
           ,'idServicio' => $elemento["idServicio"]
          ,'idCategoria' => $elemento["idCategoria"]
         ,'idUsuario' => $elemento["idUsuario"]
         ,'comision' => $elemento["comision"]
         ,'total' => $elemento["total"]
          );
      }
      $moneda=1;
      //if(count($respuesta)>0){
     foreach ($respuesta as $key => $value) {
        $dbh = $connection->prepare("CALL sp_addTicket(?,?,?,?,?,?)");
        $dbh->bindParam(1, $value["idSalon"]);
        $dbh->bindParam(2, $value["idSucursal"]);
        $dbh->bindParam(3, $value["idUsuario"]);
        $dbh->bindParam(4, $value["idCliente"]);
        $dbh->bindParam(5, $value["idEstilista"]);
        $dbh->bindParam(6,  $moneda);
        $dbh->execute();
        $elemento = $dbh->fetch();
         if(!empty($elemento)) {
          if($elemento["respuesta"]>0){
            $cantidad=1;
             $dbh = $connection->prepare("CALL sp_addTicketProducto(?,?,?,?,?,?,?,?)");
              $dbh->bindParam(1, $value["idSalon"]);
              $dbh->bindParam(2, $elemento["respuesta"]);
              $dbh->bindParam(3, $value["idProducto"]);
              $dbh->bindParam(4, $value["idServicio"]);
              $dbh->bindParam(5, $value["total"]);
              $dbh->bindParam(6, $cantidad);
              $dbh->bindParam(7, $value["comision"]);
              $dbh->bindParam(8, $value["idUsuario"]);
              $dbh->execute();
              $elemento2 = $dbh->fetch();
          }

            $respuestas[] = array('respuesta' => $elemento["respuesta"]
            , 'mensaje' => htmlentities($elemento["mensaje"])
            );
            }
    
      }
 $connection = null;
    //}
      $data=array('data'=>$respuestas);

   


        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($data, JSON_UNESCAPED_UNICODE));
 }catch(PDOException $e){
    echo "Error: " . $e->getMessage();
  }

});