<?php 
$app->get("/sitios/:idSitio/comentarios/web/", function($idSitio) use($app){
    try{
        $idComentario=0;
        $connection = getConnection();
        $dbh = $connection->prepare("CALL sp_getComentarios(?,?)");
        $dbh->bindParam(1, $idSitio);
        $dbh->bindParam(2, $idComentario);
        $dbh->execute();
         $elementos = $dbh->fetchAll();
        $connection = null;
        $respuesta = array();
        foreach ($elementos as $elemento) {
            if($elemento["idEstatus"]==1){
              $respuesta[] = array('idComentario' => $elemento["idComentario"]
                ,'idTipoComentario' => $elemento["idTipoComentario"]
                , 'comentario' => htmlentities($elemento["comentario"])
                , 'fuente' => htmlentities($elemento["fuente"])
                , 'nombre' => htmlentities($elemento["nombre"])
                , 'clase' => htmlentities($elemento["clase"])
                , 'url' => htmlentities($elemento["url"])
                ,'idEstatus' => $elemento["idEstatus"]
                , 'tipo' => htmlentities($elemento["tipo"])
                , 'icono' => htmlentities($elemento["icono"])
                );
          }
        }
        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});
$app->get("/sitios/:idSitio/comentarios/", function($idSitio) use($app){
    try{
        $idComentario=0;
        $connection = getConnection();
        $dbh = $connection->prepare("CALL sp_getComentarios(?,?)");
        $dbh->bindParam(1, $idSitio);
        $dbh->bindParam(2, $idComentario);
        $dbh->execute();
         $elementos = $dbh->fetchAll();
        $connection = null;
        $respuesta = array();
        foreach ($elementos as $elemento) {
          $respuesta[] = array('idComentario' => $elemento["idComentario"]
                ,'idTipoComentario' => $elemento["idTipoComentario"]
                , 'comentario' => htmlentities($elemento["comentario"])
                , 'fuente' => htmlentities($elemento["fuente"])
                , 'nombre' => htmlentities($elemento["nombre"])
                , 'clase' => htmlentities($elemento["clase"])
                , 'url' => htmlentities($elemento["url"])
                ,'idEstatus' => $elemento["idEstatus"]
                , 'tipo' => htmlentities($elemento["tipo"])
                , 'icono' => htmlentities($elemento["icono"])
                );
        }



        $data=array('data'=>$respuesta);
        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($data, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});
$app->get("/sitios/:idSitio/comentarios/:idComentario", function($idSitio,$idComentario) use($app){
    try{
        $connection = getConnection();
        $dbh = $connection->prepare("CALL sp_getComentarios(?,?)");
        $dbh->bindParam(1, $idSitio);
        $dbh->bindParam(2, $idComentario);
        $dbh->execute();
        $elemento = $dbh->fetch();
        $connection = null;
        $respuesta = array();
        if(!empty($elemento)) {
          $respuesta = array('idComentario' => $elemento["idComentario"]
                ,'idTipoComentario' => $elemento["idTipoComentario"]
                , 'comentario' => htmlentities($elemento["comentario"])
                , 'fuente' => htmlentities($elemento["fuente"])
                , 'nombre' => htmlentities($elemento["nombre"])
                , 'clase' => htmlentities($elemento["clase"])
                , 'url' => htmlentities($elemento["url"])
                ,'idEstatus' => $elemento["idEstatus"]
                , 'tipo' => htmlentities($elemento["tipo"])
                , 'icono' => htmlentities($elemento["icono"])
                );
        }

        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});

$app->post("/sitios/:idSitio/comentarios/", function($idSitio) use($app){
   try{
        $connection = getConnection();
        $idComentarioTipo=$app->request->post('idComentarioTipo');
        $comentario=$app->request->post('comentario');
        $fuente=$app->request->post('fuente');
        $nombre=$app->request->post('nombre');
        $clase=$app->request->post('clase');
        $orden=$app->request->post('orden');
        $url=$app->request->post('url');
        $idEstatus=$app->request->post('idEstatus');
        $dbh = $connection->prepare("CALL sp_addComentario(?,?,?,?,?,?,?,?)");
        $dbh->bindParam(1, $idSitio);
        $dbh->bindParam(2, $idComentarioTipo);
        $dbh->bindParam(3, $comentario);
        $dbh->bindParam(4, $fuente);
        $dbh->bindParam(5, $nombre);
        $dbh->bindParam(6, $clase);
        $dbh->bindParam(7, $url);
        $dbh->bindParam(8, $idEstatus);
        $dbh->execute();
        $elemento = $dbh->fetch();
        $connection = null;
        $respuesta = array();
        if(!empty($elemento)) {
          $respuesta = array('respuesta' => $elemento["respuesta"]
            , 'mensaje' => htmlentities(utf8_encode($elemento["mensaje"]))
            );
        }
        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});

$app->options("/sitios/:idSitio/comentarios/:idComentario", function($idSitio,$idComentario) {
    //Return response headers
});
$app->put("/sitios/:idSitio/comentarios/:idComentario", function($idSitio,$idComentario) use($app){
    try{
        $connection = getConnection();
        $idComentarioTipo=$app->request->post('idComentarioTipo');
        $comentario=$app->request->post('comentario');
        $fuente=$app->request->post('fuente');
        $nombre=$app->request->post('nombre');
        $clase=$app->request->post('clase');
        $orden=$app->request->post('orden');
        $url=$app->request->post('url');
        $idEstatus=$app->request->post('idEstatus');
        $dbh = $connection->prepare("CALL sp_editComentario(?,?,?,?,?,?,?,?,?)");
        $dbh->bindParam(1, $idSitio);
        $dbh->bindParam(2, $idComentario);
        $dbh->bindParam(3, $idComentarioTipo);
        $dbh->bindParam(4, $comentario);
        $dbh->bindParam(5, $fuente);
        $dbh->bindParam(6, $nombre);
        $dbh->bindParam(7, $clase);
        $dbh->bindParam(8, $url);
        $dbh->bindParam(9, $idEstatus);
        $dbh->execute();
        $elemento = $dbh->fetch();
        $connection = null;
        $respuesta = array();
        if(!empty($elemento)) {
          $respuesta = array('respuesta' => $elemento["respuesta"]
            , 'mensaje' => htmlentities(utf8_encode($elemento["mensaje"]))
            );
        }
        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});