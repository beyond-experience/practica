<?php
$app->get("/sitios/:idSitio/paginas/", function($idSitio) use($app){
    try{
        $idPage=0;
        $connection = getConnection();
        $dbh = $connection->prepare("CALL sp_getPaginas(?,?)");
        $dbh->bindParam(1, $idSitio);
        $dbh->bindParam(2, $idPage);
        $dbh->execute();
         $elementos = $dbh->fetchAll();
        $connection = null;
        $respuesta = array();
        foreach ($elementos as $elemento) {
          $respuesta[] = array('idPagina' => $elemento["idPagina"]
           , 'pagina' => htmlentities(utf8_encode($elemento["pagina"]))
            , 'urlSeo' => htmlentities(utf8_encode($elemento["urlSeo"]))
            , 'urlCanonica' => htmlentities($elemento["urlCanonica"])
            , 'titulo' => htmlentities($elemento["titulo"])
            , 'descripcion' => htmlentities($elemento["descripcion"])
            , 'keywords' => htmlentities($elemento["keywords"])
            , 'imagenPagina' => htmlentities($elemento["imagenPagina"])
        );
        }
         $data=array('data'=>$respuesta);
        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($data, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});
$app->get("/sitios/:idSitio/paginas/:idPage", function($idSitio,$idPage) use($app){
    try{
        $connection = getConnection();
        $dbh = $connection->prepare("CALL sp_getPaginas(?,?)");
        $dbh->bindParam(1, $idSitio);
        $dbh->bindParam(2, $idPage);
        $dbh->execute();
        $elemento = $dbh->fetch();
        $connection = null;
        $respuesta = array();
        if(!empty($elemento)) {
          $respuesta = array('idPagina' => $elemento["idPagina"]
           , 'pagina' => htmlentities(utf8_encode($elemento["pagina"]))
            , 'urlSeo' => htmlentities(utf8_encode($elemento["urlSeo"]))
            , 'urlCanonica' => htmlentities($elemento["urlCanonica"])
            , 'titulo' => htmlentities($elemento["titulo"])
            , 'descripcion' => htmlentities($elemento["descripcion"])
            , 'keywords' => htmlentities($elemento["keywords"])
            , 'imagenPagina' => htmlentities($elemento["imagenPagina"])
        );
        }
        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});

$app->post("/sitios/:idSitio/paginas/", function($idSitio) use($app){
    try{
        
       $connection = getConnection();
        $pagina=utf8_decode($app->request->post('pagina'));
        $urlSeo=utf8_decode($app->request->post('urlSeo'));
        $urlCanonica=utf8_decode($app->request->post('urlCanonica'));
        $titulo=utf8_decode($app->request->post('titulo'));
        $descripcion=utf8_decode($app->request->post('descripcion'));
        $imagenPagina=$app->request->post('imagenPagina');
        $dbh = $connection->prepare("CALL sp_addPagina(?,?,?,?,?,?,?,?)");
        $dbh->bindParam(1, $idSitio);
        $dbh->bindParam(2, $pagina);
        $dbh->bindParam(3, $urlSeo);
        $dbh->bindParam(4, $urlCanonica);
        $dbh->bindParam(5, $titulo);
        $dbh->bindParam(6, $descripcion);
        $dbh->bindParam(7, $keywords);
        $dbh->bindParam(8, $imagenPagina);
        $dbh->execute();
        $elemento = $dbh->fetch();
        $connection = null;
        $respuesta = array();
        if(!empty($elemento)) {
          $respuesta = array('respuesta' => $elemento["respuesta"]
            , 'mensaje' => htmlentities(utf8_encode($elemento["mensaje"]))
            );
        }
        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});
$app->options("/sitios/:idSitio/paginas/:idPage", function($idSitio,$idPage) {
    //Return response headers
});
$app->put("/sitios/:idSitio/paginas/:idPage", function($idSitio,$idPage) use($app){
    try{
      $connection = getConnection();
        $pagina=utf8_decode($app->request->post('pagina'));
        $urlSeo=utf8_decode($app->request->post('urlSeo'));
        $urlCanonica=utf8_decode($app->request->post('urlCanonica'));
        $titulo=utf8_decode($app->request->post('titulo'));
        $descripcion=utf8_decode($app->request->post('descripcion'));
        $imagenPagina=$app->request->post('imagenPagina');
        $dbh = $connection->prepare("CALL sp_editPagina(?,?,?,?,?,?,?,?,?)");
        $dbh->bindParam(1, $idSitio);
        $dbh->bindParam(2, $idPage);
        $dbh->bindParam(3, $pagina);
        $dbh->bindParam(4, $urlSeo);
        $dbh->bindParam(5, $urlCanonica);
        $dbh->bindParam(6, $titulo);
        $dbh->bindParam(7, $descripcion);
        $dbh->bindParam(8, $keywords);
        $dbh->bindParam(9, $imagenPagina);
        $dbh->execute();
        $elemento = $dbh->fetch();
        $connection = null;
        $respuesta = array();
        if(!empty($elemento)) {
          $respuesta = array('respuesta' => $elemento["respuesta"]
            , 'mensaje' => htmlentities(utf8_encode($elemento["mensaje"]))
            );
        }
        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});

