<?php
$app->get("/sitios/:idSitio/categorias/activas/", function($idSitio) use($app){
    try{
        $idCategoria =0;
        $connection = getConnection();
        $dbh = $connection->prepare("CALL sp_getCategorias(?,?)");
        $dbh->bindParam(1, $idSitio);
        $dbh->bindParam(2, $idCategoria);
        $dbh->execute();
        $elementos = $dbh->fetchAll();
        $connection = null;
        $respuesta = array();
        foreach ($elementos as $elemento) {
            if($elemento["idEstatus"]==1){
                 $respuesta[]= array('idCategoria' => $elemento["idCategoria"]
                ,'idSitio' => $elemento["idSitio"]
                ,'categoria' => htmlentities($elemento["categoria"])
                ,'titulo' => htmlentities($elemento["titulo"])
                ,'subTitulo' => htmlentities($elemento["subTitulo"])
                ,'descripcion' => htmlentities($elemento["descripcion"])
                ,'url' => htmlentities($elemento["url"])
                ,'urlSeo' => htmlentities($elemento["urlSeo"])
                ,'urlImagen' => htmlentities($elemento["urlImagen"])
                ,'idEstatus' => $elemento["idEstatus"]
                );
            }
               
        }
        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});
$app->get("/sitios/:idSitio/categorias/", function($idSitio) use($app){
    try{
        $idCategoria =0;
        $connection = getConnection();
        $dbh = $connection->prepare("CALL sp_getCategorias(?,?)");
        $dbh->bindParam(1, $idSitio);
        $dbh->bindParam(2, $idCategoria);
        $dbh->execute();
        $elementos = $dbh->fetchAll();
        $connection = null;
        $respuesta = array();
        foreach ($elementos as $elemento) {
         $respuesta[]= array('idCategoria' => $elemento["idCategoria"]
                ,'idSitio' => $elemento["idSitio"]
                ,'categoria' => htmlentities($elemento["categoria"])
                ,'titulo' => htmlentities($elemento["titulo"])
                ,'subTitulo' => htmlentities($elemento["subTitulo"])
                ,'descripcion' => htmlentities($elemento["descripcion"])
                ,'url' => htmlentities($elemento["url"])
                ,'urlSeo' => htmlentities($elemento["urlSeo"])
                ,'urlImagen' => htmlentities($elemento["urlImagen"])
                ,'idEstatus' => $elemento["idEstatus"]
                );
        }

      $data=array('data'=>$respuesta);
      $app->response->headers->set("Content-type", "application/json");
      $app->response->status(200);
      $app->response->body(json_encode($data, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});

$app->get("/sitios/:idSitio/categorias/:idCategoria", function($idSitio,$idCategoria) use($app){
    try{
        $connection = getConnection();
        $dbh = $connection->prepare("CALL sp_getCategorias(?,?)");
        $dbh->bindParam(1, $idSitio);
        $dbh->bindParam(2, $idCategoria);
        $dbh->execute();
        $elemento = $dbh->fetch();
        $connection = null;
        $respuesta = array();
        if(!empty($elemento)) {
          $respuesta= array('idCategoria' => $elemento["idCategoria"]
                ,'idSitio' => $elemento["idSitio"]
                ,'categoria' => htmlentities($elemento["categoria"])
                ,'titulo' => htmlentities($elemento["titulo"])
                ,'subTitulo' => htmlentities($elemento["subTitulo"])
                ,'descripcion' => htmlentities($elemento["descripcion"])
                ,'url' => htmlentities($elemento["url"])
                ,'urlSeo' => htmlentities($elemento["urlSeo"])
                ,'urlImagen' => htmlentities($elemento["urlImagen"])
                ,'idEstatus' => $elemento["idEstatus"]
                );
        }

        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});
$app->get("/sitios/:idSitio/categorias/seo/:categoria", function($idSitio,$categoria) use($app){
    try{
        $connection = getConnection();
        $dbh = $connection->prepare("CALL sp_getCategoriasSeo(?,?)");
        $dbh->bindParam(1, $idSitio);
        $dbh->bindParam(2, $categoria);
        $dbh->execute();
        $elemento = $dbh->fetch();
        $connection = null;
        $respuesta = array();
        if(!empty($elemento)) {
          $respuesta= array('idCategoria' => $elemento["idCategoria"]
                ,'idSitio' => $elemento["idSitio"]
                ,'categoria' => htmlentities($elemento["categoria"])
                ,'titulo' => htmlentities($elemento["titulo"])
                ,'subTitulo' => htmlentities($elemento["subTitulo"])
                ,'descripcion' => htmlentities($elemento["descripcion"])
                ,'url' => htmlentities($elemento["url"])
                ,'urlSeo' => htmlentities($elemento["urlSeo"])
                ,'urlImagen' => htmlentities($elemento["urlImagen"])
                ,'idEstatus' => $elemento["idEstatus"]
                );
        }

        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});

$app->post("/sitios/:idSitio/categorias/", function($idSitio) use($app){
   try{
        $connection = getConnection();
        $categoria=$app->request->post('categoria');
        $descripcion=$app->request->post('descripcion');
        $titulo=$app->request->post('titulo');
        $subTitulo=$app->request->post('subTitulo');
        $url=$app->request->post('url');
        $urlSeo=$app->request->post('urlSeo');
        $carpeta=$app->request->post('carpeta');
        $carpeta=$app->request->post('carpeta');
        $urlImagen=$app->request->post('urlImagen');
        $dbh = $connection->prepare("CALL sp_addCategoria(?,?,?,?,?,?,?,?,?)");
        $dbh->bindParam(1, $idSitio);
        $dbh->bindParam(2, $categoria);
        $dbh->bindParam(3, $titulo);
        $dbh->bindParam(4, $subTitulo);
        $dbh->bindParam(5, $idEstatus);
        $dbh->bindParam(6, $url);
        $dbh->bindParam(7, $descripcion);
        $dbh->bindParam(8, $urlSeo);
        $dbh->bindParam(9, $_urlImagen);


        $dbh->execute();
        $elemento = $dbh->fetch();
        $connection = null;
        $respuesta = array();
        if(!empty($elemento)) {
          $respuesta = array('respuesta' => $elemento["respuesta"]
            , 'mensaje' => htmlentities($elemento["mensaje"])
            );
        }
        $data=array('data'=>$respuesta);
        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});
$app->options("/sitios/:idSalon/categorias/:idCategoria", function($idSalon,$idCategoria) {
    //Return response headers
});

$app->put("/sitios/:idSitio/categorias/:idCategoria", function($idSitio,$idCategoria) use($app){
    try{
        $connection = getConnection();
        $categoria=$app->request->post('categoria');
        $descripcion=$app->request->post('descripcion');
        $titulo=$app->request->post('titulo');
        $subTitulo=$app->request->post('subTitulo');
        $url=$app->request->post('url');
        $urlSeo=$app->request->post('urlSeo');
        $carpeta=$app->request->post('carpeta');
        $carpeta=$app->request->post('carpeta');
        $urlImagen=$app->request->post('urlImagen');

        $dbh = $connection->prepare("CALL sp_editCategoria(?,?,?,?,?,?,?,?,?,?)");
        $dbh->bindParam(1, $idSitio);
        $dbh->bindParam(2, $idCategoria);
        $dbh->bindParam(3, $categoria);
        $dbh->bindParam(4, $titulo);
        $dbh->bindParam(5, $subTitulo);
        $dbh->bindParam(6, $idEstatus);
        $dbh->bindParam(7, $url);
        $dbh->bindParam(8, $descripcion);
        $dbh->bindParam(9, $urlSeo);
        $dbh->bindParam(10, $_urlImagen);

        $dbh->execute();
        $elemento = $dbh->fetch();
        $connection = null;
        $respuesta = array();
        if(!empty($elemento)) {
          $respuesta = array('respuesta' => $elemento["respuesta"]
            , 'mensaje' => htmlentities($elemento["mensaje"])
            );
        }

        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});
$app->delete("/sitios/:idSalon/categorias/:idCategoria", function($idSalon,$idCategoria) use($app){
    try{
        $connection = getConnection();
        $dbh = $connection->prepare("CALL sp_deleteCategoria(?,?)");
        $dbh->bindParam(1, $idSalon);
        $dbh->bindParam(2, $idCategoria);
        $dbh->execute();
        $elemento = $dbh->fetch();
        $connection = null;
        $respuesta = array();
        if(!empty($elemento)) {
          $respuesta = array('respuesta' => $elemento["respuesta"]
            , 'mensaje' => htmlentities($elemento["mensaje"])
            );
        }
        $app->response->headers->set("Content-type", "application/json");
        $app->response->status(200);
        $app->response->body(json_encode($respuesta, JSON_UNESCAPED_UNICODE));
    }
    catch(PDOException $e){
        echo "Error: " . $e->getMessage();
    }
});